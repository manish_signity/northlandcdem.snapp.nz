<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php

$id=isset($_GET['id'])?$_GET['id']:false;
$message=isset($_GET['message'])?$_GET['message']:false;


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Messages</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'messages'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Messages
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Messages</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($message && $message==="addsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Message sent successfully
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="deletesuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Message deleted successfully
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="emailsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                User credentials sent successfully
                            </div>
                            <?php } ?>
                            <div class="box">
                                <div class="box-links">
                                    <a class="link" href="message.php?id=new&edit=search">Create New Message</a> |
                                    <a class="link" href="send_user_credentials.php?edit=search">Send User Credentials</a>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive" style="display:none">
                                    <table id="messagestable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Title</th>
                                                <th>Message</th>
                                                <th>Sender</th>
                                                <th>is admin sender</th>
                                                <th>Contacts</th>
                                                <th>Sent</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>id</th>
                                                <th>Title</th>
                                                <th>Message</th>
                                                <th>Sender</th>
                                                <th>is admin sender</th>
                                                <th>Contacts</th>
                                                <th>Sent</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('#messagestable').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_messages.php",
                    "order": [[0, 'desc']],
                    "dom": 'flrtip',
                    "columns": [
                        { "visible":    false}, 
                        null,
                        null,
                        null,
                        { "visible":    false}, 
                        null,
                        null,
                        null
                      ]
                    ,
                    "columnDefs": [ {
                      "targets": [ 2 ],
                      "data": function(row, type, val, meta) {
                          return row[2].length > 100 ? row[2].substring(0,100)+'...' : row[2];
                      }
                    }, {
                      "targets": [ 7 ],
                      "data": function(row, type, val, meta) {
                          return "<a class='link' href='message.php?id="+row[0]+"'>View</a>"
                                +" | <a class='link' href='message.php?id="+row[0]+"&edit=search'>Copy</a>"
                                +" | <a class='link' href='message.php?delete_id="+row[0]+"' onclick='return confirm(\"Are you sure you want to delete this message?\");'>Delete</a>";
                      }
                    } ]
                } );
            });
        </script>

    </body>
</html>
