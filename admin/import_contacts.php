<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php

// restrict import page to just admin@snapp.co.nz user
if ($_SESSION['loggedInUser'] !== "admin@snapp.co.nz") {
    header("Location:/admin/contacts.php");
    exit;
}

$duplicate_email=$missing_email="";

$input_name="import_file";
$max_file_size=10000000;

$state="upload";


if(isset($_FILES[$input_name])) {
    require_once "common/import_functions.php";
    $upload_result = checkUpload($input_name, $max_file_size);
    $upload_error = $upload_result['error'];
    $upload_sha1 = $upload_result['sha1'];
    if (!isset($upload_error)) {
        $state="import_stage";
        // clear out admin users import stage
        $query=$conn->prepare("delete from import_stage where admin_id=:admin_id");
        $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
        $count=$query->execute();
        
        // load csv
        $filename = "./uploads/".$upload_sha1.".csv";
        require_once "common/parsecsv.lib.php";
        $csv = new parseCSV($filename);
        
        //run through csv and import to stage
        importToStage($csv, $conn);
        
        $importResults = attemptImport($conn);
        
        $missing_email=$importResults['missing'];
        $duplicate_email=$importResults['duplicate'];
        
        $missing_count=count($missing_email);
        $duplicate_count=count($duplicate_email);
        
        if ($missing_count == 0 && $duplicate_count == 0) {
            $state="import_success";
        }
    }
} else if (isset($_POST['check_import'])) {
    require_once "common/import_functions.php";
    $state="import_stage";
    
    // edit import stage
    editImportStage($conn, $_POST);
    
    // attempt the import
    $importResults = attemptImport($conn);

    $missing_email=$importResults['missing'];
    $duplicate_email=$importResults['duplicate'];

    $missing_count=count($missing_email);
    $duplicate_count=count($duplicate_email);
    
    if ($missing_count == 0 && $duplicate_count == 0) {
        $state="import_success";
    }
}

if ($state === "import_success") {
    header("Location:/admin/contacts.php?import_success=true");
    exit;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bay of Plenty CDEM | Import Contacts</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'contacts'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Import Contacts
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="contacts.php">Contacts</a></li>
                        <li class="active">Import Contacts</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($state==="upload") { ?>
                            <?php if (isset($upload_error)) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Error!</b> <?=$upload_error?>
                            </div>
                            <?php } ?>
                            
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Select file for import</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" enctype="multipart/form-data" method="POST" action="import_contacts.php">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="<?=$max_file_size?>" />
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="importFile">File input</label>
                                            <input type="file" id="importFile" name="<?=$input_name?>">
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary" onclick="return checkOverwrite();">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                            <?php } else if ($state === "import_stage") { ?>
                            <div class="alert alert-warning alert-dismissable">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Action needed.</b> Please check the import details below
                            </div>
                            <!-- form start -->
                            <form role="form" method="POST" action="import_contacts.php">
                                <input type="hidden" name="check_import" value="true" />
                                <div class="box box-warning">
                                    <div class="box-header">
                                        <h3 class="box-title">Missing Email Address (<?=$missing_count?>)</h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body table-responsive">
                                        <table id="missingemail" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>id</th>
                                                    <th>Display Only Contact</th>
                                                    <th>Email</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Company</th>
                                                    <th>Department</th>
                                                    <th>Mobile Phone</th>
                                                    <th>Phone</th>
                                                </tr>
                                            </thead>

                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th>id</th>
                                                    <th>Display Only Contact</th>
                                                    <th>Email</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Company</th>
                                                    <th>Department</th>
                                                    <th>Mobile Phone</th>
                                                    <th>Phone</th>
                                                </tr>
                                            </tfoot>

                                            <tbody>
                                                <?php foreach ($missing_email as $row) { ?>
                                                <tr>
                                                    <td></td>
                                                    <td><?=$row['id']?></td>
                                                    <td><input type="checkbox" name="displayonly-<?=$row['id']?>" checked="checked"></td>
                                                    <td>
                                                        <span id="displayemail-<?=$row['id']?>"></span>
                                                        <input type="email" id="email-<?=$row['id']?>" name="email-<?=$row['id']?>" style="display:none;">
                                                    </td>
                                                    <td><?=$row['first_name']?></td>
                                                    <td><?=$row['last_name']?></td>
                                                    <td><?=$row['company']?></td>
                                                    <td><?=$row['department']?></td>
                                                    <td><?=$row['mobile_phone']?></td>
                                                    <td><?=$row['phone']?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                <div class="box box-warning">
                                    <div class="box-header">
                                        <h3 class="box-title">Duplicate Email Address (<?=$duplicate_count?>)</h3>
                                    </div><!-- /.box-header -->
                                        <div class="box-body table-responsive">
                                            <table id="duplicateemail" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>id</th>
                                                        <th>Display Only Contact</th>
                                                        <th>Email</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Company</th>
                                                        <th>Department</th>
                                                        <th>Mobile Phone</th>
                                                        <th>Phone</th>
                                                    </tr>
                                                </thead>

                                                <tfoot>
                                                    <tr>
                                                        <th></th>
                                                        <th>id</th>
                                                        <th>Display Only Contact</th>
                                                        <th>Email</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Company</th>
                                                        <th>Department</th>
                                                        <th>Mobile Phone</th>
                                                        <th>Phone</th>
                                                    </tr>
                                                </tfoot>

                                                <tbody>
                                                    <?php foreach ($duplicate_email as $row) { ?>
                                                    <tr>
                                                        <?php 
                                                            $checkboxChecked = $row['dummy_email'] ? 'checked="checked"' : ''; 
                                                            $spanDisplay = $row['dummy_email'] ? '' : 'style="display:none;"'; 
                                                            $inputDisplay = $row['dummy_email'] ? 'style="display:none;"' : ''; 
                                                        ?>
                                                        <td></td>
                                                        <td><?=$row['id']?></td>
                                                        <td><input type="checkbox" name="displayonly-<?=$row['id']?>" <?=$checkboxChecked?>></td>
                                                        <td>
                                                            <span id="displayemail-<?=$row['id']?>" <?=$spanDisplay?>><?=$row['email']?></span>
                                                            <input type="email" id="email-<?=$row['id']?>" name="email-<?=$row['id']?>" <?=$inputDisplay?> value="<?=$row['email']?>">
                                                        </td>
                                                        <td><?=$row['first_name']?></td>
                                                        <td><?=$row['last_name']?></td>
                                                        <td><?=$row['company']?></td>
                                                        <td><?=$row['department']?></td>
                                                        <td><?=$row['mobile_phone']?></td>
                                                        <td><?=$row['phone']?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                
                                <div class="box box-success">
                                    <div class="box-header">
                                        <h3 class="box-title">Continue Import</h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div><!-- /.box -->
                            </form>
                            <?php } ?>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            var missingEmails = <?php echo json_encode($missing_email); ?>;
            var duplicateEmails = <?php echo json_encode($duplicate_email); ?>;
            
            $(document).ready(function() {
                var missingemail = $('#missingemail').DataTable( {
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    "searching":false,
                    "columns": [
                        { "className":  'details-control',
                          "orderable":  false, },
                        { "visible":    false}, 
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                      ]
                } );
                var duplicateemail = $('#duplicateemail').DataTable( {
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    "searching":false,
                    "columns": [
                        { "className":  'details-control',
                          "orderable":  false, },
                        { "visible":    false}, 
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                      ]
                } );
                
                // Add event listener for toggling Display Only Contact checkbox
                $('#missingemail input, #duplicateemail input').on('ifChanged', function(event){
                    var checkboxName = $(this).attr('name');
                    var isChecked = $(this).is(':checked');
                    var id = checkboxName.substring('displayonly-'.length);
                    if (isChecked) {
                        $('#email-'+id).hide();
                        $('#displayemail-'+id).text($('#email-'+id).val()).show();
                    } else {
                        $('#displayemail-'+id).hide();
                        $('#email-'+id).show();
                    }
                    
                });
                
                // Add event listener for opening and closing details
                $('#missingemail tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = missingemail.row( tr );

                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(missingEmails, row.data()) ).show();
                        tr.addClass('shown');
                    }
                } );
                $('#duplicateemail tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = duplicateemail.row( tr );

                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(duplicateEmails, row.data()) ).show();
                        tr.addClass('shown');
                    }
                } );
                
            });
            
            function format ( data, d ) {
                var rowData=null;
                for (var i = 0, len = data.length; i < len; i++) {
                    if (data[i]['id'] === d[1]) {
                        rowData = data[i];
                        break;
                    }
                }
                if(rowData!==null) {
                    var privateData = JSON.parse(rowData['private_data']);
                    var nonPrivateData = JSON.parse(rowData['non_private_data']);
                    var details = '';
                    for (var label in privateData) {
                        details+='<b>'+label+':</b> '+privateData[label]+'\n';
                    }
                    for (var label in nonPrivateData) {
                        details+='<b>'+label+':</b> '+nonPrivateData[label]+'\n';
                    }
                    
                    return details;
                }
            }
            
            function checkOverwrite() {
                return confirm("Are you sure you want to overwrite all the current Contacts data?");
            }
        </script>

    </body>
</html>
