<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php

// restrict import page to just admin@snapp.co.nz user
if ($_SESSION['loggedInUser'] !== "admin@snapp.co.nz") {
    header("Location:/admin/contacts.php");
    exit;
}

$input_name="import_file";
$max_file_size=10000000;

$state="upload";

$noMatchContacts=array();

if(isset($_FILES[$input_name])) {
    require_once "common/import_functions.php";
    $upload_result = checkUpload($input_name, $max_file_size);
    $upload_error = $upload_result['error'];
    $upload_sha1 = $upload_result['sha1'];
    if (!isset($upload_error)) {
        $state="import_stage";
        
        // load csv
        $filename = "./uploads/".$upload_sha1.".csv";
        require_once "common/parsecsv.lib.php";
        $csv = new parseCSV($filename);
        
        //run through csv and import groups
        $noMatchContacts = importGroups($csv, $conn);
        
    }
}


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bay of Plenty CDEM | Import Groups</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'contacts'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Import Groups
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="contacts.php">Contacts</a></li>
                        <li class="active">Import Groups</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($state==="upload") { ?>
                                <?php if (isset($upload_error)) { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Error!</b> <?=$upload_error?>
                                </div>
                                <?php } ?>

                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">Select file for import</h3>
                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                    <form role="form" enctype="multipart/form-data" method="POST" action="import_groups.php">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="<?=$max_file_size?>" />
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="importFile">File input</label>
                                                <input type="file" id="importFile" name="<?=$input_name?>">
                                            </div>
                                        </div><!-- /.box-body -->

                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div><!-- /.box -->
                            <?php } else { ?>
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">Group contacts that did not match and Contacts</h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <ul>
                                        <?php foreach($noMatchContacts as $contact=>$groups) { ?>
                                        <li>
                                            <span><?=$contact?></span>
                                            <ul>
                                                <?php foreach($groups as $group) { ?>
                                                <li><?=$group?></li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                        <?php } ?>
                                        </ul>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            <?php } ?>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
        </script>

    </body>
</html>
