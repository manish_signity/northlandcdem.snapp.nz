<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>
<?php require_once "common/functions.php"; ?>

<?php
$edit=isset($_GET['edit'])?$_GET['edit']:false;
$message=isset($_GET['message'])?$_GET['message']:false;
$sendResultMsg=isset($_GET['sendResultMsg'])?$_GET['sendResultMsg']:false;
$id="";

$message_data=array();
$message_contacts=array();
$message_groups=array();

if (isset($_POST['select_from'])) {

   
    $id=$_POST['id'];
    $title=isset($_POST['title'])?trim($_POST['title']):false;
    $message_post=isset($_POST['message'])?trim($_POST['message']):false;
    $selects=isset($_POST['selects'])?$_POST['selects']:array();
    if ($_POST['select_from']==="contact" && count($selects)>0) {
        $inQuery = implode(',', array_fill(0, count($selects), '?'));
        $query=$conn->prepare("select email, first_name, last_name from contact where email in (".$inQuery.")");
        foreach($selects as $k => $email) {
            $query->bindValue(($k+1), $email);
        }
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $message_contact) {
            $message_contacts[]=array("email"=>$message_contact['email'],"first_name"=>$message_contact['first_name'],"last_name"=>$message_contact['last_name']);
        }
    }
} else if (isset($_POST['id'])) {

    $id=$_POST['id'];
    
    if ($id==="new") {
        
        $title=isset($_POST['title'])?trim($_POST['title']):false;
        $message_post=isset($_POST['message'])?trim($_POST['message']):false;
        $contacts=isset($_POST['contacts'])?$_POST['contacts']:array();
        $groups=isset($_POST['groups'])?$_POST['groups']:array();
        
       
        $valid=false;
        
        $title_empty    = !($title&&$title!=='');
        $message_empty  = !($message_post&&$message_post!=='');
        
        $contacts_empty = !($contacts&&$contacts!=='');
        $groups_empty   = !($groups&&$groups!=='');
        
        if($contacts_empty && $groups_empty){
          $contacts__user_empty = 1;
        }
        
        if ($title_empty && $message_empty && $contacts__user_empty) {
            $message="invalid-title-message";
        } else if ($title_empty) {
            $message="invalid-title";
        }else if ($contacts__user_empty) {
            $message="invalid-contacts";
        } else if ($message_empty) {
            $message="invalid-message";
        } else {
            $valid=true;
        }
        
        if ($valid) {
                    
            $query=$conn->prepare("insert into message (title,message,admin_sender) values(:title,:message,:admin_id)");
            $query->bindValue("title",$title);
            $query->bindValue("message",$message_post);
            $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
            $query->execute();
            $message_id = $conn->lastInsertId();
            
            if (count($groups)>0) {
               
                $inQuery = implode(',', array_fill(0, count($groups), '?'));
                $query=$conn->prepare("select email from group_contact where group_id in (".$inQuery.")");
                foreach($groups as $k => $group_id) {
                    $query->bindValue(($k+1), $group_id);
                }
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_ASSOC);

                foreach($results as $contact) {
                    $contacts[]=$contact['email'];
                }
            }
            
            $contacts = array_unique($contacts);
        
            $query=$conn->prepare("insert into message_contact (message_id,email) values(:message_id,:email)");
            $query->bindValue("message_id",$message_id);
            foreach($contacts as $email) {
                $query->bindValue("email",$email);
                $query->execute();
            }
            
            require_once "common/send_message.php"; 
                    
            $db_details=getDBDetails();
            
            $sendResult = sendMessage($conn, $db_details, $message_id);
            
//            var_dump($sendResult);
            
            header("Location:/admin/message.php?id=".$message_id."&message=addsuccess&sendResultMsg=".$sendResult['msg']);
            exit;
        } else {

            $message_data=array("title"=>$_POST['title'],"message"=>$_POST['message']);
            
            if (count($contacts)>0) {
               // die("31");
                $inQuery = implode(',', array_fill(0, count($contacts), '?'));
                $query=$conn->prepare("select email, first_name, last_name from contact where email in (".$inQuery.")");
                foreach($contacts as $k => $email) {
                    $query->bindValue(($k+1), $email);
                }
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_ASSOC);

                foreach($results as $message_contact) {
                    $message_contacts[]=array("email"=>$message_contact['email'],"first_name"=>$message_contact['first_name'],"last_name"=>$message_contact['last_name']);
                }
            }
            
            if (count($groups)>0) {
              //  die("32");
                $inQuery = implode(',', array_fill(0, count($groups), '?'));
                $query=$conn->prepare("select id, name from groups where id in (".$inQuery.")");
                foreach($groups as $k => $group_id) {
                    $query->bindValue(($k+1), $group_id);
                }
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_ASSOC);

                foreach($results as $message_group) {
                    $message_groups[]=array("id"=>$message_group['id'],"name"=>$message_group['name']);
                }
            }
            
            $edit="search";
        }
    } else {
        // currently no 'update' of message, only a copy that creates a new one (above)
        header("Location:/admin/message.php?id=".$message_id);
        exit;
    }
        
} else if (isset($_GET['id'])) {
    $id=$_GET['id'];
    
    if ($id==="new") {
        $message_data=array("title"=>"","message"=>"");
    } else {
        $query=$conn->prepare("select * from message where id=:id");
        $query->bindValue("id",$id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
            $message_data=$results[0];
        }
        
        $query=$conn->prepare("select message_contact.email, contact.first_name, contact.last_name from message_contact left join contact on message_contact.email=contact.email where message_id=:id");
        $query->bindValue("id",$id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
            $message_contacts=$results;
        }
        
        if ($edit) {
            $id="new";
        }
        
    }
    
} else if (isset($_GET['delete_id'])) {
    $query=$conn->prepare("delete ignore from message where id=:delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    header("Location:/admin/messages.php?message=deletesuccess");
    exit;
}


$disabled=$edit?"":"disabled='disabled'";

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Message</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'messages'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $edit?"New":"View"; ?> Message
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="messages.php">Messages</a></li>
                        <li class="active"><?php echo $edit?"New":"View"; ?> Message</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($message && ($message==="invalid-contacts" || $message==="invalid-title-message")) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Contacts / Groups required
                            </div>
                            <?php } ?>
                            <?php if ($message && ($message==="invalid-title" || $message==="invalid-title-message")) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Title required
                            </div>
                            <?php } ?>
                            <?php if ($message && ($message==="invalid-message" || $message==="invalid-title-message")) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Message content required
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="addsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Message sent. <?=$sendResultMsg?>
                            </div>
                            <?php } ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Message Details</h3>
                                </div>
                                <div class="box-body">
                                    <?php if ($edit) { 
                                        ?>
                                    <form id="message_form" role="form" method="POST" action="message.php">
                                        <input type="hidden" name="id" value="<?=$id?>">
                                    <?php } ?>
                                    <div>
                                        <label>Contacts</label>
                                        <div>
                                            <ul id="contactslist" class="csv">
                                            <?php
                                            foreach($message_contacts as $message_contact) {
                                                ?>
                                                <li id="contactslist_<?=$message_contact['email']?>"><?=$message_contact['first_name']?> <?=$message_contact['last_name']?> (<?=$message_contact['email']?>)</li>
                                                <?php
                                            }
                                            ?>
                                            </ul>
                                        </div>
                                        <?php
                                        foreach($message_contacts as $message_contact) {
                                            ?>
                                            <input type="hidden" name="contacts[]" value="<?=$message_contact['email']?>"/>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php if ($edit) { ?>
                                    <span><h4 class="box-title">Select Contacts</h4> (<a id="contacts_showhide">show</a>)</span>
                                    <table id="contactstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Organisation</th>
                                                <th>Department</th>
                                                <th>Phone</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Last Modified</th>
                                                <th>fullname</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Organisation</th>
                                                <th>Department</th>
                                                <th>Phone</th>
                                                
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Last Modified</th>
                                                <th>fullname</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br/>
                                    <?php } ?>
                                    <div>
                                        <label>Groups</label>
                                        <div>
                                            <ul id="groupslist" class="csv">
                                            <?php
                                            foreach($message_groups as $message_group) {
                                                ?>
                                                <li id="groupslist_<?=$message_group['id']?>"><?=$message_group['name']?></li>
                                                <?php
                                            }
                                            ?>
                                            </ul>
                                        </div>
                                        <?php
                                        foreach($message_groups as $message_group) {
                                            ?>
                                            <input type="hidden" name="groups[]" value="<?=$message_group['id']?>"/>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php if ($edit) { ?>
                                    <span><h4 class="box-title">Select Groups</h4> (<a id="groups_showhide">show</a>)</span>
                                    <table id="groupstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>child count</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>child count</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br/>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="Enter ..." value="<?=$message_data['title']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea name="message" class="form-control" rows="3" placeholder="Enter ..." <?=$disabled?> ><?=$message_data['message']?></textarea>
                                    </div>
                                    <?php if ($edit) { ?>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    
                                    <?php if ($edit) {
                                        switch ($edit) {
                                            case "search":
                                                $cancel_href="messages.php";
                                                break;
                                            case "dashboard":
                                                $cancel_href="index.php";
                                                break;
                                            default:
                                                $cancel_href="message.php?id=".$id;
                                        }
                                        ?>
                                        <a class="btn btn-app" href="<?=$cancel_href?>">
                                            <i class="fa fa-times"></i> Cancel
                                        </a>
                                        <a id="save_link" class="btn btn-app">
                                            <i class="fa fa-arrow-right"></i> Send
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class="btn btn-app" href="messages.php">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            <?php if ($edit) { ?>
            var checkboxSelect = {}, groupCheckboxSelect = {};
            $(function() {
                $('#contactstable').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_contacts.php",
                    "order": [[1, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        {'searchable':false,'orderable':false},
                        null,
                        {"visible":false},
                        null,
                        {"visible":false},
                        {"visible":false},
                        null,
                        null,
                        null,
                        {"visible":false}
                      ]
                    ,
                    "columnDefs": [ {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          return "<input type='checkbox' value='"+row[7]+"' />";
                      }
                    } ]
                } );
                $('#contactstable').on( 'draw.dt', function () {
                    $('#contactstable input[type="checkbox"]').change(function () {
                        selectContact($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#contactstable input[type="checkbox"]').each(function() {
                        if (checkboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                
                $('#groupstable').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_groups.php",
                    "order": [[3, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        {'searchable':false,'orderable':false},
                        { "visible":    false}, 
                        { "visible":    false}, 
                        null,
                        null,
                        null,
                        { "visible":    false}, 
                        null,
                        null
                      ],
                    "columnDefs": [ {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          return "<input type='checkbox' value='"+row[1]+"' />";
                      }
                    } ]
                } );
                $('#groupstable').on( 'draw.dt', function () {
                    $('#groupstable input[type="checkbox"]').change(function () {
                        selectGroup($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#groupstable input[type="checkbox"]').each(function() {
                        if (groupCheckboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                
                $('#contactstable_wrapper').hide();
                $('#contacts_showhide').click(function() {
                    tableClick(this, $('#contactstable_wrapper'));
                });
                $('#groupstable_wrapper').hide();
                $('#groups_showhide').click(function() {
                    tableClick(this, $('#groupstable_wrapper'));
                });
            });
            $('#save_link').click(function() {
                $('#message_form').submit();
            });
            
            function selectContact(email, checked, tr) { 
                checkboxSelect[email]=checked;
                if (checked) {
                    if ($('#contactslist li[id="contactslist_'+email+'"]').length == 0) {
                        var tds = $(tr).find('td');
                        var firstName = $(tds[1]).text();
                        var lastName = $(tds[2]).text();
                        $('#contactslist').append($('<li>', {
                            id: 'contactslist_'+email,
                            text: firstName+' '+lastName+' ('+email+')'
                        })).after('<input type="hidden" name="contacts[]" value="'+email+'"/>');
                    }
                } else {
                    $('#contactslist li[id="contactslist_'+email+'"]').remove();
                    $('input[type="hidden"][value="'+email+'"]').remove();
                    $('#contactstable input[type="checkbox"]').each(function() {
                        if ($(this).val()===email) {
                            $(this).prop('checked', false);
                        }
                    });
                }
            }
            function selectGroup(groupId, checked, tr) {
                groupCheckboxSelect[groupId]=checked;
                if (checked) {
                    var tds = $(tr).find('td');
                    var name = $(tds[1]).text();
                    
                    $('#groupslist').append($('<li>', {
                        id: 'groupslist_'+groupId,
                        text: name
                    })).after('<input type="hidden" name="groups[]" value="'+groupId+'"/>');
                } else {
                    $('#groupslist li[id="groupslist_'+groupId+'"]').remove();
                    $('input[type="hidden"][value="'+groupId+'"]').remove();
                    $('#groupstable input[type="checkbox"]').each(function() {
                        if ($(this).val()===groupId) {
                            $(this).prop('checked', false);
                        }
                    });
                }
            }
            function tableClick(link, table) {
                if ($(link).text()==="show") {
                    $(table).show();
                    $(link).text("hide");
                } else {
                    $(table).hide();
                    $(link).text("show");
                }
            }
            <?php } ?>
        </script>

    </body>
</html>
