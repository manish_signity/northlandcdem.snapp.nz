<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>
<?php
    require_once "common/export_functions.php";
    header('Content-type: text/csv');
    header('Content-Disposition: attachment; filename="hbrc_export_'.date('d-m-Y-His').'.csv"');
    exportCSV($conn);
    die();