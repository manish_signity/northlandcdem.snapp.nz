<?php
require "functions.php";

function exportCSV($conn) {
    
    $query=$conn->prepare("select email, first_name, last_name, company, department, mobile_phone, phone, email_second,ddi,home_phone,organisation_phone,private_data, non_private_data from contact");
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_ASSOC);
    
    //email_second,ddi,home_phone,organisation_phone,
    $mainHeaders = array("Email","Email Second","Name","Organisation","Mobile Phone",'DDI','Home Phone','organisation Phone',"Groups");
    //$nonPrivateHeaders = array("Co Dept","Co Address","Co City","Co Region","Co Post Code","Co Country","Co DDI Phone","Co Main Phone");
    //$privateHeaders = array("Home Phone","Home Address");
    
    //$headers = array_merge($mainHeaders, $nonPrivateHeaders, $privateHeaders);
    $headers = $mainHeaders; 
    $fixedHeadersSize = count($headers);
    
    $array=array( $headers );
    $extraDataByEmail = array();
        
    foreach ($results as $contact) {
    	$groupNames = array();
    	$query=$conn->prepare(
    			"select g.id, g.name from group_contact gc, groups g "
    			."where gc.group_id=g.id "
    			."and gc.email=:email");
    	$query->bindValue("email",$contact['email']);
    	$query->execute();
    	$groups=$query->fetchAll(PDO::FETCH_ASSOC);
    	
    	foreach($groups as $group) {
    		$groupNames[] = $group['name'];
    	}
        //$non_private_data = json_decode($contact['non_private_data'], true);
        //$private_data = json_decode($contact['private_data'], true);
        //$groupNames = array();
        
        // fixed columns
        /* $selectedCdemGroup = findInArray($non_private_data, 'selected_cdem_group');
        $selectedCdemGroupChild = findInArray($non_private_data, 'selected_cdem_group_child');
        $coDept = findInArray($non_private_data, 'Co Dept');
        $coAddress = findInArray($non_private_data, 'Co Address');
        $coCity = findInArray($non_private_data, 'Co City');
        $coRegion = findInArray($non_private_data, 'Co Region');
        $coPostCode = findInArray($non_private_data, 'Co Post Code');
        $coCountry = findInArray($non_private_data, 'Co Country');
        $coDDIPhone = findInArray($non_private_data, 'Co DDI Phone');
        $coMainPhone = findInArray($non_private_data, 'Co Main Phone');
        $coJobTitle = findInArray($non_private_data, 'job_title');
        $homePhone = findInArray($private_data, 'Home Phone');
        $homeAddress = findInArray($private_data, 'Home Address'); */
        
       /*  $query=$conn->prepare(
                 "select g.id, g.name from group_contact gc, groups g "
                ."where gc.group_id=g.id "
                ."and gc.email=:email");
        $query->bindValue("email",$contact['email']);
        $query->execute();
        $groups=$query->fetchAll(PDO::FETCH_ASSOC);
        
        foreach($groups as $group) {
            $groupNames[] = $group['name'];
        }
         */
        
        $contactArray = array(
            $contact['email'],
        	$contact['email_second'],
            $contact['first_name'],
            $contact['company'],
            $contact['mobile_phone'],
        	$contact['ddi'],
        	$contact['home_phone'],
        	$contact['organisation_phone'],
        	implode('|', $groupNames),
        );
        
        //$extraDataByEmail[$contact['email']] = array("non_private_data"=>$non_private_data, "private_data"=>$private_data);
                
        // append to end of array
        $array[] = $contactArray;
    }
    
    
    // check for dynamic columns
    /* $dynamicHeaders = array();
    
    foreach ($array as &$contactArray) {
        if ($contactArray[0] === "Email") {
            continue; // header row
        }
        $extraData = $extraDataByEmail[$contactArray[0]];
        $non_private_data = $extraData['non_private_data'];
        
        // check for dynamic columns
        foreach ($non_private_data as $key => $value) {
            if ($key === "selected_cdem_group" || $key === "selected_cdem_group_child" || $key === "job_title") {
                continue;
            }
            if (array_key_exists($key, $dynamicHeaders)) {
                // if a dynamic header value
                $dynIndex = $dynamicHeaders[$key];
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            } else if (!in_array($key, $nonPrivateHeaders)) {
                // if not a fixed header, new dynamic value
                $dynIndex = count($dynamicHeaders);
                $array[0][] = $key;
                $dynamicHeaders[$key] = $dynIndex;
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            }
        }
    } */
        
    // reset dynamic headers for private data
    /* $fixedHeadersSize += count($dynamicHeaders);
    $dynamicHeaders = array();
        
    foreach ($array as &$contactArray) {
        if ($contactArray[0] === "Email") {
            continue; // header row
        }
        $extraData = $extraDataByEmail[$contactArray[0]];
        $private_data = $extraData['private_data'];
        
        // check for dynamic columns
        foreach ($private_data as $key => $value) {
            if (array_key_exists($key, $dynamicHeaders)) {
                // if a dynamic header value
                $dynIndex = $dynamicHeaders[$key];
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            } else if (!in_array($key, $privateHeaders)) {
                // if not a fixed header, new dynamic value
                $dynIndex = count($dynamicHeaders);
                $array[0][] = $key;
                $dynamicHeaders[$key] = $dynIndex;
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            }
        }
    }
    
    fillArrayWithBlanks($array); */
    
//    var_dump($array);
    echo array2csv($array);
}

function findInArray($array, $key) {
	return $array && array_key_exists($key, $array) ? cleanArrayValue($array[$key]) : '';
}


function cleanArrayValue($value) {
	return str_replace("\n", " ", $value);
}
function fillArrayWithBlanks(&$array) {
	$headerSize = count($array[0]);

	foreach($array as &$contactsArray) {
		if (count($contactsArray) < $headerSize) {
			for ($i = 0; $i < $headerSize; $i++) {
				if (!array_key_exists($i, $contactsArray)) {
					$contactsArray[$i] = "";
				}
			}
		}
		ksort($contactsArray);
	}
}

function array2csv(array &$array) {
	if (count($array) == 0) {
		return null;
	}
	ob_start();
	$df = fopen("php://output", 'w');
	//fputcsv($df, array_keys(reset($array)));
	foreach ($array as $row) {
		fputcsv($df, $row);
	}
	fclose($df);
	return ob_get_clean();
}