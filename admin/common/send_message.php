<?php

function sendMessage($conn, $db_details, $message_id) {
	require_once "gcm.php";
	require_once "easyapns/classes/class_APNS.php";
	require_once "easyapns/classes/class_DbConnect.php";
	$gcmids=$apnids=array();
        $title=$message=$sender=null;
        $senderFirstName=$senderLastName=$senderAdminName=null;
	    $success=1;
        
        $query=$conn->prepare(
                "select m.title, m.message, c.first_name, c.last_name, a.name from message m "
                . "left join contact c on m.sender = c.email "
                . "left join admin a on m.admin_sender = a.id "
                . "where m.id=:id");
        $query->bindValue("id",$message_id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
            $title=$results[0]['title'];
            $message=$results[0]['message'];
            $senderFirstName=$results[0]['first_name'];
            $senderLastName=$results[0]['last_name'];
            $senderAdminName=$results[0]['name'];
        }
        
        if ($senderFirstName !== null || $senderLastName !== null) {
            if ($senderFirstName !== null) {
                $sender = $senderFirstName;
            }
            if ($senderLastName !== null) {
                $sender = ($sender !== null ? $sender." " : "") . $senderLastName;
            }
        } else {
            $sender = $senderAdminName;
        }
        
        /*$query=$conn->prepare("select gcm_id, apn_id, unread_message_count from message_contact mc, contact_device cd, device d, contact c where mc.message_id=:message_id and mc.email=cd.email and cd.device_id=d.id and cd.email=c.email");
        $query->bindValue("message_id",$message_id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $token_id) {
            $unread_message_count = $token_id['unread_message_count'];
            if ($token_id['gcm_id']!==null) {
//                if (!array_key_exists($unread_message_count, $gcmids)) {
//                    $gcmids[$unread_message_count]=array();
//                }
//                $gcmids[$unread_message_count][]=$token_id['gcm_id'];
                $gcmids[]=$token_id['gcm_id'];
            }
            if ($token_id['apn_id']!==null) {
                if (!array_key_exists($unread_message_count, $apnids)) {
                    $apnids[$unread_message_count]=array();
                }
                $apnids[$unread_message_count][]=$token_id['apn_id'];
            }
        }
        
        $limit=100;
        $push_message=strlen($message)>$limit?substr($message, 0, $limit-3).'...':$message;
        $push=array("title"=>$title,"message"=>$push_message,"id"=>$message_id,"sender"=>$sender);*/

        $query=$conn->prepare(
                "select m.title, m.message, c.email,c.first_name, c.last_name, c.device_token, c.platform from message_contact m1  join message m on m.id = m1.message_id  join contact c on m1.email = c.email  where m.id=:message_id and c.device_token!=''");

        $query->bindValue("message_id",$message_id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
       
        $iOSToken = array(); 
        $androidToken = array(); 

        foreach($results as $token_id) {
            $unread_message_count = $token_id['unread_message_count'];
            if($token_id['platform']=='ios'){
                $iOSToken[] = $token_id['device_token'];
            } else {
                $androidToken[] = $token_id['device_token'];
            }
        }
       
	//call send push functions
	if($message) {
            /*$failed=0;
            if (count($gcmids)) {
                try {
                    $gcm = new GCM;
                    $status = $gcm->sendPush($gcmids, $push);
                    $failed = $status!==null?$status['failure']:$failed;
                } catch (Exception $e) {
                    $success = 0;
                }
            }*/

            /*if (count($apnids)) {
                foreach($apnids as $unread_message_count => $ids) {
                    $badge_number=$unread_message_count+1;
                    try {
                        $db = new DbConnect($db_details['host'], $db_details['user'], $db_details['password'], $db_details['dbname']);
                        //$db->show_errors();

                        $apns = new APNS($db);
                        $apns->newMessage($ids);
                        $apns->addMessageAlert($title);
                        $apns->addMessageBadge($badge_number);
    //                    $apns->addMessageSound('x.wav');
                        $apns->addMessageCustom('C', $push);
                        $apns->queueMessage();
                        $apns->processQueue();
                    } catch (Exception $e) {
                        $success = 0;
                    }
                }
            }
            */
            
            $fromTitle = $sender;
            $fromMessage = $title;


            if(!empty($iOSToken)){
                sendNotificationIosFCM($iOSToken,$fromMessage,$fromTitle);
            }
            if(!empty($androidToken)){
                sendNotificationAndroidFCM($androidToken,$fromMessage,$fromTitle);

            }

            if ($success) {
                
                $query=$conn->prepare("update message set sent=now() where id=:id");
                $query->bindValue("id",$message_id);
                $query->execute();
                
                $query=$conn->prepare("update contact set unread_message_count=unread_message_count+1 where email in (select email from message_contact where message_id=:message_id)");
                $query->bindValue("message_id",$message_id);
                $query->execute();
                
                $msg = "Alert sent to " . (count($androidToken)) . " Android devices and " . count($iOSToken) . " iOS devices.";
            } else {
                $msg = "Alert created but couldn't send to all users.";
            }
        }else {
            $msg = "Empty Message!";
        }
        
        $debug=array(/*'apnids'=>$apnids,'gcmids'=>$gcmids,'title'=>$title,'push'=>$push*/);
        return array('success'=>$success,'msg'=>$msg,'debug'=>$debug);
}


function sendNotificationIosFCM( $device_ids, $message, $title ){
        
   /*  $headers = array(
        'Authorization: key=AIzaSyCo35x9nRiMPewDu6GtPgvR6HeQttY_0gs',
        'Content-Type: application/json'
    ); */
    
	$headers = array(
			'Authorization: key=AIzaSyDaXnlp7kjLKSQbO_dLpgH9UY3BOAvnjbU',
			'Content-Type: application/json'
	);
    
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array (
        'registration_ids' => $device_ids,
        'priority' => 'high',
        'notification'  => array('body' => $message, 'title' => $title, 'sound'=> 'default', 'badge' => 1),
        'data'  => array('priority'=>'high')
    );
    // Open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    $res = json_decode($result, true);
    if(curl_errno($ch)){
       echo 'error:' . curl_error($c);
    }
    curl_close($ch);    
    return $res;
}

function sendNotificationAndroidFCM( $device_ids, $message, $title){
   
    /* $headers = array(
        'Authorization: key= AIzaSyCo35x9nRiMPewDu6GtPgvR6HeQttY_0gs',
        'Content-Type: application/json'
    ); */
    $headers = array(
    		'Authorization: key=AIzaSyDaXnlp7kjLKSQbO_dLpgH9UY3BOAvnjbU',
    		'Content-Type: application/json'
    );
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        'registration_ids' => $device_ids,
        'data' => array('message' => $message , 'title' => $title )
    );
    
    // Open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    $res = json_decode($result, true);
    if(curl_errno($ch)){
       echo 'error:' . curl_error($c);
    }
    curl_close($ch);    
    return $res;
}
