<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>
<?php require_once "common/functions.php"; ?>
<?php require_once 'common/phpmailer/PHPMailerAutoload.php'; ?>

<?php
$edit=isset($_GET['edit'])?$_GET['edit']:false;
$message=isset($_GET['message'])?$_GET['message']:false;
$sendResultMsg=isset($_GET['sendResultMsg'])?$_GET['sendResultMsg']:false;
$checkboxSelect="{}";

$message_data=array("subject"=>"Login Credentials for Northland CDEM App","message"=>"");
$message_contacts=array();

if (isset($_POST['select_not_loggedin']) && $_POST['select_not_loggedin'] === "true" ) {
	
    $subject=isset($_POST['subject'])?trim($_POST['subject']):false;
    $message_post=isset($_POST['message'])?trim($_POST['message']):false;
    $contacts=isset($_POST['contacts'])?$_POST['contacts']:array();
    
    $sql = "select email, first_name, last_name from contact where (email like '%@%.%' and last_login is null)";
    
    if (count($contacts)>0) {
        $inQuery = implode(',', array_fill(0, count($contacts), '?'));
        $sql = $sql . " or email in (".$inQuery.")";
    }
    
    $query=$conn->prepare($sql);
    
    if (count($contacts)>0) {
        foreach($contacts as $k => $email) {
            $query->bindValue(($k+1), $email);
        }
    }
    
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_ASSOC);

    $checkboxSelect = "";
    foreach($results as $message_contact) {
        $message_contacts[]=array("email"=>$message_contact['email'],"first_name"=>$message_contact['first_name'],"last_name"=>$message_contact['last_name']);
        $checkboxEmail = str_replace("'", "\'", $message_contact['email']);
        if ($checkboxSelect === "") {
            $checkboxSelect = "{'".$checkboxEmail."':true";
        } else {
            $checkboxSelect .= ",'".$checkboxEmail."':true";
        }
    }
    $checkboxSelect .= "}";
    
    $message_data=array("subject"=>$subject,"message"=>$message_post);
    $edit="search";
    

    	
    	
} else if (isset($_POST['subject'])) {
    $subject=isset($_POST['subject'])?trim($_POST['subject']):false;
    $message_post=isset($_POST['message'])?trim($_POST['message']):false;
    $contacts=isset($_POST['contacts'])?$_POST['contacts']:array();
    
    $valid=false;

    $subject_empty=!($subject&&$subject!=='');
    $message_empty=!($message_post&&$message_post!=='');

    if ($subject_empty && $message_empty) {
        $message="invalid-subject-message";
    } else if ($subject_empty) {
        $message="invalid-subject";
    } /*else if ($message_empty) {
        $message="invalid-message";
    } */ else if (count($contacts)==0) {
        $message="no-contacts";
    } else {
        $valid=true;
    }

    if ($valid) {
        
        $mail = new PHPMailer();
        //$mail->IsSendmail(); // telling the class to use SendMail transport
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        
        $mail->Host = $SMTP_HOST;
        $mail->Port = $SMTP_PORT; // or 587
//        $mail->IsHTML(true);
        $mail->Username = $SMTP_USER;
        $mail->Password = $SMTP_PASSWORD;

        $contacts = array_unique($contacts);

        $inQuery = implode(',', array_fill(0, count($contacts), '?'));
        $query=$conn->prepare("select first_name, last_name, email, password from contact where email in (".$inQuery.")");
        foreach($contacts as $k => $email) {
            $query->bindValue(($k+1), $email);
        }
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);

        foreach($results as $message_contact) {
            $body = $message_post;
            $firstName = $message_contact['first_name'];
            $lastName = '';
            $email = $message_contact['email'];
            $password = $message_contact['password'];
            
            $body .= "\nUsername: $email\nPassword: $password\nDownload the Northland CDEM Contacts App by searching Northland CDEM in the AppStore or Google Play store. Or if you are reading this from your mobile device, click this link. ".$APP_STORE_LINK;
            
            $mail->ClearAllRecipients( ); // clear all   
            $mail->AddAddress($email, $firstName.' '.$lastName);
            
            $mail->Subject=$subject;
            $mail->AddReplyTo($EMAIL_REPLY_TO);
            $mail->setFrom($EMAIL_FROM_ADDRESS, 'CDEM Contacts Admin');
//            $mail->MsgHTML( addslashes($body) );
            $mail->Body = $body;           
            
            $mail->send();
        }
        
        header("Location:/admin/messages.php?message=emailsuccess");
        exit;
    } else {
        $message_data=array("subject"=>$subject,"message"=>$message_post);

        if (count($contacts)>0) {
            $inQuery = implode(',', array_fill(0, count($contacts), '?'));
            $query=$conn->prepare("select email, first_name, last_name from contact where email in (".$inQuery.")");
            foreach($contacts as $k => $email) {
                $query->bindValue(($k+1), $email);
            }
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);

            $checkboxSelect = "";
            foreach($results as $message_contact) {
                $message_contacts[]=array("email"=>$message_contact['email'],"first_name"=>$message_contact['first_name'],"last_name"=>'');
                $checkboxEmail = str_replace("'", "\'", $message_contact['email']);
                if ($checkboxSelect === "") {
                    $checkboxSelect = "{'".$checkboxEmail."':true";
                } else {
                    $checkboxSelect .= ",'".$checkboxEmail."':true";
                }
            }
            $checkboxSelect .= "}";
        }

        $edit="search";
    }
} else if (!$edit) {
    header("Location:/admin/send_user_credentials.php?edit=search");
    exit;
}

$disabled=$edit?"":"disabled='disabled'";

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Send User Credentials</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'messages'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Send User Credentials
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="messages.php">Messages</a></li>
                        <li class="active">Send User Credentials</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($message && ($message==="invalid-subject" || $message==="invalid-subject-message")) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Subject required
                            </div>
                            <?php } ?>
                            <?php if ($message && ($message==="invalid-message" || $message==="invalid-subject-message")) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Message content required
                            </div>
                            <?php } ?>
                            
                            <?php if ($message && $message==="no-contacts") { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                No Contacts selected
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="addsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Email sent. <?=$sendResultMsg?>
                            </div>
                            <?php } ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Message Details</h3>
                                </div>
                                <div class="box-body">
                                    <?php if ($edit) { 
                                        ?>
                                    <form id="message_form" role="form" method="POST" action="send_user_credentials.php">
                                    <?php } ?>
                                    <div>
                                        <label>Contacts</label>
                                        <div>
                                            <ul id="contactslist" class="csv">
                                            <?php
                                            foreach($message_contacts as $message_contact) {
                                                ?>
                                                <li id="contactslist_<?=$message_contact['email']?>"><?=$message_contact['first_name']?> <?=$message_contact['last_name']?> (<?=$message_contact['email']?>)</li>
                                                <?php
                                            }
                                            ?>
                                            </ul>
                                        </div>
                                        <?php
                                        foreach($message_contacts as $message_contact) {
                                            ?>
                                            <input type="hidden" name="contacts[]" value="<?=$message_contact['email']?>"/>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php if ($edit) { ?>
                                    <span><h4 class="box-title">Select Contacts</h4> (<a id="contacts_showhide">show</a>) - (<a onclick="selectAllNotLoggedIn();">Select All Users that haven't Logged in</a>)</span>
                                    <table id="contactstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Organisation</th>
                                                <th>Department</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Login</th>
                                                <th>fullname</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Organisation</th>
                                                <th>Department</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Login</th>
                                                <th>fullname</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br/>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" name="subject" class="form-control" placeholder="Enter ..." value="<?=$message_data['subject']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Body</label>
                                        <textarea name="message" class="form-control" rows="3" placeholder="Enter ..." <?=$disabled?> ><?=$message_data['message']?></textarea>
                                        <p>Note: the following is automatically included below your body message</p>
                                        <p>Username: {{email@example.com}}<br>Password: {{password}}<br>Download the Northland CDEM Contacts App by searching Northland CDEM in the AppStore or Google Play store. Or if you are reading this from your mobile device, click this link. <?=$APP_STORE_LINK?></p>
                                        
                                        
                                        
                                    </div>
                                    <?php if ($edit) { ?>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    
                                    <?php if ($edit) {
                                        switch ($edit) {
                                            case "search":
                                                $cancel_href="messages.php";
                                                break;
                                            case "contacts":
                                                $cancel_href="contacts.php";
                                                break;
                                            case "dashboard":
                                                $cancel_href="index.php";
                                                break;
                                            default:
                                                $cancel_href="messages.php";
                                        }
                                        ?>
                                        <a class="btn btn-app" href="<?=$cancel_href?>">
                                            <i class="fa fa-times"></i> Cancel
                                        </a>
                                        <a id="save_link" class="btn btn-app">
                                            <i class="fa fa-arrow-right"></i> Send
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class="btn btn-app" href="messages.php">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            <?php if ($edit) { ?>
            var checkboxSelect = <?=$checkboxSelect?>;
            $(function() {
                $('#contactstable').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_contacts_sendcred.php",
                    "order": [[1, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        {'searchable':false,'orderable':false},
                        null,
                        {"visible":false},
                        null,
                        {"visible":false},
                        null,
                        {"visible":false},
                        {"visible":false},
                        null,
                        {"visible":false}
                      ]
                    ,
                    "columnDefs": [ {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          return "<input type='checkbox' value='"+row[6]+"' />";
                      }
                    } ]
                } );
                $('#contactstable').on( 'draw.dt', function () {
                    $('#contactstable input[type="checkbox"]').change(function () {
                        selectContact($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#contactstable input[type="checkbox"]').each(function() {
                        if (checkboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                
                $('#contactstable_wrapper').hide();
                $('#contacts_showhide').click(function() {
                    tableClick(this, $('#contactstable_wrapper'));
                });
            });
            $('#save_link').click(function() {
                $('#message_form').submit();
            });
            
            function selectContact(email, checked, tr) {
                checkboxSelect[email]=checked;
                if (checked) {
                    if ($('#contactslist li[id="contactslist_'+email+'"]').length == 0) {
                        var tds = $(tr).find('td');
                        var firstName = $(tds[1]).text();
                        var lastName = $(tds[2]).text();
                        $('#contactslist').append($('<li>', {
                            id: 'contactslist_'+email,
                            text: firstName+' '+lastName+' ('+email+')'
                        })).after('<input type="hidden" name="contacts[]" value="'+email+'"/>');
                    }
                } else {
                    $('#contactslist li[id="contactslist_'+email+'"]').remove();
                    $('input[type="hidden"][value="'+email+'"]').remove();
                    $('#contactstable input[type="checkbox"]').each(function() {
                        if ($(this).val()===email) {
                            $(this).prop('checked', false);
                        }
                    });
                }
            }
            function tableClick(link, table) {
                if ($(link).text()==="show") {
                    $(table).show();
                    $(link).text("hide");
                } else {
                    $(table).hide();
                    $(link).text("show");
                }
            }
            function selectAllNotLoggedIn() {
                var theForm = document.getElementById('message_form');
                addHidden(theForm, 'select_not_loggedin', 'true');
                theForm.submit();
            }
            function addHidden(theForm, key, value) {
                // Create a hidden input element, and append it to the form:
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = key;
                input.value = value;
                theForm.appendChild(input);
            }
            <?php } ?>
        </script>

    </body>
</html>
