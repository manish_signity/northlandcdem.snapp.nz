// Snapp Mobile - HBRC CDEM emergency contacts app
//console.log = function(){};


angular.module('hbrc', ['ionic', 'hbrc.controllers', 'hbrc.services', 'ngRoute', 'ngCookies', 'chieffancypants.loadingBar', 'ngAnimate', 'angular-toArrayFilter'], function($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
            value = obj[name];

            if(value instanceof Array) {
                for(i=0; i<value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value instanceof Object) {
                for(subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
})

    .run(function($rootScope, $ionicPlatform, $state, $window, $ionicPopup, $ionicHistory, $http) {

        //console.log = function(){};

        $ionicPlatform.ready(function() {
            window.shouldRotateToOrientation = function (degrees) {
                return false;
            };

            if(window.cordova && window.cordova.plugins.Keyboard) {
                window.cordova.plugins.Keyboard.disableScroll(true);
            }

            if ('addEventListener' in document) {
                document.addEventListener('DOMContentLoaded', function() {
                    FastClick.attach(document.body);
                }, false);
            }

            $ionicPlatform.registerBackButtonAction(backButtonHandler, 100);

            document.addEventListener("resume", appReturnedFromBackground, false);

        });

        console.log(typeof(Storage)!=="undefined");

        Storage.prototype.setArray = function(key, obj) {
            seen = [];
            return this.setItem(key, JSON.stringify(obj, function(key, val) {
                    if (val != null && typeof val == "object") {
                        if (seen.indexOf(val) >= 0) {
                            return;
                        }
                        seen.push(val);
                    }
                    return val;
                })
            );
        };
        Storage.prototype.getArray = function(key) {
            return JSON.parse(this.getItem(key));
        };
        Storage.prototype.removeArray = function(key) {
            return this.removeItem(key);
        };

        // get user globals
        $rootScope.globals = (window.localStorage.getArray('globals')) ? window.localStorage.getArray('globals') : {};

        function appReturnedFromBackground() {
            //This function is called when the app has returned from the background
            if (typeof $rootScope.globals.currentUser != 'undefined') {
                console.log('resume from background, re-run startup');
                $state.go('entry');
            }
        }


        function backButtonHandler() {
            console.log('backButtonHandler: ' + window.location.hash);
            if ((window.location) && (window.location.hash) && ($state.current == 'main.contacts')) {

                var alertPopup = $ionicPopup.confirm({
                    title: 'Exit App',
                    template: 'Are you sure you want to exit?',
                    cancelText: 'Cancel',
                    okText: 'Exit'
                });
                alertPopup.then(function(res) {
                    if (res) navigator.main.exitApp();
                });
//                  navigator.notification.confirm('Are you sure you want to exit?', onConfirm, 'Exit App', ['Exit','Cancel']);
            } else {
                $ionicHistory.goBack()
            }
        };
    })

    .config(function($controllerProvider, $provide, $compileProvider, $stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider, cfpLoadingBarProvider) {

        $ionicConfigProvider.views.transition('none');
        $ionicConfigProvider.backButton.text(' ').icon('icon ion-ios-arrow-back');
        //$httpProvider.defaults.withCredentials = true;
        cfpLoadingBarProvider.includeSpinner = false;

        $stateProvider

            .state('splash', {
                cache: false,
                url : '/splash',
                templateUrl : 'views/sign-in/splash.html',
                controller : 'SplashPageController'
            })
            .state('entry', {
                cache: false,
                url : '/entry',
                templateUrl : 'views/sign-in/entry.html',
                controller : 'EntryPageController'
            })
            .state('auth', {
                cache: false,
                url : '/auth',
                templateUrl : 'views/sign-in/auth.html',
                controller : 'AuthPageController'
            })
            .state('download', {
                cache: false,
                url : '/download',
                templateUrl : 'views/sign-in/download.html',
                controller : 'DownloadPageController'
            })

            .state('main', {
                url : '/main',
                templateUrl : 'views/main.html',
                abstract : true,
                controller : 'MainController'
            })

            .state('main.profile', {
                url : '/profile',
                cache: false,
                views: {
                    'main': {
                        templateUrl: 'views/sign-in/profile.html',
                        controller: 'ProfilePageController'
                    }
                }
            })
            .state('main.contacts', {
                url: '/contacts',
                views: {
                    'main': {
                        templateUrl: 'views/contacts/contacts-search.html',
                        controller : 'ContactsPageController'
                    }
                }
            })
            .state('main.contactDetail', {
                url: '/contacts/:id',
                views: {
                    'main': {
                        templateUrl: 'views/contacts/contact-detail.html',
                        controller : 'ContactDetailPageController'
                    }
                }
            })
            .state('main.groups', {
                url: '/groups',
                views: {
                    'main': {
                        templateUrl: 'views/contacts/groups-search.html',
                        controller : 'GroupsPageController'
                    }
                }
            })
            .state('main.groupDetail', {
                url: '/groups/:id',
                cache: false,
                views: {
                    'main': {
                        templateUrl: 'views/contacts/group-detail.html',
                        controller : 'GroupDetailPageController'
                    }
                }
            })
            .state('main.favourites', {
                url: '/favourites',
                cache: false,
                views: {
                    'main': {
                        templateUrl: 'views/contacts/favourites-search.html',
                        controller : 'FavouritesPageController'
                    }
                }
            })

            .state('main.messageCompose', {
                url: '/message-compose',
                views: {
                    'main': {
                        templateUrl: 'views/message/compose.html',
                        controller : 'MessageComposePageController'
                    }
                }
            })
            .state('main.messageInbox', {
                url: '/message-inbox',
                views: {
                    'main': {
                        templateUrl: 'views/message/inbox.html',
                        controller : 'MessageInboxPageController'
                    }
                }
            })
            .state('main.messageSent', {
                cache: false,
                url: '/message-sent',
                views: {
                    'main': {
                        templateUrl: 'views/message/sent.html',
                        controller : 'MessageSentPageController'
                    }
                }
            })
            .state('main.messageTrash', {
                cache: false,
                url: '/message-trash',
                views: {
                    'main': {
                        templateUrl: 'views/message/trash.html',
                        controller : 'MessageTrashPageController'
                    }
                }
            })
            .state('main.messageDetail', {
                url: '/message-detail/:id',
                views: {
                    'main': {
                        templateUrl: 'views/message/detail.html',
                        controller : 'MessageDetailPageController'
                    }
                }
            })
        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/splash');
    });

