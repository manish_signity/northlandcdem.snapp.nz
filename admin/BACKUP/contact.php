<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>
<?php require_once "common/functions.php"; ?>

<?php
global $CDEM_ROLES;

$edit=isset($_GET['edit'])?$_GET['edit']:false;
$message=isset($_GET['message'])?$_GET['message']:false;
$id="";

$contact=$private_data=$non_private_data=$devices=$groups=array();

if (isset($_POST['id'])) {
    $id=$_POST['id'];
    
    $viewPrivate=isset($_POST['view_private'])?$_POST['view_private']:"0";
    
    if ($id==="new") {
        $new_email=isset($_POST['email'])?$_POST['email']:false;
        
        $valid=false;
        
        if ($new_email) {
            $query=$conn->prepare("select email from contact where email=:email");
            $query->bindValue("email",$_POST['email']);
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);
            if (empty($results)) {
                $valid=true;
            } else {
                $message="invalid-email-used";
            }
        } else {
            $message="invalid-email";
        }
        
        if ($valid) {
            $password=isset($_POST['pass'])?$_POST['pass']:false;
            
            if (!$password) {
                $password=generatePassword(6);
            }
            
            $query=$conn->prepare("insert into contact (email,password,first_name,last_name,company,department,job_title,mobile_phone,phone,view_private,private_data,non_private_data,active,last_modified,last_modified_by) "
                    . "values(:email,:password,:first_name,:last_name,:company,:department,:job_title,:mobile_phone,:phone,:view_private,:private_data,:non_private_data,1,now(),:admin_id)");
            $query->bindValue("email",$_POST['email']);
            $query->bindValue("password",$password);
            $query->bindValue("first_name",$_POST['first_name']);
            $query->bindValue("last_name",$_POST['last_name']);
            $query->bindValue("company",$_POST['company']);
            $query->bindValue("department",$_POST['department']);
            $query->bindValue("job_title",$_POST['job_title']);
            $query->bindValue("mobile_phone",$_POST['mobile_phone']);
            $query->bindValue("phone",$_POST['phone']);
            $query->bindValue("view_private",$viewPrivate);
            $query->bindValue("private_data",$_POST['private_data']);
            $query->bindValue("non_private_data",$_POST['non_private_data']);
            $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
            $query->execute();

            header("Location:/admin/contact.php?id=".$_POST['email']."&message=addsuccess");
            exit;
        } else {
            $contact=array("email"=>$_POST['email'],"password"=>$_POST['pass'],"first_name"=>$_POST['first_name'],"last_name"=>$_POST['last_name'],
            "company"=>$_POST['company'],"department"=>$_POST['department'],"job_title"=>$_POST['job_title'],"mobile_phone"=>$_POST['mobile_phone'],"phone"=>$_POST['phone'],
            "view_private"=>$viewPrivate,"private_data"=>$_POST['private_data'],"non_private_data"=>$_POST['non_private_data']);
            
            $edit="search";
        }
        
    } else {
        $password=isset($_POST['pass'])?$_POST['pass']:false;

        if (!$password) {
            $password=generatePassword(6);
        }
            
        $query=$conn->prepare("update contact set first_name=:first_name, last_name=:last_name, company=:company, department=:department, job_title=:job_title, mobile_phone=:mobile_phone, phone=:phone, view_private=:view_private, private_data=:private_data, non_private_data=:non_private_data, password=:password, last_modified=now(), last_modified_by=:admin_id where email=:email");
        $query->bindValue("first_name",$_POST['first_name']);
        $query->bindValue("last_name",$_POST['last_name']);
        $query->bindValue("company",$_POST['company']);
        $query->bindValue("department",$_POST['department']);
        $query->bindValue("job_title",$_POST['job_title']);
        $query->bindValue("mobile_phone",$_POST['mobile_phone']);
        $query->bindValue("phone",$_POST['phone']);
        $query->bindValue("view_private",$viewPrivate);
        $query->bindValue("private_data",$_POST['private_data']);
        $query->bindValue("non_private_data",$_POST['non_private_data']);
        $query->bindValue("password",$password);
        $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
        $query->bindValue("email",$id);
        $query->execute();
        
        header("Location:/admin/contact.php?id=".$id."&message=success");
        exit;
    }
    
    
} else if (isset($_GET['id'])) {
    $id=$_GET['id'];
    if ($id==="new") {
        $contact=array("email"=>"","password"=>"","first_name"=>"","last_name"=>"",
            "company"=>"","department"=>"","job_title"=>"","mobile_phone"=>"","phone"=>"","view_private"=>"0",
            "private_data"=>"[]","non_private_data"=>"[]");

    } else {
        $query=$conn->prepare("select * from contact where email=:email");
        $query->bindValue("email",$id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
            $contact=$results[0];
        }
                
        $query=$conn->prepare(
                 "select d.platform, d.version, d.last_modified "
                ."from contact_device cd, device d "
                ."left join apns_devices ad on d.apn_id = ad.pid "
                ."where d.id=cd.device_id and cd.email=:email "
                ."and (ad.status is null or ad.status = 'active')");
        $query->bindValue("email",$id);
        $query->execute();
        $devices=$query->fetchAll(PDO::FETCH_ASSOC);
        
        $query=$conn->prepare(
                 "select g.id, g.name from group_contact gc, groups g "
                ."where gc.group_id=g.id "
                ."and gc.email=:email");
        $query->bindValue("email",$id);
        $query->execute();
        $groups=$query->fetchAll(PDO::FETCH_ASSOC);
        
    }
    $private_data=json_decode($contact['private_data'], true);
    $non_private_data=json_decode($contact['non_private_data'], true);
        
} else if (isset($_GET['delete_id'])) {
    
    $query=$conn->prepare("delete ignore from message_contact where email=:delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    $query=$conn->prepare("delete ignore from group_contact where email=:delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    $query=$conn->prepare("delete ignore from contact_device where email=:delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    $query=$conn->prepare("delete ignore from contact where email=:delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    header("Location:/admin/contacts.php?message=deletesuccess");
    exit;
}


$disabled=$edit?"":"disabled='disabled'";
$email_disabled=$id==="new"?"":"disabled='disabled'";
$enterPlaceholder=$edit?"Enter ...":"";

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Contact</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'contacts'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $edit?"Edit":"View"; ?> Contact
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="contacts.php">Contacts</a></li>
                        <li class="active"><?php echo $edit?"Edit":"View"; ?> Contact</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($message && $message==="invalid-email") { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Invalid email address
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="invalid-email-used") { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Invalid email address - existing contact found
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="success") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Contact updated successfully
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="addsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Contact added successfully
                            </div>
                            <?php } ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Contact Details</h3>
                                </div>
                                <div class="box-body">
                                    <?php if ($edit) { ?>
                                    <form id="contact_form" role="form" method="POST" action="contact.php">
                                        <input type="hidden" name="id" value="<?=$id?>">
                                    <?php } ?>
                                        
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['email']?>" <?=$email_disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <p class="help-block">Leave blank for random generation.</p>
                                        <input type="text" name="pass" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['password']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['first_name']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['last_name']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Organisation</label>
                                        <input type="text" name="company" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['company']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Department</label>
                                        <input type="text" name="department" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['department']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Job Title</label>
                                        <input type="text" name="job_title" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['job_title']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mobile Phone</label>
                                        <input type="text" name="mobile_phone" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['mobile_phone']?>" <?=$disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" class="form-control" placeholder="<?=$enterPlaceholder?>" value="<?=$contact['phone']?>" <?=$disabled?>/>
                                    </div>
                                    
                                    <div id="non_private_data_table">
                                        <?php
                                        $static_non_private_data = array(
                                            "job_title" => "",
                                            //"Co Dept" => "",
                                            "Co Main Phone" => "",
                                            "Co Address" => "",
                                            "Co City" => "",
                                            "Co Region" => "",
                                            "Co Post Code" => "",
                                            "Co Country" => "",
                                            //"Co DDI Phone" => "",
                                        );
                                        
                                        $non_private_data = array_merge($static_non_private_data, $non_private_data);
                                                                                
                                        $non_private_data_row_count=0;
                                        foreach($non_private_data as $key => $value) {
                                            if($key === 'Co Dept' || $key === 'Co DDI Phone') continue;
                                        
                                            if($key === 'selected_cdem_group') {
                                                $selected_cdem_group = $value;
                                                continue;
                                            }
                                            if($key === 'selected_cdem_group_child') {
                                                $selected_cdem_group_child = $value;
                                                continue;
                                            }    
                                            ?>
                                            <div class="form-group">
                                                <?php if ($edit) { ?>
                                                    <?php if($key === 'job_title') : ?>
                                                    <div style="display: none">Job Title <input type="hidden" id="npd_key_<?=$non_private_data_row_count?>" value="<?=$key?>"></div>
                                                    <?php else : ?>
                                                    <label><?=$key?> <input type="hidden" id="npd_key_<?=$non_private_data_row_count?>" value="<?=$key?>"/></label>
                                                    <?php endif; ?>
                                                
                                                    <?php if($key === 'job_title') : ?>
                                                    <div style="display: none"><input type="text" id="npd_value_<?=$non_private_data_row_count?>" value="<?=$value?>"></div>
                                                    <?php else : ?>
                                                    <div><input type="text" class="form-control" placeholder="<?=$enterPlaceholder?>" id="npd_value_<?=$non_private_data_row_count?>" value="<?=$value?>"></div>
                                                    <?php endif; ?>
                                                <?php } else { ?>
                                                
                                                <?php if($key != 'job_title') : ?>
                                                    <label><?=$key?></label>
                                                    <div><input type="text" class="form-control" value="<?=$value?>" <?=$disabled?>></div>
                                                <?php endif; ?>
                                                    
                                                <?php } ?>
                                            </div>
                                            <?php
                                            $non_private_data_row_count++;
                                        }
                                        
                                        ?>

                                        <div class="form-group">
                                            <label>CDEM role </label><br/>
                                            <select <?php echo ($edit) ? "":"disabled";?> id="selected_cdem_group" data-selected="<?php echo $selected_cdem_group;?>"></select> 
                                            <select <?php echo ($edit) ? "":"disabled";?> id="selected_cdem_group_child" data-selected="<?php echo $selected_cdem_group_child;?>"></select>
                                        </div>
                                        
                                        <?php
                                        
                                        
                                        if ($edit) {
                                        ?>

                                            <tr id="non_private_data_last_row" style="display:none">
                                                <td><a class="btn btn-default btn-sm" onclick="addNPDRow();">Add Row</a></td>
                                                <td>
                                                    <input type="hidden" name="non_private_data_row_count" value="<?=$non_private_data_row_count?>"> 
                                                    <input type="hidden" name="non_private_data" value="<?=$contact['non_private_data']?>">
                                                </td>
                                            </tr>

                                        <?php } ?>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <?php $view_private_checked=$contact['view_private']==="1"?"checked":""; ?>
                                                <input type="checkbox" name="view_private" value="1" <?=$disabled?> <?=$view_private_checked?>>
                                                Can View Contacts Private Details
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Home Details:</label>
                                        <table id="private_data_table">
                                        <?php
                                        
                                        $static_home_data = array(
                                            "Home Phone" => "",
                                            "Home Address" => "",
                                        );
                                        
                                        $private_data = array_merge($static_home_data, $private_data);
                                        
                                        $private_data_row_count=0;
                                        foreach($private_data as $key => $value) {
                                            ?>
                                            <tr>
                                                <?php if ($edit) { ?>
                                                <td><input type="text" id="pd_key_<?=$private_data_row_count?>" value="<?=$key?>"> : </td>
                                                <td> <input type="text" id="pd_value_<?=$private_data_row_count?>" value="<?=$value?>"></td>
                                                <?php } else { ?>
                                                <td><?=$key?>: </td>
                                                <td><?=$value?></td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                            $private_data_row_count++;
                                        }
                                        if ($edit) {
                                        ?>
                                            <tr id="private_data_last_row">
                                                <td><a class="btn btn-default btn-sm" onclick="addPDRow();">Add Row</a></td>
                                                <td>
                                                    <input type="hidden" name="private_data_row_count" value="<?=$private_data_row_count?>">
                                                    <input type="hidden" name="private_data" value="<?=$contact['private_data']?>">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </table>
                                    </div>
                                    <?php if ($id!=="new") { ?>
                                    <div class="form-group">
                                        <label>Devices:</label>
                                        <ul id="deviceslist">
                                        <?php
                                        foreach($devices as $device) {
                                            ?>
                                            <li><?=$device['platform']?> <?=$device['version']?> (<?=$device['last_modified']?>)</li>
                                            <?php
                                        }
                                        if (count($devices)===0) {
                                            echo '<li>No registered devices.</li>';
                                        }
                                        ?>
                                        </ul>
                                        <?php if (!empty($groups)) { ?>
                                        <label>Groups:</label>
                                        <ul id="groupslist">
                                        <?php
                                            foreach($groups as $group) {
                                                ?>
                                                <li><a href="/admin/group.php?id=<?=$group['id']?>&back=contact|<?=$contact['email']?>"><?=$group['name']?></a></li>
                                                <?php
                                            }
                                        ?>
                                        </ul>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php if ($edit) { ?>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    
                                    <?php if ($edit) {
                                        $cancel_href=$edit==="search"?"contacts.php":"contact.php?id=".$id;
                                        ?>
                                        <a class="btn btn-app" href="<?=$cancel_href?>">
                                            <i class="fa fa-times"></i> Cancel
                                        </a>
                                        <a id="save_link" class="btn btn-app">
                                            <i class="fa fa-save"></i> Save
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class="btn btn-app" href="contacts.php">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                        <a class="btn btn-app" href="contact.php?id=<?=$id?>&edit=view">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            
/*
            var static_cdem_groups = [
                {id: 1, name: 'Governance        ', children: ['Joint Committee Member','CEG Member']},
                {id: 2, name: 'Controller        ', children: ['Group Controller','Local Controller']},
                {id: 3, name: 'Recovery Manager  ', children: ['Group Recovery Manager','Local Recovery Manager']},
                {id: 4, name: 'ECC Staff Member  ', children: ['ECC Controller PA','ECC Response Manager','ECC Health, Safety and Risk Advisor','ECC PIM','ECC Logistics','ECC Operations','ECC Planning','ECC Intelligence','ECC Welfare','ECC Lifelines Utility Coordinator','ECC Recovery']},
                {id: 5, name: 'EOC Staff Member  ', children: ['EOC Controller PA','EOC Response Manager','EOC Health, Safety and Risk Advisor','EOC PIM','EOC Logistics','EOC Operations','EOC Planning','EOC Intelligence','EOC Welfare','EOC Recovery']},
                {id: 6, name: 'Technical Expert  '},
                {id: 7, name: 'Emergency Services', children: ['Police','Fire','Health']},
                {id: 8, name: 'Lifelines         ', children: ['Electricity','3 Waters','Transportation','Communications']},
                {id: 9, name: 'Welfare Response  ', children: ['Registration','Needs Assessment & Registration','Shelter and Accommodation','Animal','Inquiry','Household goods and services','Psychosocial Support','Financial Assistance','Care and protection services for children and young people']}
            ];
*/
            var static_cdem_groups = <?php echo json_encode( $CDEM_ROLES ); ?>;
            
            
            (function($){
            
                console.log(static_cdem_groups);
                var parent = $('#selected_cdem_group'), parent_selected = parent.attr('data-selected'), children = $('#selected_cdem_group_child'), children_selected = children.attr('data-selected');
                
                $('<option/>').html('-- Please Select --').appendTo(parent);
                $(static_cdem_groups).each(function(i,e){
                    $('<option/>').html(e.name.trim()).prop('selected', (parent_selected.trim() == e.name.trim()) ).appendTo(parent);
                });
                
                onParentChange = function(e){
                    var selected = parent.find('option:selected'), selected_children = null;
                    
                    if($(selected).length){
                        $(static_cdem_groups).each(function(i,e){
                            if($(selected).text() == e.name.trim()) selected_children = e.children;
                        });
                    }
                    
                    if(!selected_children) {
                        children.hide();
                    } else {
                        children.empty().show();
                        $('<option/>').html('-- Please Select --').appendTo(children);
                        $(selected_children).each(function(i,e){
                            $('<option/>').html(e.trim()).prop('selected', (children_selected.trim() == e.trim()) ).appendTo(children);
                        });
                    }
                    
                    console.log(selected, $(selected).text(), children);
                };
                
                parent.on('change',onParentChange).trigger('change');
            
            })(jQuery)
            
            
            
            $('#save_link').click(function() {
                buildPD();
                buildNPD();
                $('#contact_form').submit();
            });
            function addPDRow() {
                var countInput = $('#private_data_last_row').find('input').eq(0);
                var count = $(countInput).val();
                $('#private_data_last_row').before('<tr><td><input type="text" id="pd_key_'+count+'" value="">: </td><td><input type="text" id="pd_value_'+count+'" value=""></td></tr>');
                $(countInput).val(parseInt(count)+1);
            };
            function addNPDRow() {
                var countInput = $('#non_private_data_last_row').find('input').eq(0);
                var count = $(countInput).val();
                $('#non_private_data_last_row').before('<tr><td><input type="text" id="npd_key_'+count+'" value="">: </td><td><input type="text" id="npd_value_'+count+'" value=""></td></tr>');
                $(countInput).val(parseInt(count)+1);
            };
            function buildPD() {
                var pd = {};
                var count = parseInt($('#private_data_last_row').find('input').eq(0).val());
                for (var i = 0; i < count; i++) {
                    var key = $('#pd_key_'+i);
                    var value = $('#pd_value_'+i);
                    if (key.length && value.length) {
                        if ($(key).val().trim().length && $(value).val().trim().length) {
                            pd[$(key).val().trim()]=$(value).val().trim();
                        }
                    }
                }
                $('input[name="private_data"]').val(JSON.stringify(pd));
            };
            function buildNPD() {
                var npd = {};
                //var count = parseInt($('#non_private_data_last_row').find('input').eq(0).val());
                var count = $("[id^=npd_value_]").length;
                for (var i = 0; i < count; i++) {
                    var key = $('#npd_key_'+i);
                    var value = $('#npd_value_'+i);
                    console.log(key);
                    if (key.length && value.length) {
                        //if ($(key).val().trim().length && $(value).val().trim().length) {
                            npd[$(key).val().trim()]=$(value).val().trim();
                        //}
                    }
                }
                
                if($('#selected_cdem_group').val() != '-- Please Select --') npd['selected_cdem_group'] = $('#selected_cdem_group').val();
                if($('#selected_cdem_group_child').val() && $('#selected_cdem_group_child').val() != '-- Please Select --') npd['selected_cdem_group_child'] = $('#selected_cdem_group_child').val();
                
                console.log(count, JSON.stringify(npd));
                
                $('input[name="non_private_data"]').val(JSON.stringify(npd));
            };
        </script>

    </body>
</html>
