<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<title>HB CDEM CONTACTS APP - TERMS OF USE</title>
<meta charset="UTF-8">
</head>
<body>
<h1>HB CDEM CONTACTS APP</h1>
<h2>TERMS OF USE</h2>
<p>This app is owned and operated by Hawke's Bay Civil Defence Emergency Managing Group ("HBCDEM") or its agent.
</p>
<p>By downloading or using this contacts application (app), you agree to be bound by these terms– you should make sure therefore that you read them carefully before continuing to use this app.
</p>
<p>Your Obligations
</p>
1.	You acknowledge that the purpose of this app and the information it contains is to support the HBCDEM and its partner agencies in carrying out their roles in reduction, readiness, response and recovery; before, during and after an emergency (the “purpose").
<br/>2.	You will only use this app and the information it contains for the purpose as defined in paragraph 1 above.
<br/>3.	That you are who you say you are and that you are over the age of 18 years.
<br/>4.	You will not use this app and the information it contains illegally or to do anything unlawful or discriminatory.
<br/>5.	You will not pass on or show any other third party any information contained in this app.
<br/>6.	You will not adapt, reproduce, store, print or publish any information contained in this app.
<br/>7.	You will advise HBCDEM if you retire or resign from your civil defence emergency management role or you are no longer the correct person for your organization to be included in this app.
<p>If you no longer wish for any of your information to be published on the app or if your details change, you can update or remove any details from the app or contact HBCDEM.
You acknowledge that HBCDEM is entitled to take all actions that may be reasonably necessary or prudent to comply with any legal requirements or regulatory authorities without incurring any liability to you.
</p>
<h3>Intellectual Property</h3>
<p>HBCDEM is the owner or licencee of all copyright, trademarks and other intellectual property rights in connection with this app, unless specified otherwise.
</p>
<p>HBCDEM Group Obligations
</p>
1.	We will endeavour to provide the app with due care and skill, however HBCDEM does not guarantee the availability or reliability of the app.
<br/>2.	HBCDEM (or its agent) will remedy any unavailability of the app as soon as reasonably practicable.
<br/>3.	HBCDEM may amend these terms of use from time to time.
<h3>Privacy</h3>
<p>This app involves the collection of personal information.  Without these details the app would not be able to fulfill the purpose noted above.  The app will only require you to provide personal information if it is necessary for the purpose.
</p>
1.	HBCDEM will collect personal information through the app.  This personal information will only be used by HBCDEM for the purpose.
<br/>2.	Other users of the app will have access to the personal information for the purpose.  All users of the app are bound by these terms of use, including the obligation to only use the personal information for the purpose.
<br/>3.	HBCDEM will not pass on any personal information provided by an individual through the app unless specifically authorised to do so.
<br/>4.	We have security measures in place to attempt to protect against the loss, misuse and alternation of personal information contained in the app.
<br/>5.	You are entitled to contact HBCDEM at any time to confirm any personal information we hold about you and correct that information. <a href="mailto:cdemapp@hbrc.govt.nz">cdemapp@hbrc.govt.nz</a> 
<br/>6.	If you have a complaint about how we have dealt with your personal information, you can contact us or you can lay a formal complaint with the New Zealand Privacy Commissioner. 
<br/>7.	Further information on privacy legislation is available on the Privacy Commissioner’s website at www.privacy.org.nz.

</body>
</html>
