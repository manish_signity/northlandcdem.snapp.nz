<?php
require_once "authenticate.php";
require_once "../config/dbconnection.php"; 

$auth = authenticate($conn);

if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {
        
        $angularjs_post=json_decode(file_get_contents('php://input'), true);
        $angularjs_post_debug=isset($angularjs_post_debug['debug'])?$angularjs_post_debug['debug']:false;
        $post_debug=isset($_POST['debug'])?$_POST['debug']:false;
                
        $type=isset($_POST['type'])?$_POST['type']:false;
        $subject=isset($_POST['subject'])?$_POST['subject']:false;
        $body=isset($_POST['body'])?$_POST['body']:false;
        $to_contacts=isset($_POST['to_contacts'])?$_POST['to_contacts']:false;
        $to_groups=isset($_POST['to_groups'])?$_POST['to_groups']:false;
        $contacts=$to_contacts?explode(",", $to_contacts):array();
        $groups=$to_groups?explode(",", $to_groups):array();
        
        $errors=array();
        
        if (empty($contacts) && empty($groups)) {
            $errors[]="No recipients found";
        }
        if (!$type) {
            $errors[]="No message type set";
        }
        if (!$body) {
            $errors[]="No message content found";
        }
        
        if (!empty($errors)) {
            $return['success']=0;
            $return['message']=implode(",", $errors);
            
        } else {
            if (count($groups)>0) {
                $inQuery = implode(',', array_fill(0, count($groups), '?'));
                $query=$conn->prepare("select email from group_contact where group_id in (".$inQuery.")");
                foreach($groups as $k => $group_id) {
                    $query->bindValue(($k+1), $group_id);
                }
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_ASSOC);

                foreach($results as $contact) {
                    $contacts[]=$contact['email'];
                }
            }
            
            $contacts = array_unique($contacts);
            
            if (empty($contacts)) {
                $return['success']=0;
                $return['message']="No recipients found";
            } else {
                if ($type==="push") {

                    $query=$conn->prepare("insert into message (title,message,sender) values(:title,:message,:sender)");
                    $query->bindValue("title",$subject);
                    $query->bindValue("message",$body);
                    $query->bindValue("sender",$email);
                    $query->execute();
                    $message_id = $conn->lastInsertId();
                    
                    $query=$conn->prepare("insert into message_contact (message_id,email) values(:message_id,:email)");
                    $query->bindValue("message_id",$message_id);
                    foreach($contacts as $email) {
                        $query->bindValue("email",$email);
                        $query->execute();
                    }

                    $db_details=getDBDetails();
                                        
                    require_once "../admin/common/send_message.php"; 
                    $sendResult = sendMessage($conn, $db_details, $message_id);
                    
                    $return['message']="Message sent. ".$sendResult['msg'];
                            
                } else if ($type==="email") {
                    $return['success']=0;
                    $return['message']="TODO.. haven't done this yet :-)";
                } else {
                    $return['success']=0;
                    $return['message']="Unsupported type provided '".$type."'";
                }
            }
        }
        
        if ($angularjs_post_debug || $post_debug) {
            $debug=array(
                'angularjs_post'=>$angularjs_post,
                'post'=>$_POST,
                'contacts'=>$contacts,
                'groups'=>$groups,
                'type'=>$type,
                'subject'=>$subject,
                'body'=>$body
            );
            $return['debug']=$debug;
        }
        
    }

    echo json_encode($return);
}

exit;