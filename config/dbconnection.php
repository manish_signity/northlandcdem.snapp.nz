<?php
require_once('config.php');
try {
	$dsn = 'mysql:host='.$DB_HOST.';dbname='.$DB_DATABASE.';charset=utf8';
	$conn = new PDO($dsn, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
	echo 'ERROR: ' . $e->getMessage();
}

function getDBDetails() {
    global $DB_HOST;
    global $DB_USER;
    global $DB_PASSWORD;
    global $DB_DATABASE;
    return array("host"=>$DB_HOST,"user"=>$DB_USER,"password"=>$DB_PASSWORD,"dbname"=>$DB_DATABASE);
}

?>
