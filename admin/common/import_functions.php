<?php
require "functions.php";

function checkUpload($input_name, $max_file_size) {
    $upload_error = $upload_sha1 = null;
    try {

        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($_FILES[$input_name]['error']) ||
            is_array($_FILES[$input_name]['error'])
        ) {
            throw new RuntimeException('Invalid parameters.');
        }

        // Check $_FILES[$input_name]['error'] value.
        switch ($_FILES[$input_name]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file found.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unknown errors.');
        }

        // You should also check filesize here. 
        if (isset($max_file_size) && $_FILES[$input_name]['size'] > $max_file_size) {
            throw new RuntimeException('Exceeded filesize limit.');
        }

        // You should name it uniquely.
        // DO NOT USE $_FILES[$input_name]['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $upload_sha1 = sha1_file($_FILES[$input_name]['tmp_name']);
        if (!move_uploaded_file(
            $_FILES[$input_name]['tmp_name'],
            sprintf('./uploads/%s.%s',
                $upload_sha1,
                'csv'
            )
        )) {
            throw new RuntimeException('Failed to move uploaded file.');
        }


    } catch (RuntimeException $e) {

        $upload_error = $e->getMessage();

    }
    return array('error'=>$upload_error, 'sha1'=>$upload_sha1);
}

function importToStage($csv, $conn) {
    $existingPasswords=array();
//    // add existing contacts to import_stage table
//    $query=$conn->prepare("insert into import_stage (email, password, first_name, last_name, company, department, mobile_phone, phone, private_data, non_private_data, admin_id) "
//            . "select email, password, first_name, last_name, company, department, mobile_phone, phone, private_data, non_private_data, :admin_id from contact");
//    $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
//    $query->execute();

    $query=$conn->prepare("select email, password from contact");
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $result) {
        $existingPasswords[strtolower($result['email'])]=$result['password'];
    }
    
    $uniqueEmails = array();
    
    foreach($csv->data as $contact) {
        $email=$dummy_email=null;
        $first_name=$last_name=$company=$department="";
        $mobile_phone=$ddi_phone=$main_phone=$phone="";
        $private_data=$non_private_data=array();
        
        foreach($contact as $key=>$value) {
            $key=trim($key);
            $value=trim($value);
            if ($value==="") {
                continue;
            }
            switch (strtolower($key)) {
                case "job title":
                case "cdem role":
                case "cdem sub-role":
                    // ignore these entries
                    break;
                case "email address":
                    $email=$value;
                    break;
                case "first":
                    $first_name=$value;
                    break;
                case "last":
                    $last_name=$value;
                    break;
                case "company name":
                    $company=$value;
                    break;
                case "department":
                    $department=$value;
                    break;
                case "mobile phone":
                    $mobile_phone=$value;
                    break;
                default:
                    if (0 === strpos(strtolower($key), 'home')) {
                        $private_data[$key]=(string)$value;
                    } else {
                        $non_private_data[$key]=(string)$value;
                        if (strtolower($key)==="ddi phone") $ddi_phone=$value;
                        if (strtolower($key)==="main phone") $main_phone=$value;
                    }
            }
        }
        // prioritise, DDI Phone else Main Phone else blank
        $phone=$ddi_phone!==""?$ddi_phone:$main_phone;
        
        // check for blank or duplicate emails
        $setDummyEmail = false;
        if ($email===null || !isValidEmail($email) || in_array(strtolower($email),$uniqueEmails)) {
            // blank email, invalid email, or duplicate email
            $setDummyEmail = true;
        } else {
            // email hasn't been seen yet, add to email array
            $uniqueEmails[] = strtolower($email);
        }
        if ($setDummyEmail) {
            $dummy_email = setDummyEmail($first_name, $last_name, $uniqueEmails);
        }
        
        $password=array_key_exists(strtolower($email),$existingPasswords)?$existingPasswords[strtolower($email)]:generatePassword(6);
        
        $encoded_private_data = json_encode($private_data);
        $encoded_non_private_data = json_encode($non_private_data);
        
        $query=$conn->prepare("insert into import_stage (email, dummy_email, password, first_name, last_name, company, department, mobile_phone, phone, private_data, non_private_data, admin_id)"
                . "values (:email,:dummy_email,:password,:first_name,:last_name,:company,:department,:mobile_phone,:phone,:private_data,:non_private_data,:admin_id)");
        $query->bindValue("email",$email);
        $query->bindValue("dummy_email",$dummy_email);
        $query->bindValue("password",$password);
        $query->bindValue("first_name",$first_name);
        $query->bindValue("last_name",$last_name);
        $query->bindValue("company",$company);
        $query->bindValue("department",$department);
        $query->bindValue("mobile_phone",$mobile_phone);
        $query->bindValue("phone",$phone);
        $query->bindValue("private_data", $encoded_private_data==="[]"?"{}":$encoded_private_data);
        $query->bindValue("non_private_data", $encoded_non_private_data==="[]"?"{}":$encoded_non_private_data);
        $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
        $count=$query->execute();
        
    }
}

function importGroups($csv, $conn) {
    $users=array();
    
    $query=$conn->prepare("select email, first_name, last_name from contact");
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $result) {
        $name = $result['first_name'].' '.$result['last_name'];
        $users[strtolower(trim($name))]=$result['email'];
    }
    
    $groups=array();
    foreach($csv->titles as $group) {
        if ($group==="INDIVIDUALS - NO GROUP") {
            continue;
        }
        
        $query=$conn->prepare("insert ignore into groups (name, last_modified, last_modified_by) values (:groupName, now(), :adminId)");
        $query->bindValue("groupName",$group);
        $query->bindValue("adminId",$_SESSION["loggedInUserId"]);
        $query->execute();
        
        $groups[$group]=array();
    }
    
    $noMatchContacts=array();
    foreach($csv->data as $row) {
        foreach($groups as $name=>&$array) {
            $contact = $row[$name];
            if ($contact && $contact !== '') {
                if (array_key_exists(strtolower($contact), $users)) {
                    $email = $users[strtolower($contact)];
                    $array[]=$email;
                } else {
                    if (array_key_exists($contact, $noMatchContacts)) {
                        $noMatchContacts[$contact][] = $name;
                    } else {
                        $noMatchGroups = array();
                        $noMatchGroups[] = $name;
                        $noMatchContacts[$contact] = $noMatchGroups;
                    }
                }
            }
        }
    }
    
    foreach($groups as $name=>$emails) {
        $query=$conn->prepare("select id from groups where name = :groupName");
        $query->bindValue("groupName",$name);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        $groupId = $results[0]['id'];
        
        foreach($emails as $email) {
            $query=$conn->prepare("insert ignore into group_contact values (:groupId, :email)");
            $query->bindValue("groupId",$groupId);
            $query->bindValue("email",$email);
            $query->execute();
        }
        
    }
    
    return $noMatchContacts;
}

function setDummyEmail($first_name, $last_name, &$uniqueEmails) {
    $joint_names = trim($first_name).".".trim($last_name);
    $dummy_email = $joint_names;
    $count = 1;
    while(in_array(strtolower($dummy_email),$uniqueEmails)) {
        $dummy_email = $joint_names . $count++;
    }
    $uniqueEmails[] = strtolower($dummy_email);
    return $dummy_email;
}

function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function getImportStageErrorRows($conn) {
    $missingResult=$duplicateResult=array();
    
    $query=$conn->prepare("select * from import_stage where admin_id=:admin_id and email is null");
    $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
    $query->execute();
    $missingResult=$query->fetchAll(PDO::FETCH_ASSOC);

    $query=$conn->prepare("select * from import_stage where admin_id=:admin_id and email in (select email from import_stage where admin_id=:admin_id2 and email is not null group by email having count(*) > 1) order by email, dummy_email, id");
    $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
    $query->bindValue("admin_id2",$_SESSION["loggedInUserId"]);
    $query->execute();
    $duplicateResult=$query->fetchAll(PDO::FETCH_ASSOC);
    
//    array_walk_recursive($missingResult, function(&$value, $key) {
//        if (is_string($value)) {
//            $value = utf8_encode($value);
//        }
//    });
//    array_walk_recursive($duplicateResult, function(&$value, $key) {
//        if (is_string($value)) {
//            $value = utf8_encode($value);
//        }
//    });
    
    return array('missing'=>$missingResult, 'duplicate'=>$duplicateResult);
}

function attemptImport($conn) {
    $result = getImportStageErrorRows($conn);
    
    if (count($result['missing']) == 0 && count($result['duplicate']) == 0) {
        // import into contacts table
        $query=$conn->prepare("delete from contact where email not in (select email from import_stage where admin_id=:admin_id)");
        $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
        $query->execute();
        
        $query=$conn->prepare(
                "insert into contact (email,password,first_name,last_name,company,department,mobile_phone,phone,private_data,non_private_data,active,last_modified,last_modified_by) ".
                "select email, password, first_name, last_name, company, department, mobile_phone, phone, private_data, non_private_data, 1, now(), admin_id ".
                "from import_stage where admin_id=:admin_id ".
                "on duplicate key update first_name=values(first_name), last_name=values(last_name), company=values(company), department=values(department), mobile_phone=values(mobile_phone), phone=values(phone), ".
                "private_data=values(private_data), non_private_data=values(non_private_data), last_modified=values(last_modified), last_modified_by=values(last_modified_by) ");
        $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
        $query->execute();
        
    }
    
    return $result;
}

function editImportStage($conn, $postdata) {
    $stage_results = getImportStageErrorRows($conn);
    
    foreach($stage_results['missing'] as $missing) {
        if (!isset($postdata['displayonly-'.$missing['id']]) && trim($postdata['email-'.$missing['id']]) != '') {
            $query=$conn->prepare("update import_stage set email = :email where id = :id");
            $query->bindValue("email",trim($postdata['email-'.$missing['id']]));
            $query->bindValue("id",$missing['id']);
            $query->execute();
        } else if (isset($postdata['displayonly-'.$missing['id']])) {
            $query=$conn->prepare("update import_stage set email = :dummy_email where id = :id");
            $query->bindValue("dummy_email",$missing['dummy_email']);
            $query->bindValue("id",$missing['id']);
            $query->execute();
        }
    }
    
    foreach($stage_results['duplicate'] as $duplicate) {
        if (!isset($postdata['displayonly-'.$duplicate['id']]) && trim($postdata['email-'.$duplicate['id']]) != '') {
            $query=$conn->prepare("update import_stage set email = :email where id = :id");
            $query->bindValue("email",trim($postdata['email-'.$duplicate['id']]));
            $query->bindValue("id",$duplicate['id']);
            $query->execute();
        } else if (isset($postdata['displayonly-'.$duplicate['id']])) {
            $query=$conn->prepare("update import_stage set email = :dummy_email where id = :id");
            $query->bindValue("dummy_email",$duplicate['dummy_email']);
            $query->bindValue("id",$duplicate['id']);
            $query->execute();
        }
    }
    
    // re-set dummy email
    $query=$conn->prepare("update import_stage set dummy_email = null where admin_id=:admin_id");
    $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
    $query->execute();
    
    $query=$conn->prepare("select distinct email from import_stage where admin_id=:admin_id and email is not null");
    $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
    $query->execute();
    $uniqueEmailResult=$query->fetchAll(PDO::FETCH_ASSOC);
    
    $uniqueEmails = array();
    
    foreach($uniqueEmailResult as $result) {
        $uniqueEmails[] = $result['email'];
    }
    
    $stage_results = getImportStageErrorRows($conn);
    
    foreach($stage_results['missing'] as $missing) {
        $dummy_email = setDummyEmail($missing['first_name'], $missing['last_name'], $uniqueEmails);
        $query=$conn->prepare("update import_stage set dummy_email = :dummy_email where id = :id");
        $query->bindValue("dummy_email",$dummy_email);
        $query->bindValue("id",$missing['id']);
        $query->execute();
    }
    
    $currentDuplicate = null;
    foreach($stage_results['duplicate'] as $duplicate) {
        if ($currentDuplicate === null || $currentDuplicate !== $duplicate['email']) {
            $currentDuplicate = $duplicate['email'];
            continue;
        }
        $dummy_email = setDummyEmail($duplicate['first_name'], $duplicate['last_name'], $uniqueEmails);
        $query=$conn->prepare("update import_stage set dummy_email = :dummy_email where id = :id");
        $query->bindValue("dummy_email",$dummy_email);
        $query->bindValue("id",$duplicate['id']);
        $query->execute();
    }
}

function exportCSV($conn) {
    
    $query=$conn->prepare("select email, first_name, last_name, company, department, mobile_phone, phone, private_data, non_private_data from contact");
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_ASSOC);
    
    $mainHeaders = array("Email","First Name","Last Name","Company Name","Department","Mobile Phone","Phone","Job Title","Selected CDEM Role","Selected CDEM Role Child","Groups");
    $nonPrivateHeaders = array("Co Dept","Co Address","Co City","Co Region","Co Post Code","Co Country","Co DDI Phone","Co Main Phone");
    $privateHeaders = array("Home Phone","Home Address");
    
    $headers = array_merge($mainHeaders, $nonPrivateHeaders, $privateHeaders);
    $fixedHeadersSize = count($headers);
    
    $array=array( $headers );
    
    $extraDataByEmail = array();
        
    foreach ($results as $contact) {
        
        $non_private_data = json_decode($contact['non_private_data'], true);
        $private_data = json_decode($contact['private_data'], true);
        $groupNames = array();
        
        // fixed columns
        $selectedCdemGroup = findInArray($non_private_data, 'selected_cdem_group');
        $selectedCdemGroupChild = findInArray($non_private_data, 'selected_cdem_group_child');
        $coDept = findInArray($non_private_data, 'Co Dept');
        $coAddress = findInArray($non_private_data, 'Co Address');
        $coCity = findInArray($non_private_data, 'Co City');
        $coRegion = findInArray($non_private_data, 'Co Region');
        $coPostCode = findInArray($non_private_data, 'Co Post Code');
        $coCountry = findInArray($non_private_data, 'Co Country');
        $coDDIPhone = findInArray($non_private_data, 'Co DDI Phone');
        $coMainPhone = findInArray($non_private_data, 'Co Main Phone');
        $coJobTitle = findInArray($non_private_data, 'job_title');
        $homePhone = findInArray($private_data, 'Home Phone');
        $homeAddress = findInArray($private_data, 'Home Address');
        
        $query=$conn->prepare(
                 "select g.id, g.name from group_contact gc, groups g "
                ."where gc.group_id=g.id "
                ."and gc.email=:email");
        $query->bindValue("email",$contact['email']);
        $query->execute();
        $groups=$query->fetchAll(PDO::FETCH_ASSOC);
        
        foreach($groups as $group) {
            $groupNames[] = $group['name'];
        }
        
        
        $contactArray = array(
            $contact['email'],
            $contact['first_name'],
            $contact['last_name'],
            $contact['company'],
            $contact['department'],
            $contact['mobile_phone'],
            $contact['phone'],
            $coJobTitle,
            $selectedCdemGroup,
            $selectedCdemGroupChild,
            implode('|', $groupNames),
            $coDept,
            $coAddress,
            $coCity,
            $coRegion,
            $coPostCode,
            $coCountry,
            $coDDIPhone,
            $coMainPhone,
            $homePhone,
            $homeAddress
        );
        
        $extraDataByEmail[$contact['email']] = array("non_private_data"=>$non_private_data, "private_data"=>$private_data);
                
        // append to end of array
        $array[] = $contactArray;
    }
    
    
    // check for dynamic columns
    $dynamicHeaders = array();
    
    foreach ($array as &$contactArray) {
        if ($contactArray[0] === "Email") {
            continue; // header row
        }
        $extraData = $extraDataByEmail[$contactArray[0]];
        $non_private_data = $extraData['non_private_data'];
        
        // check for dynamic columns
        foreach ($non_private_data as $key => $value) {
            if ($key === "selected_cdem_group" || $key === "selected_cdem_group_child" || $key === "job_title") {
                continue;
            }
            if (array_key_exists($key, $dynamicHeaders)) {
                // if a dynamic header value
                $dynIndex = $dynamicHeaders[$key];
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            } else if (!in_array($key, $nonPrivateHeaders)) {
                // if not a fixed header, new dynamic value
                $dynIndex = count($dynamicHeaders);
                $array[0][] = $key;
                $dynamicHeaders[$key] = $dynIndex;
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            }
        }
    }
        
    // reset dynamic headers for private data
    $fixedHeadersSize += count($dynamicHeaders);
    $dynamicHeaders = array();
        
    foreach ($array as &$contactArray) {
        if ($contactArray[0] === "Email") {
            continue; // header row
        }
        $extraData = $extraDataByEmail[$contactArray[0]];
        $private_data = $extraData['private_data'];
        
        // check for dynamic columns
        foreach ($private_data as $key => $value) {
            if (array_key_exists($key, $dynamicHeaders)) {
                // if a dynamic header value
                $dynIndex = $dynamicHeaders[$key];
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            } else if (!in_array($key, $privateHeaders)) {
                // if not a fixed header, new dynamic value
                $dynIndex = count($dynamicHeaders);
                $array[0][] = $key;
                $dynamicHeaders[$key] = $dynIndex;
                $contactArray[$fixedHeadersSize+$dynIndex] = $value;
            }
        }
    }
    
    fillArrayWithBlanks($array);
    
//    var_dump($array);
    echo array2csv($array);
}

function findInArray($array, $key) {
    return $array && array_key_exists($key, $array) ? cleanArrayValue($array[$key]) : '';
}

function cleanArrayValue($value) {
    return str_replace("\n", " ", $value);
}

function fillArrayWithBlanks(&$array) {
    $headerSize = count($array[0]);
    
    foreach($array as &$contactsArray) {
        if (count($contactsArray) < $headerSize) {
            for ($i = 0; $i < $headerSize; $i++) {
                if (!array_key_exists($i, $contactsArray)) {
                    $contactsArray[$i] = "";
                }
            }
        }
        ksort($contactsArray);
    }
}

function array2csv(array &$array) {
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   //fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}