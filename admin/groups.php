<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php

$message=isset($_GET['message'])?$_GET['message']:false;
$select=isset($_GET['select'])?$_GET['select']:false;
$id=isset($_GET['id'])?$_GET['id']:false;
$back=isset($_GET['back'])?$_GET['back']:false;
$back_query=$back?"&back=".$back:"";


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Groups</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'groups'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Groups
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Groups</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($message && $message==="deletesuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Group deleted successfully
                            </div>
                            <?php } ?>
                            <div class="box">
                                <?php if (!$select) { ?>
                                <div class="box-links">
                                    <a class="link" href="group.php?id=new&edit=search">Add New Group</a>
                                </div><!-- /.box-header -->
                                <?php } ?>
                                <div class="box-body table-responsive">
                                    <?php 
                                        $select_action=$select==="parent_group"?"group":$select;
                                        if ($select) { ?>
                                    <form id="select_form" role="form" method="POST" action="<?=$select_action?>.php">
                                        <input type="hidden" name="id" value="<?=$id?>">
                                        <input type="hidden" name="select_from" value="group">
                                        <input type="hidden" name="select" value="<?=$select?>">
                                        <?php if ($back) { ?>
                                        <input type="hidden" name="back" value="<?=$back?>">
                                        <?php } ?>
                                    <?php } ?>
                                    <table id="groupstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>child count</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>child count</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <?php if ($select) { ?>
                                    <h3 class="box-title">Selected Groups</h3>
                                    <table id="selectgroupstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                        
                                        <tbody>
                                        </tbody>
                                        
                                    </table>
                                    <p>
                                        <a class="btn btn-warning" href="group.php?id=<?=$id?><?=$back_query?>">Back</a>
                                        <button type="submit" class="btn btn-primary">Add Groups</button>
                                    </p>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('#groupstable').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_groups.php",
                    "order": [[3, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        <?php echo $select?"{'searchable':false,'orderable':false}":"{\"visible\":false}"; ?>,
                        { "visible":    false}, 
                        { "visible":    false}, 
                        null,
                        null,
                        { "visible":    false}, 
                        { "visible":    false}, 
                        null,
                        null,
                        <?php echo $select?"{\"visible\":false}":"null"; ?>
                      ],
                    "columnDefs": [ {
                      "targets": [ 9 ],
                      "data": function(row, type, val, meta) {
                          return "<a class='link' href='group.php?id="+row[1]+"'>View</a>"
                                +" | <a class='link' href='group.php?id="+row[1]+"&edit=search'>Edit</a>";
                      }
                    }, {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          var select = '<?php echo $select; ?>';
                          if (!select) {
                              return "";
                          } else if (select==="parent_group") {
                              return row[1]===forId || row[2]!==null?"":"<input type='checkbox' value='"+row[1]+"' />";
                          } else {
                              return row[1]===forId?"":"<input type='checkbox' value='"+row[1]+"' />";
                          }
                      }
                    } ]
                } );
                <?php if($select) { ?>
                $('#groupstable').on( 'draw.dt', function () {
                    $('#groupstable input[type="checkbox"]').change(function () {
                        selectGroup($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#groupstable input[type="checkbox"]').each(function() {
                        if (checkboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                <?php } ?>
            });
            var forId = '<?php echo $id; ?>';
            <?php if($select) { ?>
            var checkboxSelect = {};
            function selectGroup(groupId, checked, tr) {
                checkboxSelect[groupId]=checked;
                if (checked) {
                    $(tr).find('td:first-child').remove();
                    $('<td><a href="#" onclick="selectContact(\''+groupId+'\', false, null);">Remove</a><input type="hidden" name="selects[]" value="'+groupId+'"/></td>').appendTo(tr);
                    tr[0].groupId=groupId;
                    
                    $('#selectgroupstable tbody').append(tr);
                } else {
                    var toRemove;
                    $('#selectgroupstable tbody tr').each(function() {
                        if (this.groupId===groupId) {
                            toRemove=this;
                        }
                    });
                    if (toRemove) {
                        $(toRemove).remove();
                        $('#groupstable input[type="checkbox"]').each(function() {
                            if ($(this).val()===groupId) {
                                $(this).prop('checked', false);
                            }
                        });
                    }
                }
            }
            <?php } ?>
        </script>

    </body>
</html>
