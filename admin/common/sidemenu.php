<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li <?php echo ($page == 'index') ? "class='active'" : ""; ?>>
                <a href="index.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li <?php echo ($page == 'contacts') ? "class='active'" : ""; ?>>
                <a href="contacts.php">
                    <i class="fa fa-user"></i> <span>Contacts</span>
                </a>
            </li>
            <li <?php echo ($page == 'groups') ? "class='active'" : ""; ?>>
                <a href="groups.php">
                    <i class="fa fa-group"></i> <span>Groups</span>
                </a>
            </li>
            <li <?php echo ($page == 'messages') ? "class='active'" : ""; ?>>
                <a href="messages.php">
                    <i class="fa fa-comment"></i> <span>Messages</span>
                </a>
            </li>
            <li <?php echo ($page == 'admins') ? "class='active'" : ""; ?>>
                <a href="admins.php">
                    <i class="fa fa-gears"></i> <span>Admin Users</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>