<?php
require_once('../../config/config.php');

// SQL server connection information
$sql_details = array(
    'user' => $DB_USER,
    'pass' => $DB_PASSWORD,
    'db'   => $DB_DATABASE,
    'host' => $DB_HOST
);
 
// DB table to use
$table = 'messages_view';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id', 'dt' => 0 ),
    array( 'db' => 'title',  'dt' => 1 ),
    array( 'db' => 'message',   'dt' => 2 ),
    array( 'db' => 'sender',     'dt' => 3 ),
    array( 'db' => 'admin_sender',     'dt' => 4 ),
    array( 'db' => 'contact_count',   'dt' => 5 ),
    array(
        'db'        => 'sent',
        'dt'        => 6,
        'formatter' => function( $d, $row ) {
            return $d==null?'':date( 'jS M Y h:i a', strtotime($d));
        }
    )
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);