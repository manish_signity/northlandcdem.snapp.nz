<?php
    if(isset($alerts)){
        foreach($alerts as $alert){
            $alertClass="info";
            $iClass="info";
            switch($alert['level']){
                case "error":
                    $alertClass="danger";
                    $iClass="ban";
                    break;
                case "warn":
                    $alertClass="warning";
                    $iClass="warning";
                    break;
                case "info":
                    $alertClass="info";
                    $iClass="info";
                    break;
                case "success":
                    $alertClass="success";
                    $iClass="check";
                    break;
            }
            ?>
            <div class="alert alert-<?=$alertClass?> alert-dismissable">
                <i class="fa fa-<?=$iClass?>"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?=$alert['title']?></b> <?=$alert['message']?>
            </div>
            <?php
        }
    }
    