<?php
require_once "authenticate.php";
require_once "../config/dbconnection.php"; 

$auth = authenticate($conn);

function addColumn(&$values, $col_name, $value) {
    $values[]=array('col_name'=>$col_name,'value'=>$value);
}

if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {
        $post_vars['device_id'] = '';
        $post_vars['device_token']= '';
        $post_vars['platform'] = 'iso';
        $post_vars=$_POST;
        
        $device_id=isset($post_vars['device_id'])?$post_vars['device_id']:false;
        $device_token=isset($post_vars['device_token'])?$post_vars['device_token']:false;
        $platform=isset($post_vars['platform'])?$post_vars['platform']:'';
        
        $values=array();
        
       // if ($device_id) {
            addColumn($values, 'device_id', $device_id);
        //}
        //if ($device_token) {
            addColumn($values, 'device_token', $device_token);
        //}
        //if ($platform) {
            addColumn($values, 'platform', $platform);
        //}
        
        if (!empty($values)) {
            $update_sql="update contact set last_modified=now(), last_modified_by=null,";

            $col_names=array();
            foreach($values as $data) {
                $col_names[]=" ".$data['col_name']."=:".$data['col_name'];
            }
            $update_sql.=implode(",",$col_names);
            
            $update_sql.=" where email=:email";
            
            $query=$conn->prepare($update_sql);
            foreach($values as $data) {
                $query->bindValue($data['col_name'],$data['value']);
            }
            $query->bindValue("email",$email);
            $query->execute();
            $return['message']="Profile updated. ";
            
        } else {
            $return['message']="No profile data to update. ";
        }
    }

    echo json_encode($return);
}

exit;