<?php
require_once('../../config/config.php');
require_once ("../../config/dbconnection.php");
require_once ("../common/functions.php"); 

// SQL server connection information
$sql_details = array(
    'user' => $DB_USER,
    'pass' => $DB_PASSWORD,
    'db'   => $DB_DATABASE,
    'host' => $DB_HOST
);
 
// DB table to use
$table = 'contacts_view';
 
// Table's primary key
$primaryKey = 'email';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'first_name', 'dt' => 1 ),
    array( 'db' => 'last_name',  'dt' => 2 ),
    array( 'db' => 'company',   'dt' => 3 ),
    array( 'db' => 'department',     'dt' => 4 ),
    array( 'db' => 'job_title',     'dt' => 5 ),
    array( 'db' => 'mobile_phone',     'dt' => 6 ),
    array( 'db' => 'email',   'dt' => 7 ),
    array( 'db' => 'phone',     'dt' => 8 ),
    array(
        'db'        => 'last_modified',
        'dt'        => 9,
        'formatter' => function( $d, $row ) {
            return date( 'jS M Y h:i a', strtotime($d));
        }
    ),
    array( 'db' => 'fullname',     'dt' => 10 )
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );

$ssp = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

//$index = 9;
//
//var_dump($ssp['data'][$index]);

array_walk_recursive(
    $ssp, function (&$value) {
        if (is_string($value)) {
            $value = ($value);
        }
    }
);

$query=$conn->prepare("Select T1.email, T2.id, GROUP_CONCAT(T2.name) AS gname FROM group_contact as T1 inner join groups AS T2 ON T1.group_id = T2.id GROUP BY T1.email");

$query->execute();
$groupsData = $query->fetchAll(PDO::FETCH_ASSOC);
$groupsArr = array();
if(!empty($groupsData)){
    foreach ($groupsData as $key => $value) {
        $groupsArr[trim($value['email'])] =  $value['gname'];
    }
} 


foreach ($ssp['data'] as $key => $value) {
    if (array_key_exists($value[7],$groupsArr))
    {
        $ssp['data'][$key][11] = $groupsArr[$value[7]];
    } else {
        $ssp['data'][$key][11] = '';
    }
}

//var_dump($ssp['data'][$index]);
//
//echo json_encode($ssp['data'][$index]);

echo json_encode( $ssp );