<?php

function addErrorAlert($title, $message) {
    addAlert("error", $title, $message);
}
function addWarnAlert($title, $message) {
    addAlert("warn", $title, $message);
}
function addInfoAlert($title, $message) {
    addAlert("info", $title, $message);
}
function addSuccessAlert($title, $message) {
    addAlert("success", $title, $message);
}

function addAlert($level, $title, $message) {
    global $alerts;
    if(!isset($alerts)) $alerts = array();
    $alerts[] = array("level"=>$level,"title"=>$title,"message"=>$message);
}

function getRandomBytes($nbBytes = 32) {
    $bytes = openssl_random_pseudo_bytes($nbBytes);
    if (false !== $bytes) {
        return $bytes;
    }
    else {
        throw new \Exception("Unable to generate secure token from OpenSSL.");
    }
}

function generatePassword($length){
    return substr(preg_replace("/[^a-zA-Z0-9]/", "", base64_encode(getRandomBytes($length+1))),0,$length);
}

function isNullOrEmptyString($value) {
    return $value === null || trim($value) === "";
}