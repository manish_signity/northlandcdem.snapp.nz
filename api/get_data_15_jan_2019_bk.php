<?php 
require_once "authenticate.php"; 
require_once "../config/dbconnection.php"; 

$auth = authenticate($conn);

if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {        
        $super=$viewPrivate=false;

        $query=$conn->prepare("select count(*) from groups, group_contact where groups.id=group_contact.group_id and groups.super=1 and group_contact.email=:email ");
        $query->bindValue("email",$email);
        $query->execute();
        if ( $query->fetchColumn(0) > 0 ){
            $super=true;
        }
        $query=$conn->prepare("select view_private from contact where email=:email ");
        $query->bindValue("email",$email);
        $query->execute();
        if ( $query->fetchColumn(0) == 1 ){
            $viewPrivate=true;
        }

        $contacts=array();
        $groups_view=array();
        $group_contacts=array();
        $child_groups=array();
        $groups=array();

        $default_non_private_data = array(
            "Co Main Phone" => " ",
            "Co Address" => " ",
            "Co City" => " ",
            "Co Region" => " ",
            "Co Post Code" => " ",
            "Co Country" => " ",
        );
        
        if ($super) {
            // user part of super group, so can see all contacts
            // if view private set, can see private data, otherwise can't
            //$query=$conn->prepare("select email, first_name, last_name, company, department, mobile_phone, phone,".($viewPrivate ? " private_data,":"")." non_private_data from contact where active=1 ");
            // MARK: change query to get all, then filter out private data in foreach on line 43
            $query=$conn->prepare("select email, first_name, last_name, company, department, job_title, mobile_phone, phone, private_data, non_private_data from contact where active=1 ");
            $query->execute();
            $contact_results=$query->fetchAll(PDO::FETCH_ASSOC);

            $super_non_private_data = array(
                "Home Address" => " ",
                "Home Phone" => " "
            );

            foreach($contact_results as $c_result) {
                $c_result['fullname']=$c_result['first_name']." ".$c_result['last_name'];
                $c_result['company']= ($c_result['company']);
                
                $non_private_data_arr = json_decode($c_result['non_private_data'], true);
                
                $result = array_merge($default_non_private_data, $super_non_private_data, $non_private_data_arr);
                $result = array_unique($result);
                
                foreach($result as $key => $value) {
                    if($key === 'Co Dept' || $key === 'Co DDI Phone') unset($result[$key]);
                }
                
                $c_result['non_private_data'] = json_encode( $result );
                
                $c_result['private_data'] = ($super || $viewPrivate || $email == $c_result['email']) ? $c_result['private_data']:null; // filter out private data if not $viewPrivate or this users contact
                $contacts[$c_result['email']]=$c_result;
            }

//            $query=$conn->prepare("select * from group_view ");
//            $query->execute();
//            $group_view_results=$query->fetchAll(PDO::FETCH_ASSOC);
//
//            foreach($group_view_results as $group_view) {
//                $groups_view[$group_view['group_id']]['view'][]=$group_view['view_group_id'];
//            }

            $query=$conn->prepare("select * from group_contact ");
            $query->execute();
            $group_contact_results=$query->fetchAll(PDO::FETCH_ASSOC);

            foreach($group_contact_results as $group_contact) {
                $group_contacts[$group_contact['group_id']]['contacts'][]=$group_contact['email'];
            }

            $query=$conn->prepare("select id, parent_id, name, super from groups ");
            $query->execute();
            $groups_results=$query->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($groups_results as $g_result) {
//                $g_result['view']=array_key_exists($g_result['id'], $groups_view)?$groups_view[$g_result['id']]['view']:array();
                $g_result['contacts']=array_key_exists($g_result['id'], $group_contacts)?$group_contacts[$g_result['id']]['contacts']:array();
                
                if($g_result['name'] == '') continue;
                
                if ($g_result['parent_id']===null) {
                    $g_result['children']=array();
                    $groups[$g_result['id']]=$g_result;
                } else {
                    $child_groups[]=$g_result;
                }
            }

            foreach($child_groups as $child_group) {
                if($child_group['name'] == '') continue;
                $groups[$child_group['parent_id']]['children'][]=$child_group;
            }
            
        } else {
            // user not in super group, can only see users in own group, group child groups or group view groups
            // if view private set, can see private data, otherwise can't
            // contacts from users group and any child groups
            $query=$conn->prepare("select distinct c.email, c.first_name, c.last_name, c.company, c.department, c.mobile_phone, c.phone,".($viewPrivate ? " c.private_data,":"")." c.non_private_data "
                    . "from group_contact gc "
                    . "left join groups g on gc.group_id=g.id or gc.group_id=g.parent_id "
                    . "left join group_contact cgc on g.id=cgc.group_id "
                    . "left join contact c on cgc.email=c.email and c.active=1 "
                    . "where gc.email=:email "
                    . "and c.email is not null ");
            $query->bindValue("email",$email);
            $query->execute();
            $contact_results=$query->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($contact_results as $c_result) {
                $c_result['fullname']=$c_result['first_name']." ".$c_result['last_name'];

                $non_private_data_arr = json_decode($c_result['non_private_data'], true);
                $result = array_merge($default_non_private_data, $non_private_data_arr);
                                
                foreach($result as $key => $value) {
                     if($key === 'Co Dept' || $key === 'Co DDI Phone' || $key === 'Home Address' || $key === 'Home Phone') unset($result[$key]);
                }
                $result = array_unique($result);
                
                $c_result['non_private_data'] = json_encode( $result );

                $contacts[$c_result['email']]=$c_result;
            }
            
            // contacts from users group views
            $query=$conn->prepare("select distinct c.email, c.first_name, c.last_name, c.company, c.department, c.job_title, c.mobile_phone, c.phone,".($viewPrivate ? " c.private_data,":"")." c.non_private_data "
                    . "from group_contact gc "
                    . "left join groups g on gc.group_id=g.id "
                    . "left join group_view gv on g.id=gv.group_id "
                    . "left join group_contact cgvc on gv.view_group_id=cgvc.group_id "
                    . "left join contact c on cgvc.email=c.email and c.active=1 "
                    . "where gc.email=:email "
                    . "and c.email is not null ");
            $query->bindValue("email",$email);
            $query->execute();
            $contact_view_results=$query->fetchAll(PDO::FETCH_ASSOC);

            foreach($contact_view_results as $c_result) {
                if (!array_key_exists($c_result['email'], $contacts)) {
                    $c_result['fullname']=$c_result['first_name']." ".$c_result['last_name'];
                    $contacts[$c_result['email']]=$c_result;
                }
            }
                        
            //group data
            
            $query=$conn->prepare(
                      "select g.id, g.parent_id, g.name, g.super from groups g, group_contact gc where g.id=gc.group_id and gc.email=:email1 "
                    . "union distinct "
                    . "select g.id, g.parent_id, g.name, g.super from groups g, group_contact gc where g.parent_id=gc.group_id and gc.email=:email2 "
                    . "union distinct "
                    . "select g.id, g.parent_id, g.name, g.super from groups g, group_view gv, group_contact gc where g.id=gv.view_group_id and gv.group_id=gc.group_id and gc.email=:email3 ");
            $query->bindValue("email1",$email);
            $query->bindValue("email2",$email);
            $query->bindValue("email3",$email);
            $query->execute();
            $contacts_group_results=$query->fetchAll(PDO::FETCH_ASSOC);
            
            $query=$conn->prepare(
                      "select cgc.* from group_contact cgc, group_contact gc where cgc.group_id=gc.group_id and gc.email=:email1 "
                    . "union distinct "
                    . "select cgc.* from group_contact cgc, groups g, group_contact gc where cgc.group_id=g.id and g.parent_id=gc.group_id and gc.email=:email2 "
                    . "union distinct "
                    . "select cgc.* from group_contact cgc, group_view gv, group_contact gc where cgc.group_id=gv.view_group_id and gv.group_id=gc.group_id and gc.email=:email3 ");
            $query->bindValue("email1",$email);
            $query->bindValue("email2",$email);
            $query->bindValue("email3",$email);
            $query->execute();
            $contacts_group_contacts_results=$query->fetchAll(PDO::FETCH_ASSOC);
            
            $contacts_group_contacts=array();
            
            foreach($contacts_group_contacts_results as $result) {
                $contacts_group_contacts[$result['group_id']][]=$result['email'];
            }
            
            $groups=array();
            $child_groups=array();
            
            foreach($contacts_group_results as $result) {
                $result['contacts']=array_key_exists($result['id'], $contacts_group_contacts)?$contacts_group_contacts[$result['id']]:array();

                if($result['name'] == '') continue;

                if ($result['parent_id']===null) {
                    $result['children']=array();
                    $groups[$result['id']]=$result;
                } else {
                    $child_groups[$result['id']]=$result;
                }
            }
            
            foreach($child_groups as $child_group) {
                if($child_group['name'] == '') continue;
 
                if (array_key_exists($child_group['parent_id'], $groups)) {
                    $groups[$child_group['parent_id']]['children'][]=$child_group;
                } else {
                    $groups[$child_group['id']]=$child_group;
                }
            }
                        
            // add themselves with all data
            $query=$conn->prepare("select distinct c.email, c.first_name, c.last_name, c.company, c.department, c.job_title, c.mobile_phone, c.phone, c.private_data, c.non_private_data "
                . "from contact c "
                . "where c.email=:email ");
            $query->bindValue("email",$email);
            $query->execute();
            $contact_results=$query->fetchAll(PDO::FETCH_ASSOC);

            foreach($contact_results as $c_result) {
                $c_result['fullname']=$c_result['first_name']." ".$c_result['last_name'];
                $contacts[$c_result['email']]=$c_result;
            }
            
        }
        
        $return['data']['contacts'] = $contacts;
        $return['data']['groups'] = $groups;
        $return['data']['cdem_roles'] = $CDEM_ROLES;
    }

    header('Content-type: application/json;charset=utf-8;');
    echo json_encode($return);
}

exit;
