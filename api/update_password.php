<?php
require_once "../config/dbconnection.php";  

$query=$conn->prepare("select * from contact");
//$query->bindValue("email",$A1['email']);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_ASSOC);

if (!empty($results)) {
    foreach ($results as $key => $value) {
        $email = $value['email'];
        $pass = generateRandomString(6);
        $update_sql="update contact set password = '".$pass."'";
        $update_sql.=" where email=:email";
        $query=$conn->prepare($update_sql);
        $query->bindValue("email",$email);
        $query->execute();
        $return['message']="Profile updated. ";
    }
        
} else {
    $return['message']="No profile data to update. ";
}

function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
    