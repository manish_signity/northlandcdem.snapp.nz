<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php
$edit=isset($_GET['edit'])?$_GET['edit']:false;
$message=isset($_GET['message'])?$_GET['message']:false;
$back=isset($_GET['back'])?$_GET['back']:false;
$id="";

$group=array();
$contacts=array();


if(isset($_POST['del_contacts_ids']) && !empty($_POST['del_contacts_ids']) && (isset($_POST['id'])) ){
	if ($id !="new") {
		$id=$_POST['id'];
		$post_contacts = $_POST['del_contacts_ids'];
		$post_contacts = explode(",",$post_contacts);
		
		$in = join(',', array_fill(0, count($post_contacts), '?'));
		$post_contacts[]=$id;
		$query=$conn->prepare("delete from group_contact where email  IN ($in) AND group_id=?");
		$query->execute($post_contacts);
	}
}


if (isset($_POST['contacts']) && !empty($_POST['contacts'])) {
	$selects = $_POST['contacts'];
	$id=$_POST['id'];
	foreach($selects as $contact) {
		$query=$conn->prepare("insert ignore into group_contact values(:group_id, :contact_email)");
		$query->bindValue("group_id",$id);
		$query->bindValue("contact_email",$contact);
		$query->execute();
	}
	$back=isset($_POST['back'])?$_POST['back']:false;
	$back_query=$back?"&back=".$back:"";
	header("Location:/admin/group_test.php?id=".$id."&message=".$message.$back_query);
	exit;

}if (isset($_POST['select_from'])) {
    $id=$_POST['id'];
    $select=isset($_POST['select'])?$_POST['select']:"";
    $selects=isset($_POST['selects'])?$_POST['selects']:array();
    $message="";
    if ($_POST['select_from']==="contact") {
        $message="addcontactsuccess";
    
        foreach($selects as $contact) {
            $query=$conn->prepare("insert ignore into group_contact values(:group_id, :contact_email)");
            $query->bindValue("group_id",$id);
            $query->bindValue("contact_email",$contact);
            $query->execute();
        }
    } else if ($_POST['select_from']==="group") {
        if ($select==="parent_group") {
            $message="addchildgroupsuccess";
            
            foreach($selects as $child_group) {
                $query=$conn->prepare("update groups set parent_id=:parent_id where id=:id");
                $query->bindValue("parent_id",$id);
                $query->bindValue("id",$child_group);
                $query->execute();
            }
        } else {
            $message="addgroupsuccess";

            foreach($selects as $group_view) {
                $query=$conn->prepare("insert ignore into group_view values(:group_id, :group_view)");
                $query->bindValue("group_id",$id);
                $query->bindValue("group_view",$group_view);
                $query->execute();
            }
        }
    }
    
    $back=isset($_POST['back'])?$_POST['back']:false;
    $back_query=$back?"&back=".$back:"";
    
    header("Location:/admin/group_test.php?id=".$id."&message=".$message.$back_query);
    exit;
    
} else if (isset($_POST['id'])) {
	
    $id=$_POST['id'];
    $valid=false;
    $super=isset($_POST['super'])?$_POST['super']:"0";
    
    // check group name isn't already used
    $query=$conn->prepare("select name from groups where name=:name".($id==="new"?"":" and id<>:id"));
    $query->bindValue("name",$_POST['name']);
    if ($id!=="new") {
        $query->bindValue("id",$id);
    }
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_ASSOC);
    
    if(empty($_POST['name'])){
        	$message="empty-name-used";
    }else{
		    if (empty($results)) {
		        $valid=true;
		    } else {
		        $message="invalid-name-used";
		    }
    }
    
    $parent_id=isset($_POST['parent_id'])?$_POST['parent_id']:null;
    if ($valid) {
        if ($id==="new") {
            $query=$conn->prepare("insert into groups (parent_id, name, super, last_modified, last_modified_by) values (:parent_id, :name, :super, now(), :admin_id)");
            $query->bindValue("parent_id",$parent_id);
            $query->bindValue("name",$_POST['name']);
            $query->bindValue("super",$super);
            $query->bindValue("admin_id",$_SESSION["loggedInUserId"]);
            $query->execute();
            $id = $conn->lastInsertId();
        } else {
            $query=$conn->prepare("update groups set name=:name, super=:super where id=:id");
            $query->bindValue("name",$_POST['name']);
            $query->bindValue("super",$super);
            $query->bindValue("id",$id);
            $query->execute();
        }
    } else {
        if ($id==="new") {
            $group=array("id"=>"new","parent_id"=>$parent_id,"name"=>$_POST['name'],"super"=>$super);
        } else {
            $query=$conn->prepare(
                    "select groups.id, groups.parent_id, groups.name, groups.super, parent.name as parent, groups.last_modified, admin.name as admin_name from groups left join groups parent on groups.parent_id=parent.id, admin ".
                    "where groups.last_modified_by=admin.id and groups.id=:id");
            $query->bindValue("id",$id);
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($results)) {
                $group=$results[0];
            }
            $group['name']=$_POST['name'];
            $group['super']=$super;
        }
        
        $child_groups=array();
        $contacts=array();
        $group_views=array();
        
        $edit=$id==="new"?"search":"view";
    }

    if (isset($_POST['del_contacts_ids'])) {
        $post_contacts=$_POST['del_contacts_ids'];
        
        if ($valid) {
        	$post_contacts = $_POST['del_contacts_ids'];
        	$post_contacts = explode(",",$post_contacts);
        	 
        	$in = join(',', array_fill(0, count($post_contacts), '?'));
        	$post_contacts[]=$id;
        	$query=$conn->prepare("delete from group_contact where email  IN ($in) AND group_id=?");
        	$query->execute($post_contacts);
        	
        	
            /* $query=$conn->prepare("select email from group_contact where group_id=:id");
            $query->bindValue("id",$id);
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);
            $group_contacts=array();
            foreach($results as $result) {
                $group_contacts[]=$result['email'];
            }

            $new_contacts = array_diff($post_contacts, $group_contacts);
            $query=$conn->prepare("insert into group_contact values (:id, :email)");
            $query->bindValue("id",$id);
            foreach($new_contacts as $new_contact) {
                $query->bindValue("email",$new_contact);
                $query->execute();
            }

            $in = join(',', array_fill(0, count($post_contacts), '?'));
            $post_contacts[]=$id;

            $query=$conn->prepare("delete from group_contact where email not in ($in) and group_id=?");
            $query->execute($post_contacts); */
        } else {
            $in = join(',', array_fill(0, count($post_contacts), '?'));
            $query=$conn->prepare("select contact.* from contact where email in ($in)");
            $query->execute($post_contacts);
            $contacts=$query->fetchAll(PDO::FETCH_ASSOC);
        }
        
    }
    
    if (isset($_POST['childgroups'])) {
        $post_childgroups=$_POST['childgroups'];
        
        if ($valid) {
            $query=$conn->prepare("select id from groups where parent_id=:id");
            $query->bindValue("id",$id);
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);
            $child_groups=array();
            foreach($results as $result) {
                $child_groups[]=$result['id'];
            }

            $new_childgroups = array_diff($post_childgroups, $child_groups);
            if (!empty($new_childgroups)) {
                $in = join(',', array_fill(0, count($new_childgroups), '?'));
                array_unshift($new_childgroups, $id);
                $query=$conn->prepare("update groups set parent_id=? where id in ($in)");
                $query->execute($new_childgroups);
            }

            $remove_childgroups = array_diff($child_groups, $post_childgroups);
            if (!empty($remove_childgroups)) {
                $in = join(',', array_fill(0, count($new_childgroups), '?'));
                $query=$conn->prepare("update groups set parent_id=null where id in ($in)");
                $query->execute($new_childgroups);
            }
        } else {
            $in = join(',', array_fill(0, count($post_childgroups), '?'));
            $query=$conn->prepare("select groups_view.* from groups_view where id in ($in) ");
            $query->execute($post_childgroups);
            $child_groups=$query->fetchAll(PDO::FETCH_ASSOC);
        }
    }
    
    if (isset($_POST['groupviews'])) {
        $post_groupviews=$_POST['groupviews'];
        
        if ($valid) {
            $query=$conn->prepare("select view_group_id from group_view where group_id=:id");
            $query->bindValue("id",$id);
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);
            $group_views=array();
            foreach($results as $result) {
                $group_views[]=$result['view_group_id'];
            }

            $new_groupviews = array_diff($post_groupviews, $group_views);
            $query=$conn->prepare("insert into group_view values (:id, :viewGroupId)");
            $query->bindValue("id",$id);
            foreach($new_groupviews as $new_groupview) {
                $query->bindValue("viewGroupId",$new_groupview);
                $query->execute();
            }

            $in = join(',', array_fill(0, count($post_groupviews), '?'));
            $post_groupviews[]=$id;

            $query=$conn->prepare("delete from group_view where view_group_id not in ($in) and group_id=?");
            $query->execute($post_groupviews);
        } else {
            $in = join(',', array_fill(0, count($post_groupviews), '?'));
            $query=$conn->prepare("select groups_view.* from groups_view where id in ($in) ");
            $query->execute($post_groupviews);
            $group_views=$query->fetchAll(PDO::FETCH_ASSOC);
        }
    }
    
    
    $back=isset($_POST['back'])?$_POST['back']:false;
    $back_query=$back?"&back=".$back:"";

    if ($valid) {
        header("Location:/admin/group_test.php?id=".$id."&message=success".$back_query);
        exit;
    }
   
} else if (isset($_GET['id'])) {
    $id=$_GET['id'];
    
    if ($id==="new") {
        $parent_id=isset($_GET['parent_id'])?$_GET['parent_id']:null;
        $group=array("id"=>"new","parent_id"=>$parent_id,"name"=>"","super"=>"0");
        $child_groups=array();
        $contacts=array();
        $group_views=array();
    } else {
/*
        $query=$conn->prepare(
                "select groups.id, groups.parent_id, groups.name, groups.super, parent.name as parent, groups.last_modified, admin.name as admin_name from groups left join groups parent on groups.parent_id=parent.id, admin ".
                "where groups.last_modified_by=admin.id and groups.id=:id");
*/
        $query=$conn->prepare(
                "select groups.id, groups.parent_id, groups.name, groups.super, parent.name as parent, groups.last_modified, admin.name as admin_name from groups left join groups parent on groups.parent_id=parent.id, admin ".
                "where groups.id=:id");
        $query->bindValue("id",$id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
            $group=$results[0];
        }
        
        $query=$conn->prepare("select groups_view.* from groups_view, groups where groups_view.id=groups.id and groups.parent_id=:id ");
        $query->bindValue("id",$id);
        $query->execute();
        $child_groups=$query->fetchAll(PDO::FETCH_ASSOC);

        $query=$conn->prepare("select contact.* from contact, group_contact where group_contact.email=contact.email and group_contact.group_id=:id ");
        $query->bindValue("id",$id);
        $query->execute();
        $contacts=$query->fetchAll(PDO::FETCH_ASSOC);

        $query=$conn->prepare("select groups_view.* from groups_view, group_view where group_view.view_group_id=groups_view.id and group_view.group_id=:id ");
        $query->bindValue("id",$id);
        $query->execute();
        $group_views=$query->fetchAll(PDO::FETCH_ASSOC);
    }
} else if (isset($_GET['delete_id'])) {
    $query=$conn->prepare("delete ignore from groups where id=:delete_id; update groups set parent_id = null where parent_id = :delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    header("Location:/admin/groups.php?message=deletesuccess");
    exit;
}

$disabled=$edit?"":"disabled='disabled'";

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Group</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'groups'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $edit?"Edit":"View"; ?> Group
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="groups.php">Groups</a></li>
                        <li class="active"><?php echo $edit?"Edit":"View"; ?> Group</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                           <?php if ($message && $message==="empty-name-used") { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Group name is required.
                            </div>
                            <?php } ?>
                            
                            <?php if ($message && $message==="invalid-name-used") { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Group name already used
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="success") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Group updated successfully
                            </div>
                            <?php } else if ($message && $message==="addchildgroupsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Child Groups added successfully
                            </div>
                            <?php } else if ($message && $message==="addcontactsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Contacts added successfully
                            </div>
                            <?php } else if ($message && $message==="addgroupsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Groups added successfully
                            </div>
                            <?php } ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Group Details</h3>
                                </div>
                                <div class="box-body">
                                    <?php if ($edit) { ?>
                                    <form id="group_form" role="form" method="POST" action="group_test.php">
                                    
                                        <input type="hidden" name="id" value="<?php echo $group['id']?>">
                                        <?php if ($back) { ?>
                                        <input type="hidden" name="back" value="<?php echo $back?>">
                                        <?php } ?>
                                         <div id='del_contacts'>
                                            <input type="text" name="del_contacts_ids" id="del_contacts_ids"  value="" />
                                          </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Enter ..." value="<?php echo $group['name']?>" <?php echo $disabled?>/>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <?php $super_checked=$group['super']==="1"?"checked":""; ?>
                                                <input type="checkbox" name="super" value="1" <?php echo $disabled?> <?php echo $super_checked?>>
                                                Super Group
                                            </label>
                                        </div>
                                    </div>
                                    <?php if ($id!=="new") { ?>
                                    <div>
                                        <strong>Last Modified: </strong><?php echo date( 'jS M Y h:i a', strtotime($group['last_modified']))?> by <?php echo $group['admin_name']?>
                                    </div>
                                    <?php } ?>
                                    <?php if ($group['parent_id']===null) { ?>
                                    <!--h4 class="box-title">Child Groups:</h4>
                                    <table id="childgrouptable" class="table table-bordered">
                                        
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Contacts</th>
                                                <?php if ($edit) { ?>
                                                    <th>Remove</th>
                                                <?php } else { ?>
                                                    <th>Actions</th>
                                                <?php }  ?>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Contacts</th>
                                                <?php if ($edit) { ?>
                                                    <th>Remove</th>
                                                <?php } else { ?>
                                                    <th>Actions</th>
                                                <?php } ?>
                                            </tr>
                                        </tfoot>
                                        
                                        <tbody>
                                            <?php
                                            foreach($child_groups as $child_group) {
                                                ?>
                                            <tr>
                                                <td><?php echo $child_group['name']?></td>
                                                <td><?php echo $child_group['super']?></td>
                                                <td><?php echo $child_group['contact_count']?></td>
                                                <?php if ($edit) { ?>
                                                    <td>
                                                        <a href="#" onclick="selectGroup('<?php echo $child_group['id']?>', false, null);">Remove</a>
                                                        <input type="hidden" name="childgroups[]" value="<?php echo $child_group['id']?>"/>
                                                    </td>
                                                <?php } else { ?>
                                                    <td>
                                                        <a class='link' href='group_test.php?id=<?php echo $child_group['id']?>&back=<?php echo $group['id']?>'>View</a> | <a class='link' href='group_test.php?id=<?php echo $child_group['id']?>&edit=parent&back=<?php echo $group['id']?>'>Edit</a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table-->
                                    <?php if ($edit) { ?>
                                    <!--span><h4 class="box-title">Select Child Groups</h4> (<a id="childgroups_showhide">show</a>)</span>
                                    <table id="groupstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Child Groups</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Child Groups</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br/-->
                                    <?php } ?>
                                    <?php } ?>
                                    <h4 class="box-title">Contacts:</h4>
                                    <table id="contactstable" class="table table-bordered table-striped">
                                        
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Company</th>
                                                <th>Department</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <?php if ($edit) { ?>
                                                    <th>Remove</th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Company</th>
                                                <th>Department</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <?php if ($edit) { ?>
                                                    <th>Remove</th>
                                                <?php } ?>
                                            </tr>
                                        </tfoot>
                                        

                                        <tbody>
                                            <?php
                                            foreach($contacts as $contact) {
                                                ?>
                                            <tr>
                                                <td><?php echo $contact['first_name']?></td>
                                                <td><?php echo $contact['last_name']?></td>
                                                <td><?php echo $contact['company']?></td>
                                                <td><?php echo $contact['department']?></td>
                                                <td><?php echo $contact['mobile_phone']?></td>
                                                <td><?php echo $contact['email']?></td>
                                                <td><?php echo $contact['phone']?></td>
                                                <?php if ($edit) { ?>
                                                    <td>
                                                        <a href="#" onclick="selectContact('<?php echo $contact['email']?>', false, null);">Remove</a>
                                                        <input type="hidden" name="contacts[]" value="<?php echo $contact['email']?>">
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>

                                    </table>
                                    <?php if ($edit) { ?>
                                    <span><h4 class="box-title">Select Contacts</h4> (<a id="contacts_showhide">show</a>)</span>
                                    <table id="selectcontactstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Company</th>
                                                <th>Department</th>
                                                <th>Job Title</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Modified</th>
                                                <th>fullname</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Company</th>
                                                <th>Department</th>
                                                <th>Job Title</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Modified</th>
                                                <th>fullname</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br/>
                                    <?php } ?>
                                    <h4 class="box-title">Group Can View:</h4>
                                    <table id="groupviewtable" class="table table-bordered">
                                        
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Contacts</th>
                                                <?php if ($edit) { ?>
                                                    <th>Remove</th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Contacts</th>
                                                <?php if ($edit) { ?>
                                                    <th>Remove</th>
                                                <?php } ?>
                                            </tr>
                                        </tfoot>
                                        
                                        <tbody>
                                            <?php
                                            foreach($group_views as $group_view) {
                                                ?>
                                            <tr>
                                                <td><?php echo $group_view['name']?></td>
                                                <td><?php echo $group_view['super']?></td>
                                                <td><?php echo $group_view['parent']?></td>
                                                <td><?php echo $group_view['contact_count']?></td>
                                                <?php if ($edit) { ?>
                                                    <td>
                                                        <a href="#" onclick="selectGroupView('<?php echo $group_view['id']?>', false, null);">Remove</a>
                                                        <input type="hidden" name="groupviews[]" value="<?php echo $group_view['id']?>"/>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php if ($edit) { ?>
                                    <span><h4 class="box-title">Select Group Can View</h4> (<a id="groupview_showhide">show</a>)</span>
                                    <table id="selectgroupviewtable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Child Groups</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>id</th>
                                                <th>parent_id</th>
                                                <th>Name</th>
                                                <th>Super Group</th>
                                                <th>Parent</th>
                                                <th>Child Groups</th>
                                                <th>Contacts</th>
                                                <th>Last Modified</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br/>
                                    </form>
                                    <?php } ?>
                                    <?php if ($edit) { ?>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <?php 
                                    $back_query=$back?"&back=".$back:"";
                                    if ($edit) {
                                        $cancel_href=
                                                $edit==="search"?"groups.php":
                                                ($edit==="parent"?"group_test.php?id=".$back:
                                                    "group_test.php?id=".$id.$back_query);
                                        ?>
                                        <a class="btn btn-app" href="<?php echo $cancel_href?>">
                                            <i class="fa fa-arrow-left"></i> Cancel
                                        </a>
                                        <a id="save_link" class="btn btn-app">
                                            <i class="fa fa-save"></i> Save
                                        </a>
                                        <?php if ($id!=="new") { ?>
                                        <a class="btn btn-app" onclick="return confirm('Are you sure you want to delete this group?');" href="group_test.php?delete_id=<?php echo $id?>">
                                            <i class="fa fa-times"></i> Delete
                                        </a>
                                        <?php } ?>
                                        <?php
                                    } else {
                                        if (0 === strpos($back, 'contact|')) {
                                            $back_href="contact.php?id=".substr($back, 8);
                                        } else {
                                            $back_href=$back?"group_test.php?id=".$back:"groups.php";
                                        }
                                        ?>
                                        <a class="btn btn-app" href="<?php echo $back_href?>">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                        <a class="btn btn-app" href="group_test.php?id=<?php echo $id?>&edit=view<?php echo $back_query?>">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                        <?php if ($group['parent_id']===null) { ?>
<!--                                        <a class="btn btn-app" href="groups.php?id=<?php echo $id?>&select=parent_group<?php echo $back_query?>">
                                            <i class="fa fa-plus-square-o"></i> Add Child Group
                                        </a>-->
                                        <?php } ?>
<!--                                        <a class="btn btn-app" href="contacts.php?id=<?php echo $id?>&select=group<?php echo $back_query?>">
                                            <i class="fa fa-plus-square-o"></i> Add Contacts
                                        </a>
                                        <a class="btn btn-app" href="groups.php?id=<?php echo $id?>&select=group<?php echo $back_query?>">
                                            <i class="fa fa-plus-square-o"></i> Add Group View
                                        </a>-->
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
                
        <!-- page script -->
        <script type="text/javascript">
            <?php if ($edit) { 
            echo 'var groupId='.($id==="new"?"null":"'".$id."'").';';    
            ?>
            var checkboxSelect = {}, groupCheckboxSelect = {}, groupViewCheckboxSelect = {};
            $(function() {
                $('#contactstable input[type="hidden"]').each(function() {
                    checkboxSelect[$(this).val()]=true;
                });
                $('#childgrouptable input[type="hidden"]').each(function() {
                    groupCheckboxSelect[$(this).val()]=true;
                });
                $('#groupviewtable input[type="hidden"]').each(function() {
                    groupViewCheckboxSelect[$(this).val()]=true;
                });
                


                $('#contactstable').dataTable( {
                    "order": [[1, 'asc']],
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                
                $('#selectcontactstable').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_contacts.php",
                    "order": [[1, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        {'searchable':false,'orderable':false},
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null, 
                        { "visible":    false}, 
                        { "visible":    false}
                      ]
                    ,
                    "columnDefs": [ {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          return "<input type='checkbox' value='"+row[7]+"' />";
                      }
                    } ]
                } );
                $('#selectcontactstable').on( 'draw.dt', function () {
                    $('#selectcontactstable input[type="checkbox"]').change(function () {
                        selectContact($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#selectcontactstable input[type="checkbox"]').each(function() {
                        if (checkboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                
                $('#groupstable').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_groups.php",
                    "order": [[3, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        {'searchable':false,'orderable':false},
                        { "visible":    false}, 
                        { "visible":    false}, 
                        null,
                        null,
                        null,
                        null, 
                        null,
                        { "visible":    false}
                      ],
                    "columnDefs": [ {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          var hasChildren = row[6]>0;
                          var hasParent = row[2]!==null;
                          var parentIsGroup = row[2]===groupId;
                          var disable = (hasParent && !parentIsGroup) || hasChildren;
                          return "<input type='checkbox' value='"+row[1]+"' "+(disable?"disabled='disabled'":"")+" />";
                      }
                    } ]
                } );
                $('#groupstable').on( 'draw.dt', function () {
                    $('#groupstable input[type="checkbox"]').change(function () {
                        selectGroup($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#groupstable input[type="checkbox"]').each(function() {
                        if (groupCheckboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                
                $('#selectgroupviewtable').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "scripts/get_groups.php",
                    "order": [[3, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                        {'searchable':false,'orderable':false},
                        { "visible":    false}, 
                        { "visible":    false}, 
                        null,
                        null,
                        null,
                        null, 
                        null,
                        { "visible":    false}
                      ],
                    "columnDefs": [ {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          return "<input type='checkbox' value='"+row[1]+"' />";
                      }
                    } ]
                } );
                $('#selectgroupviewtable').on( 'draw.dt', function () {
                    $('#selectgroupviewtable input[type="checkbox"]').change(function () {
                        selectGroupView($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#selectgroupviewtable input[type="checkbox"]').each(function() {
                        if (groupViewCheckboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                
                $('#groupstable_wrapper').hide();
                $('#childgroups_showhide').click(function() {
                    tableClick(this, $('#groupstable_wrapper'));
                });
                $('#selectcontactstable_wrapper').hide();
                $('#contacts_showhide').click(function() {
                    tableClick(this, $('#selectcontactstable_wrapper'));
                });
                $('#selectgroupviewtable_wrapper').hide();
                $('#groupview_showhide').click(function() {
                    tableClick(this, $('#selectgroupviewtable_wrapper'));
                });
                
            });
            $('#save_link').click(function() {
                $('#group_form').submit();
                console.log( $('#group_form').serialize() );
            });
            
            function selectContact(email, checked, tr) {
            	var rows= $('#contactstable tbody tr').length;
                checkboxSelect[email]=checked;
                if (checked) {
                    $(tr).find('td:first-child').remove();
                    $('<td><a href="#" onclick="selectContact(\''+email+'\', false, null);">Remove</a><input type="hidden" name="contacts[]" value="'+email+'"/></td>').appendTo(tr);
                    tr[0].email=email;
                    
                    $('#contactstable tbody').append(tr);
                } else {
                	var del_contacts_data = $.trim($("#del_contacts_ids").val());
                	   if(del_contacts_data ==''){
             		          $("#del_contacts_ids").val(email);
             	       }else{
           		              $("#del_contacts_ids").val(del_contacts_data+","+email)
             	       }
                	 
                	 $('#contactstable tbody tr').find('input[value="'+email+'"]').closest('tr').remove();
                	 
                	/* if(rows ==1){
                	    $('#contactstable tbody tr').find('input[value="'+email+'"]').closest('tr').hide();
                    }else{
                    	
                     } */
                    
                    $('#selectcontactstable input[type="checkbox"]').each(function() {
                        if ($(this).val()===email) {
                            $(this).prop('checked', false);
                        }
                    });
                }
            }
            function selectGroup(groupId, checked, tr) {
                groupCheckboxSelect[groupId]=checked;
                if (checked) {
                    $(tr).find('td:first-child').remove();
                    $(tr).find('td:eq(2)').remove();
                    $(tr).find('td:eq(2)').remove();
                    $('<td><a href="#" onclick="selectGroup(\''+groupId+'\', false, null);">Remove</a><input type="hidden" name="childgroups[]" value="'+groupId+'"/></td>').appendTo(tr);
                    tr[0].groupId=groupId;
                    
                    $('#childgrouptable tbody').append(tr);
                } else {
                    $('#childgrouptable tbody tr').find('input[value="'+groupId+'"]').closest('tr').remove();
                    $('#groupstable input[type="checkbox"]').each(function() {
                        if ($(this).val()===groupId) {
                            $(this).prop('checked', false);
                        }
                    });
                }
            }
            function selectGroupView(groupId, checked, tr) {
                groupViewCheckboxSelect[groupId]=checked;
                if (checked) {
                    $(tr).find('td:first-child').remove();
                    $(tr).find('td:eq(3)').remove();
                    $('<td><a href="#" onclick="selectGroupView(\''+groupId+'\', false, null);">Remove</a><input type="hidden" name="groupviews[]" value="'+groupId+'"/></td>').appendTo(tr);
                    tr[0].groupId=groupId;
                    
                    $('#groupviewtable tbody').append(tr);
                } else {
                    $('#groupviewtable tbody tr').find('input[value="'+groupId+'"]').closest('tr').remove();
                    $('#selectgroupviewtable input[type="checkbox"]').each(function() {
                        if ($(this).val()===groupId) {
                            $(this).prop('checked', false);
                        }
                    });
                }
            }
            function tableClick(link, table) {
                if ($(link).text()==="show") {
                    $(table).show();
                    $(link).text("hide");
                } else {
                    $(table).hide();
                    $(link).text("show");
                }
            }
            <?php } ?>
        </script>

    </body>
</html>
