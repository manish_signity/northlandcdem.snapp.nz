angular.module('hbrc.services', [])

    .factory('AppService',
    ['$window', '$http', '$q', '$ionicLoading', '$timeout', '$filter',
    function($window, $http, $q, $ionicLoading, $timeout, $filter, ApiService) {
        return {
            getGCMAppId: function() {
                return "319485179606";
            },
            getAppUrl: function() {
                return "https://hbrc.snapp.co.nz/";
            },
            getApiUrl: function() {
                return this.getAppUrl()+"api/";
            },
            getCachedContacts: function(){
                var cached_data = window.localStorage.getArray('cdem_data');
                return $filter('toArray')(cached_data.contacts);
            },
            getCachedGroups: function(){
                var cached_data = window.localStorage.getArray('cdem_data');
                return $filter('toArray')(cached_data.groups);
            },
            helpText: function(text,timeout){
                //if($window.localStorage.getItem("helpTextShown_"+$rootScope.viewState)) return;
                if(!timeout) timeout = 2500;
                $ionicLoading.show({
                    template: text
                });
                $timeout($ionicLoading.hide,timeout);
                //$window.localStorage.setItem("helpTextShown_"+$rootScope.viewState, true);
            },
            checkConnection: function(){
                if(typeof navigator.connection == 'undefined') return true;

                var networkState = navigator.connection.type.toUpperCase();

                console.log('connection state: ',networkState);

                return (networkState != 'NONE' && networkState != 'UNKNOWN');
            },
            updateUnreadMessageCount: function() {
                var unreadMessages = $rootScope.messages.inbox.filter(function (element) {
                    return !element.read;
                });
                $rootScope.unreadMessageCount = unreadMessages.length;
                console.log('update message count: ' + unreadMessages.length);
                var unread = {'unread_message_count': unreadMessages.length};
                if (this.checkConnection()) ApiService.UpdateProfile(unread);

                if ($rootScope.push) {
                    $rootScope.push.setApplicationIconBadgeNumber(function () {
                        console.log('success update badge ' + unreadMessages.length);
                    }, function () {
                        console.log('error update badge ' + unreadMessages.length);
                    }, unreadMessages.length);
                }
            }
        };
    }])


    .factory('ApiService', function ($http, $rootScope, AppService, $filter) {
        return {

            UpdateProfile: function (data, callback) {
                //data.debug = true;
                return $http({
                    method: 'POST',
                    url: AppService.getApiUrl() + 'update_profile.php',
                    timeout: 5000,
                    data: data
                }).success(function (response) {
                    if(callback) callback(response);
                });
            },

            GetData: function (callback) {
                return $http({
                    method: 'POST',
                    url: AppService.getApiUrl() + 'get_data.php',
                    timeout: 10000,
                    data: {}
                }).success(function (response) {
                    callback(response);
                });
            },

            GetMessages: function (datestamp,callback) {
                var params = (datestamp) ? {from: datestamp}:{from: $rootScope.globals.lastDownloadedMessages};
                var now = new Date();
                return $http({
                    method: 'GET',
                    url: AppService.getApiUrl() + 'get_messages.php',
                    timeout: 5000,
                    params: params
                }).success(function (response) {

                    if (response.success) {
                        console.log('got message success: ',response);
                        if(response.data.inbox.length) {

                            $rootScope.globals.lastDownloadedMessages = $filter('date')(now, 'yyyy-MM-dd HH:mm:ss');
                            window.localStorage.setArray('globals', $rootScope.globals);

                            //alert(JSON.stringify($rootScope.messages.inbox));
                            console.log('root messages', $rootScope.messages);
                            $rootScope.messages.inbox = $rootScope.messages.inbox.concat(response.data.inbox);
                            console.log('root messages after merge', $rootScope.messages);
                            //angular.extend($rootScope.messages.inbox, response.data.inbox);
                            //angular.extend($rootScope.messages.sent, response.data.sent);
                            window.localStorage.setArray('cdem_messages',$rootScope.messages);

                            $rootScope.updateUnreadMessageCount();

                        };
                        if(callback) callback(response);
                    } else {
                        if(callback) callback(response);
                    }
                });
            },

            sendNewMessage: function (data) {

                return $http({
                    method: 'POST',
                    url: AppService.getApiUrl() + 'send_message.php',
                    timeout: 5000,
                    data: data
                });
            }

        }
    })

    .factory('AuthenticationService',
    ['Base64', '$http', '$window', '$rootScope',
        function (Base64, $http, $window, $rootScope) {
            var service = {};

            service.Login = function (username, authdata, callback) {

                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;

                var params = {};

                if($rootScope.globals && $rootScope.globals.lastDownloadedMessages) {
                    params = {from: $rootScope.globals.lastDownloadedMessages}
                }

                $http({
                    method: 'GET',
                    url: 'https://hbrc.snapp.co.nz/api/login.php',
                    params: params
                }).success(function (response) {
                    //console.log('auth login response: ', JSON.stringify(response));

                    callback(response);
                }).error(function(){
                    var response = {error: true, message: "Network error. Please try again."}

                    //console.log('auth login error response: ', JSON.stringify(response));
                    callback(response);
                });
            };

            service.SetCredentials = function (username, authdata, data, callback) {

                console.log('setcreds1', $rootScope.globals);
                // get user globals
                $rootScope.globals = (window.localStorage.getArray('globals')) ? window.localStorage.getArray('globals') : {};

                $rootScope.globals.currentUser = {
                        username: username,
                        authdata: authdata
                    };

                $rootScope.globals.lastModified = data.last_modified;
                $rootScope.newMessageCount = data.new_message_count;


                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
                window.localStorage.setArray('globals', $rootScope.globals);

                console.log('setcreds2', $rootScope.globals, data);
                callback();
            };

            service.ClearCredentials = function () {
                console.log('clear credentials',$rootScope.globals);
                window.localStorage.removeArray('globals');
                window.localStorage.removeArray('cdem_data');
                window.localStorage.removeArray('cdem_messages');
                window.localStorage.removeArray('contactFavorites');
                $rootScope.globals = {};
                window.localStorage.setArray('globals', $rootScope.globals);
                //$http.defaults.headers.common.Authorization = 'Basic ';
                console.log('after clear',$rootScope.globals);
            };

            return service;
        }])

    .factory('Base64', function () {

        var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

        return {
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);

                return output;
            },

            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    window.alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                do {
                    enc1 = keyStr.indexOf(input.charAt(i++));
                    enc2 = keyStr.indexOf(input.charAt(i++));
                    enc3 = keyStr.indexOf(input.charAt(i++));
                    enc4 = keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";

                } while (i < input.length);

                return output;
            }
        };
    })


    .factory('PushNotificationService', ['$ionicPopup', '$ionicBackdrop', '$rootScope', '$state', '$window', '$http', 'AppService', 'ApiService', '$timeout',
        function($ionicPopup, $ionicBackdrop, $rootScope, $state, $window, $http, AppService, ApiService, $timeout) {
            $rootScope.alertsIDs = [];
            $rootScope.pushRegisterTries = 0;

            var me = {
                callRegister: function(deferred) {
                    pushHandler.deferred = deferred;
                    var pushNotification = window.plugins.pushNotification;
                    if( typeof device == 'undefined' ) {
                        pushHandler.onNotificationGCM({event: "registered", regid: "dummygcm"});
                    } else {
                        if (device.platform == 'android' || device.platform == 'Android') {
                            pushNotification.register(pushHandler.successHandler, pushHandler.errorHandler, {"senderID": AppService.getGCMAppId(), "ecb": "pushHandler.onNotificationGCM"});
                        } else {
                            pushNotification.register(pushHandler.tokenHandler, pushHandler.errorHandler, {"badge": "true", "sound": "true", "alert": "true", "ecb": "pushHandler.onNotificationAPN"});
                        }
                    }
                },
                handleNotification: function(notif) {
                    //if($rootScope.alertsIDs.indexOf(notif.id) > -1) return;

                    $timeout(function() {
                        ApiService.GetMessages($rootScope.globals.lastDownloadedMessages, function (response) {
                            if (response.success) {

                                $rootScope.updateUnreadMessageCount();

                                var alertPopup = $ionicPopup.confirm({
                                    title: notif.title,
                                    subTitle: notif.sender,
                                    template: notif.message,
                                    cancelText: 'Close',
                                    okText: 'View'
                                });
                                alertPopup.then(function (res) {
                                    if (res) {
                                        if ($ionicPopup._popupStack.length > 0) $ionicBackdrop.release();

                                        //$state.go('main.messageDetail', {id: notif.id});
                                        $state.go('main.messageInbox');
                                    }
                                });

                            }
                        });
                        $rootScope.alertsIDs.push(notif.id);
                    }, 500);
                },
                registerGCMId: function(gcmId) {
                    console.log('registerGCMId: ' + gcmId);
                    if (me.getGCMId() != gcmId || me.diffPlatformVersion()) {
                        me.updateAndroidUser(gcmId);
                    }
                },
                getGCMId: function() {
                    return ($rootScope.globals.device_push) ? $rootScope.globals.device_push.device_gcm_id:'';
                },
                registerAPNToken: function(token) {
                    if (me.getAPNId() != token || me.diffPlatformVersion()) {
                        console.log('Register APN token', token);
                        me.updateIOSUser(token);
                    }
                },
                getAPNId: function() {
                    return ($rootScope.globals.device_push) ? $rootScope.globals.device_push.device_apn_id:'';
                },
                getDeviceInfo: function() {
                    return $rootScope.globals.device_push;
                },
                saveDeviceInfo: function(deviceInfo) {
                    if($rootScope.globals.profileUpdated){ // if profile has already been sent, send device info
                        ApiService.UpdateProfile(deviceInfo);
                    }
                    $rootScope.globals.device_push = deviceInfo;
                    window.localStorage.setArray('globals', $rootScope.globals);
                },
                diffPlatformVersion: function() {
                    var deviceInfo = me.getDeviceInfo();
                    var device = me.getDevice();
                    //alert('diffPlatformVersion, platform: ' + deviceInfo['platform'] + ' ' + device.platform + ', version: ' + deviceInfo['version'] + ' ' + device.version);
                    return deviceInfo['platform'] != device.platform || deviceInfo['version'] != device.version;
                },
                getDevice: function() {
                    if (typeof device == 'undefined') return {platform: 'desktop', version: 'debug'};
                    return device;
                },
                updateAndroidUser: function(id) {
                    console.log('registering GCM id: ' + id);
                    var push_device = {device_gcm_id: id, device_platform: device.platform, device_version: device.version};
                    me.saveDeviceInfo(push_device)
                },
                updateIOSUser: function(id) {
                    var push_device = {
                        device_apn_id: id,
                        device_uid: device.uuid,
                        device_name: (device.name ? device.name : device.model),
                        device_model: device.model,
                        device_version: device.version,
                        device_platform: device.platform,
                    };
                    me.saveDeviceInfo(push_device)
                },
                errorPopup: function(){
                    $rootScope.pushRegisterTries++;
                    if($rootScope.pushRegisterTries > 3) return;
                    $ionicPopup.alert({
                        title: "Failed to register device.",
                        subTitle: "Please check you have allowed push notifications for this app under Settings->Notifications for this app and try again.",
                        buttons: [
                            {
                                text: 'Retry',
                                type: 'button-positive',
                                onTap: function (e) {
                                    pushHandler.service.callRegister()
                                }
                            }
                        ]
                    }).then(function(){
                        if ($ionicPopup._popupStack.length > 0) $ionicBackdrop.release();
                    });
                }
            };
            return me;
        }]



);