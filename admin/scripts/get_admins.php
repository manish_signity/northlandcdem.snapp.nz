<?php
require_once('../../config/config.php');

// SQL server connection information
$sql_details = array(
    'user' => $DB_USER,
    'pass' => $DB_PASSWORD,
    'db'   => $DB_DATABASE,
    'host' => $DB_HOST
);
 
// DB table to use
$table = 'admin';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id', 'dt' => 0 ),
    array( 'db' => 'email',  'dt' => 1 ),
    array( 'db' => 'name',  'dt' => 2 ),
    array(
        'db'        => 'last_login',
        'dt'        => 3,
        'formatter' => function( $d, $row ) {
        	if(!empty($d)){
            return date( 'jS M Y h:i a', strtotime($d));
        	}else{
        		return '';
        	}
        }
    )
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);