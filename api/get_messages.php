<?php 
require_once "authenticate.php"; 
require_once "../config/dbconnection.php"; 

$auth = authenticate($conn);

if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {
        $inbox=array();
        $sent=array();
        $fromTime=isset($_GET['from'])?strtotime($_GET['from']):false;
        $from=$fromTime?date('Y-m-d H:i:s',$fromTime):false;
        $fromQuery=$from?" and m.sent >= :from":"";
        
        $return['from']=$from;
        $return['from_get']=$_GET['from'];
        
        $query=$conn->prepare("select distinct m.id from message m, message_contact mc where m.id=mc.message_id and (m.sender=:sender or mc.email=:email)".$fromQuery);
        $query->bindValue("sender",$email);
        $query->bindValue("email",$email);
        if ($from) {
            $query->bindValue("from",$from);
        }
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);

        $message_ids=array();

        foreach($results as $result) {
            $message_ids[]=$result['id'];
        }

        if (!empty($message_ids)) {
            $inQuery = implode(',', array_fill(0, count($message_ids), '?'));
            $query=$conn->prepare("select m.id, m.title, m.message, case when a.email is null then m.sender else a.email end as sender, case when a.name is null then CONCAT(C.first_name, ' ', C.last_name) else a.name end as sender_name,  m.sent from message m left join admin a on m.admin_sender = a.id left join contact C on C.email = m.sender  where m.id in (".$inQuery.") ORDER BY m.id DESC ");
            foreach($message_ids as $k => $message_id) {
                $query->bindValue(($k+1), $message_id);
            }
            $query->execute();
            $messages=$query->fetchAll(PDO::FETCH_ASSOC);

            $query=$conn->prepare("select * from message_contact where message_id in (".$inQuery.")");
            foreach($message_ids as $k => $message_id) {
                $query->bindValue(($k+1), $message_id);
            }
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_ASSOC);

            $message_contacts=array();
            
            foreach($results as $result) {
                $message_contacts[$result['message_id']][]=$result['email'];
            }
            
            foreach($messages as $message) {
                $message['contacts']=$message_contacts[$message['id']];
                if($message['sender']===$email) {
                    $sent[]=$message;
                }
                foreach($message['contacts'] as $to) {
                    if($to===$email) {
                        $inbox[]=$message;
                        break;
                    }
                }
            }
        }
        
        $return['data']['inbox']=$inbox;
        $return['data']['sent']=$sent;
        
    }

    echo json_encode($return);
}

exit;
