<?php

function authenticate($conn) {
	
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header('WWW-Authenticate: Basic realm="HBRC Emergency Contacts"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Login credentials required.';
        return null;
    }else if ( isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER'] == null) ) {
        header('WWW-Authenticate: Basic realm="HBRC Emergency Contacts"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Login credentials required.';
        return null;
    } else {
        $email=isset($_SERVER['PHP_AUTH_USER'])?$_SERVER['PHP_AUTH_USER']:false;
        $password=isset($_SERVER['PHP_AUTH_PW'])?$_SERVER['PHP_AUTH_PW']:false;

        $return=array("success"=>"0","message"=>"");
        $returnEmail=$email;
        
        if (!($email && $password)) {
            $return['message']="Invalid parameters";

        } else {

            $query=$conn->prepare("select email, active from contact where email like :email and password=:password ");
            $query->bindValue("email",$email);
            $query->bindValue("password",$password);
            $query->execute();
            $valid=$query->fetchAll(PDO::FETCH_ASSOC);

            if (empty($valid)) {
                $return['message']="Not authorised";
            } else {
                $dbEmail = $valid[0]['email'];
                
                if (strtolower($dbEmail)!==strtolower($email)) {
                    $return['message']="Not authorised";
                } else if (!empty($valid) && $valid[0]['active']==0) {
                    $return['message']="Not active";
                } else {
                    $return['success']="1";
                    $returnEmail=$dbEmail;
                }
            }
        }
        return array('return'=>$return,'email'=>$returnEmail);
    }
}