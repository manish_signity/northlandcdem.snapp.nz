<?php
require_once "authenticate.php";
require_once "../config/dbconnection.php"; 
$auth = authenticate($conn);


if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {
        
        if (isset($_POST['newpassword'])  && !empty($_POST['newpassword'])) {
        	/* check old password with new password 15-june-2018 */
	        	$chkQuery = $conn->prepare("select email, password from contact where email like :email ");
	        	$chkQuery->bindValue("email",$email);
	        	$chkQuery->execute();
	        	$chkResults = $chkQuery->fetchAll(PDO::FETCH_ASSOC);
	        	if($chkResults[0]['password'] == $_POST['newpassword'] ){
	        		$return['message']="New password should be different than old password. ";
	        		echo json_encode($return);
	        		die();
	        	}
        	/* check old password with new password 15-june-2018 */
        	
        	$password   = $_POST['newpassword'];
        	$update_sql = "update contact set password = '".$password."'";
        	$update_sql.= " where email=:email";
        	$query      = $conn->prepare($update_sql);        	
            $query->bindValue("email",$email);
            $query->execute();
            $return['message']="Password updated. ";
            
        } else {
            $return['message']="No password to update. ";
        }
        
    }

    echo json_encode($return);
}

exit;