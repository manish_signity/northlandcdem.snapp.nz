<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php

$select=isset($_GET['select'])?$_GET['select']:false;
$id=isset($_GET['id'])?$_GET['id']:false;
$back=isset($_GET['back'])?$_GET['back']:false;
$back_query=$back?"&back=".$back:"";
$message=isset($_GET['message'])?$_GET['message']:false;

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Contacts</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'contacts'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Contacts
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Contacts</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($_GET['import_success'])) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Import completed successfully
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="deletesuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Contact deleted successfully
                            </div>
                            <?php } ?>
                            <div class="box" style="width:116%; overflow:scroll">
                                <?php if (!$select) { ?>
                                <div class="box-links">
                                    <a class="link" href="contact.php?id=new&edit=search">Add New Contact</a> |
<!--
                                    <?php if ($_SESSION['loggedInUser'] === "admin@snapp.co.nz") { ?> 
                                    <a class="link" href="import_contacts.php">Import Contacts</a> |
                                    <a class="link" href="import_groups.php">Import Groups</a> |
                                    <?php } ?>
-->
                                    <a class="link" href="export_contacts.php">Export Contacts</a> |
                                    <a class="link" href="send_user_credentials.php?edit=contacts">Send User Credentials</a>
                                </div><!-- /.box-header -->
                                <?php } ?>
                                <div class="box-body table-responsive">
                                    <?php if ($select) { ?>
                                    <form id="select_form" role="form" method="POST" action="<?=$select?>.php">
                                        <input type="hidden" name="id" value="<?=$id?>">
                                        <input type="hidden" name="select_from" value="contact">
                                        <?php if ($back) { ?>
                                        <input type="hidden" name="back" value="<?=$back?>">
                                        <?php } ?>
                                    <?php } ?>
                                    <table id="contactstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Organisation</th>
                                                <th>Department</th>
                                                <th>Job Title</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Modified</th>
                                                <th>Group Name</th>
                                                <th>fullname</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Select</th>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Organisation</th>
                                                <th>Department</th>
                                                <th>Job Title</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Modified</th>
                                                <th>Group Name</th>
                                                <th>fullname</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <?php if ($select) { ?>
                                    <h3 class="box-title">Selected Contacts</h3>
                                    <table id="selectcontactstable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Company</th>
                                                <th>Department</th>
                                                <th>Job Title</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Modified</th>
                                                <th>Group Name</th>
                                                <th>fullname</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Last Name</th>
                                                <th>Company</th>
                                                <th>Department</th>
                                                <th>Job Title</th>
                                                <th>Mobile Phone</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Last Modified</th>
                                                <th>Group Name</th>
                                                <th>fullname</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                        
                                        <tbody>
                                        </tbody>
                                        
                                    </table>
                                    <p>
                                        <a class="btn btn-warning" href="<?=$select?>.php?id=<?=$id?><?=$back_query?>">Back</a>
                                        <button type="submit" class="btn btn-primary">Add Contacts</button>
                                    </p>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <!--<script src="js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>-->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            var checkboxSelect = {};
            $(function() {
                $('#contactstable').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "pageLength":100,
                    "ajax": "scripts/get_contacts.php",
                    "order": [[1, 'asc']],
                    "dom": 'flrtip',
                    "columns": [
                                <?php echo $select?"{'searchable':false,'orderable':false}":"{\"visible\":false}"; ?>,
                                null,
                                {"visible":false},
                                null,
                                {"visible":false},
                                {"visible":false},
                                null,
                                null,
                                {"visible":false},
                                null,
                                {"visible":false},
                                {"visible":false},
                                <?php echo $select?"{\"visible\":false}":"null"; ?>
                       ]
                    ,
                    "columnDefs": [ {
                        "targets": [ 12 ],
                        "data": function(row, type, val, meta) {
                            return "<a class='link' href='contact.php?id="+row[7].replace('\'','%27')+"'>View</a>"
                                +" | <a class='link' href='contact.php?id="+row[7].replace('\'','%27')+"&edit=search'>Edit</a>"
                                +" | <a class='link' href='contact.php?delete_id="+row[7].replace('\'','%27')+"' onclick='return confirmDelete();'>Delete</a>";
                        }
                    },{
                        "targets": [ 10 ],
                        "data": function(row, type, val, meta) {
                          return row[11];
                        }
                    }, {
                      "targets": [ 0 ],
                      "data": function(row, type, val, meta) {
                          return "<input type='checkbox' value='"+row[7]+"' />";
                      }
                    } ]
                } );
                <?php if($select) { ?>
                $('#contactstable').on( 'draw.dt', function () {
                    $('#contactstable input[type="checkbox"]').change(function () {
                        selectContact($(this).val(), this.checked, $(this).closest('tr').clone());
                    });
                    $('#contactstable input[type="checkbox"]').each(function() {
                        if (checkboxSelect[$(this).val()]) {
                            $(this).prop('checked', true);
                        }
                    });
                } );
                <?php } ?>
            });
            <?php if($select) { ?>
            function selectContact(email, checked, tr) {
                checkboxSelect[email]=checked;
                if (checked) {
                    $(tr).find('td:first-child').remove();
                    $('<td><a href="#" onclick="selectContact(\''+email+'\', false, null);">Remove</a><input type="hidden" name="selects[]" value="'+email+'"/></td>').appendTo(tr);
                    tr[0].email=email;
                    
                    $('#selectcontactstable tbody').append(tr);
                } else {
                    var toRemove;
                    $('#selectcontactstable tbody tr').each(function() {
                        if (this.email===email) {
                            toRemove=this;
                        }
                    });
                    if (toRemove) {
                        $(toRemove).remove();
                        $('#contactstable input[type="checkbox"]').each(function() {
                            if ($(this).val()===email) {
                                $(this).prop('checked', false);
                            }
                        });
                    }
                }
            }
            <?php } ?>
            function confirmDelete() {
                return confirm("Are you sure you want to delete this contact?");
            }
        </script>

    </body>
</html>
