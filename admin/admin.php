<?php require_once "../config/dbconnection.php"; ?>
<?php require_once "common/checkLoggedIn.php"; ?>

<?php
$edit=isset($_GET['edit'])?$_GET['edit']:false;
$message=isset($_GET['message'])?$_GET['message']:false;
$id="";
$errors=array();

$disabled=$edit?"":"disabled='disabled'";
$emailDisabled=$edit&&$_GET['id']==="new"?"":"disabled='disabled'";

$admin=array();
if (isset($_POST['id'])) {
    $id=$_POST['id'];
    $email=isset($_POST['email'])?trim($_POST['email']):false;
    $password=isset($_POST['password'])?trim($_POST['password']):false;
    $name=isset($_POST['name'])?trim($_POST['name']):false;
    
    if ($id==="new") {
        if (!$email) {
            $errors[]="Email required.";
        }
        if (!$password) {
            $errors[]="Password required.";
        }
        
        if (count($errors) === 0) {
            $email=strtolower($email);
            $saltedPassword=md5($email.$password);

            $query=$conn->prepare("insert into admin (email, password, name) values (:email, :password, :name) "
                    . "on duplicate key update password=values(password), name=values(name)");
            $query->bindValue("email",$email);
            $query->bindValue("password",$saltedPassword);
            $query->bindValue("name",$name);
            $query->execute();
            $id=$conn->lastInsertId();

            header("Location:/admin/admin.php?id=".$id."&message=addsuccess");
            exit;
        } else {
            $admin=array("email"=>$email,"password"=>$password,"name"=>$name);
        }
        
    } else {
        if ($password!==false) {
            $email=strtolower($email);
            $saltedPassword=md5($email.$password);
            
            $query=$conn->prepare("update admin set name=:name, password=:password where id=:id");
            $query->bindValue("name",$name);
            $query->bindValue("password",$saltedPassword);
            $query->bindValue("id",$id);
            $query->execute();
        } else {
            $query=$conn->prepare("update admin set name=:name where id=:id");
            $query->bindValue("name",$name);
            $query->bindValue("id",$id);
            $query->execute();
        }
        
        header("Location:/admin/admin.php?id=".$id."&message=success");
        exit;
    }
    
} else if (isset($_GET['id'])) {
    $id=$_GET['id'];
    
    if ($id==="new") {
        $admin=array("email"=>"","password"=>"","name"=>"");
    } else {
        $query=$conn->prepare("select email, name, last_login from admin where id=:id");
        $query->bindValue("id",$id);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
            $admin=$results[0];
        }
        $admin['password']="";
    }
    
} else if (isset($_GET['delete_id'])) {
    $query=$conn->prepare("delete ignore from admin where id=:delete_id");
    $query->bindValue("delete_id",$_GET['delete_id']);
    $query->execute();
    
    header("Location:/admin/admins.php?message=deletesuccess");
    exit;
}


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo SITE_GLOBAL_TITLE;?> | Admin User</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HBRC style -->
        <link href="css/hbrc.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php require_once "common/header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $page = 'admins'; require_once "common/sidemenu.php"; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    <?php  if ($id==="new") {?>
                         Add Admin User
                    <?php }else{?>
                        <?php echo $edit?"Edit":"View"; ?> Admin User
                     <?php }?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="admins.php">Admin Users</a></li>
                        <?php  if ($id==="new") {?>
                        <li class="active">Add Admin User</li>
                        <?php }else{?>
                        <li class="active"><?php echo $edit?"Edit":"View"; ?> Admin User</li>
                        <?php }?>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                            foreach ($errors as $error) {
                            ?>
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?=$error?>
                            </div>
                            <?php
                            }
                            ?>
                            <?php if ($message && $message==="addsuccess") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Admin User added successfully
                            </div>
                            <?php } ?>
                            <?php if ($message && $message==="success") { ?>
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Admin User updated successfully
                            </div>
                            <?php } ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Admin User Details</h3>
                                </div>
                                <div class="box-body">
                                    <?php if ($edit) { ?>
                                    <form id="admin_form" role="form" method="POST" action="admin.php?id=<?=$id?>&edit=<?=$edit?>">
                                        <input type="hidden" name="id" value="<?=$id?>">
                                    <?php } ?>
                                        
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" placeholder="Enter ..." value="<?=$admin['email']?>" <?=$emailDisabled?>/>
                                        <?php if ($emailDisabled) { ?>
                                            <input type="hidden" name="email" value="<?=$admin['email']?>"/>
                                        <?php } ?>
                                    </div>
                                    <?php if ($edit) { 
                                        $passwordPlaceholder=$id !== "new"?"Leave blank to keep current password ...":"Enter ...";
                                        ?>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="<?=$passwordPlaceholder?>" value="<?=$admin['password']?>"/>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Enter ..." value="<?=$admin['name']?>" <?=$disabled?>/>
                                    </div>
                                    <?php if ($id !== "new") { ?>
                                    <div>
                                        <strong>Last Login: </strong><?=date( 'jS M Y h:i a', strtotime($admin['last_login']))?>
                                    </div>
                                    <?php } ?>
                                    <?php if ($edit) { ?>
                                    </form>
                                    <?php } ?>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    
                                    <?php if ($edit) {
                                        $cancel_href=$edit==="search"?"admins.php":"admin.php?id=".$id;
                                        ?>
                                        <a class="btn btn-app" href="<?=$cancel_href?>">
                                            <i class="fa fa-times"></i> Cancel
                                        </a>
                                        <a id="save_link" class="btn btn-app">
                                            <i class="fa fa-save"></i> Save
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class="btn btn-app" href="admins.php">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                        <a class="btn btn-app" href="admin.php?id=<?=$id?>&edit=view">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $('#save_link').click(function() {
                $('#admin_form').submit();
            });
        </script>

    </body>
</html>
