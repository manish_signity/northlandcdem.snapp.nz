angular.module('hbrc.controllers', ['hbrc.services'])
    .controller('App', function($scope, $state){


    })
    .controller('SplashPageController', function($ionicPlatform, $scope, $rootScope, PushNotificationService, AppService, $location, $state, $window, $interval, $http){

        var moveOn = false;
        document.addEventListener("deviceready",function() {

            console.log('run');


            console.log('ready 1');

            if (typeof cordova != 'undefined' && $window.cordova.logger) {
                $window.cordova.logger.__onDeviceReady();
            }

            console.log('ready 3');
            console.log('ready 4');

            //if (!window.parent.ripple && AppService.checkConnection()) {
            if (!window.parent.ripple) {
                console.log('ready 5');

                var push = PushNotification.init({ "android": {"senderID": AppService.getGCMAppId()},
                    "ios": {"alert": "true", "badge": "true", "sound": "true"} } );

                push.on('registration', function(data) {
                    //data.registrationId
                    if ( device.platform == 'android' || device.platform == 'Android') {
                        PushNotificationService.registerGCMId(data.registrationId);
                    } else {
                        PushNotificationService.registerAPNToken(data.registrationId);
                    }

                    // Kick things off on the entry page
                    moveOn = true;
                });

                push.on('notification', function(data) {
                    // data.message,
                    // data.title,
                    // data.count,
                    // data.sound,
                    // data.image,
                    // data.additionalData
                    console.log('notification data: ',data);
                    var payload = data;
                    payload.sender = data.additionalData.sender;
                    payload.id = data.additionalData.id;

                    PushNotificationService.handleNotification(payload);
                });

                push.on('error', function(e) {
                    // e.message
                    console.log(device.platform + ' Push error = '+e);
                    moveOn = true;
                });
                console.log('ready 6');

                $rootScope.push = push;


            } else {
                console.log('ready , is ripple');
                moveOn = true;
            }

            // catch for if registration doesn't happen
            var entryInt = $interval(function(){

                if(moveOn) {
                    $interval.cancel(entryInt);
                    $state.go('entry');
                    console.log('ready 8');
                }

            }, 1000);

        });
    })

    /* Sign in */
    .controller('EntryPageController', function($scope, $rootScope, $window, AuthenticationService, $location, $state, AppService, $http) {

        console.log('entry');

        var globals = $rootScope.globals;

        console.log('globals: ',JSON.stringify(globals));

        if (!globals.currentUser) {
            console.log('no cached user')
            $state.go('auth');
        } else {
            console.log('has cached user')

            if (typeof $rootScope.globals.currentUser != 'undefined') {
                if (typeof $rootScope.globals.currentUser.authdata != 'undefined') {
                    console.log('current user auth', $rootScope.globals.currentUser.authdata);
                    //console.log('http defaults', JSON.stringify($rootScope.globals.currentUser));
                    $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
                }
            }

            // current user set, login to get fresh last updated details
            var currentUser = globals.currentUser;

            // NETWORK CHECK
            if(!AppService.checkConnection()) {

                console.log('no connection go to download using cache data, assume auth is current');
                $state.go('download');

            } else {
                console.log('has connection go to auth check');

                $scope.dataLoading = true;
                AuthenticationService.Login(currentUser.username, currentUser.authdata, function (response) {
                    console.log('auth response', response);
                    if (response.success == "1") {
                        AuthenticationService.SetCredentials(currentUser.username, currentUser.authdata, response.data, function () {
                            $state.go('download');
                        });
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                        // there must have been something wrong with the login details, go to login page
                        $state.go('auth');
                    }
                });
            };
        }
    })

    .controller('AuthPageController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService', 'AppService', 'Base64', '$state', '$ionicLoading', '$ionicHistory', '$timeout',
    function ($scope, $rootScope, $location, AuthenticationService, AppService, Base64, $state, $ionicLoading, $ionicHistory, $timeout) {

        AuthenticationService.ClearCredentials();

        console.log('login: ', $rootScope.globals);

        var mailtoEmail = 'cdemapp@hbrc.govt.nz';
        var mailtoSubject = 'Requesting a login to the HBCDEM app.';
        var mailtoBody = 'Please fill in the following.%0A%0A Name: %0ACompany: %0ATitle: %0A%0A';

        $scope.mailto = 'mailto:'+mailtoEmail+'?subject='+mailtoSubject+'&body='+mailtoBody;

        if(!AppService.checkConnection()){
            AppService.helpText('This app requires an internet connection for the first log in, to enable it to cache the data.', 5000);
        }

        $scope.openTerms = function(){
            //https://hbrc.snapp.co.nz/terms-of-use.php
            var ref = cordova.InAppBrowser.open('https://hbrc.snapp.co.nz/terms-of-use.php', '_blank', 'location=yes');
        };

        // reset login status
        $timeout(function(){

            $ionicHistory.nextViewOptions({
                historyRoot: true,
                disableBack: true
            });

            $ionicHistory.clearCache().then(function () {
                $ionicHistory.clearHistory();
            });

        }, 300);


        console.log('login after clear', $rootScope.globals);

        $scope.login = function () {
            console.log('auth');

            if(!AppService.checkConnection()){
                AppService.helpText('This app requires an internet connection for the first log in, to enable it to cache the data.', 5000);
                return;
            }

            $ionicLoading.show({
                template: 'Logging in...'
            });

            var username = this.username.toLowerCase();
            var authdata = Base64.encode(this.username + ':' + this.password);

            $scope.dataLoading = true;
            AuthenticationService.Login(username, authdata, function(response) {
                console.log(this, authdata);
                if(response.success == "1") {
                    AuthenticationService.SetCredentials(username, authdata, response.data, function(){
                        console.log($rootScope.globals);
                        $state.go('download');
                    });
                    $scope.dataLoading = false;
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
                $ionicLoading.hide();
            });
        };
    }])

    .controller('DownloadPageController', function($scope, $rootScope, ApiService, AppService, AuthenticationService, $location, $filter, $state, $ionicHistory) {
        console.log('download page');
        var requiresUpdate = true;
        var cached_data = window.localStorage.getArray('cdem_data');

        if(typeof $rootScope.globals.lastDownloaded != 'undefined'){
            var local = new Date($rootScope.globals.lastDownloaded.replace(' ','T')).getTime();
            var contacts = new Date($rootScope.globals.lastModified.contact.replace(' ','T')).getTime();
            var groups = new Date($rootScope.globals.lastModified.group.replace(' ','T')).getTime();
            requiresUpdate = (local < contacts || local < groups || !local || isNaN(local));

            console.log(local, contacts, groups);
        }

        console.log('requires update: '+requiresUpdate);
// DEV
var requiresUpdate = true;

        if((requiresUpdate || !cached_data) && AppService.checkConnection()) {
            ApiService.GetData(function (response) {
                if (response.success) {
                    console.log(response);
                    var dt = new Date().getTime();
                    var datetime = $filter('date')(dt, 'yyyy-MM-dd HH:mm:ss');
                    $rootScope.globals.lastDownloaded = datetime;

                    console.log('updated ');
                    console.log(response.data);

                    window.localStorage.setArray('globals', $rootScope.globals);
                    window.localStorage.setArray('cdem_data', response.data);

                    $rootScope.globals.profileUpdated = window.localStorage.getItem('has_checked_home_profile_details');

                    if(!$rootScope.globals.profileUpdated) {
                        $state.go('main.profile');
                    } else {

                        $ionicHistory.clearHistory();
                        $ionicHistory.clearCache().then(function() {
                            console.log("clear cache");
                            $state.go('main.contacts');
                        });
                    }
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                    $state.go('main.contacts');
                }

            }).error(function(){
                $state.go('main.contacts');
            });
        } else {
            console.log('no update needed');
            $state.go('main.contacts');
        }

    })


    /* Main - profile, contacts, messages */
    .controller('MainController', function($scope, $rootScope, $filter, $location, $state, $ionicSideMenuDelegate, $ionicListDelegate, $ionicHistory, ApiService, AppService) {
        $scope.inComposeMode = false;
        $scope.search = {};


        var cached_data = window.localStorage.getArray('cdem_data');
        if (cached_data) {

            $scope.contacts = [];
            if (cached_data.contacts) {
                angular.forEach($filter('toArray')(cached_data.contacts), function (value, key) {
                    var non_private_data = (JSON.parse(value.non_private_data)) ? JSON.parse(value.non_private_data) : {};
                    delete non_private_data.job_title;

                    var contact = {
                        email: value.email.toLowerCase(),
                        fullname: value.fullname,
                        first_name: (value.first_name) ? value.first_name : value.company,
                        last_name: (value.last_name) ? value.last_name : value.department,
                        company: value.company,
                        department: value.company,
                        job_title: value.job_title,
                        selected_cdem_group: non_private_data.selected_cdem_group,
                        selected_cdem_group_child: non_private_data.selected_cdem_group_child,
                        selected: false
                    }
                    $scope.contacts.push(contact);
                });
            }

            $scope.groups = [];
            if (cached_data.groups) {
                angular.forEach($filter('toArray')(cached_data.groups), function (value, key) {
                    var group = {
                        id: value.id,
                        name: value.name,
                        selected: false
                    }

                    if (value.contacts) group.number_contacts = value.contacts.length;
                    if(!value.id) console.log('blank group', value, key);
                    if(value.id) $scope.groups.push(group);
                });
            }
        } else {
            $state.go('auth');
        }


        $scope.favorites = (window.localStorage.getArray('contactFavorites')) ? window.localStorage.getArray('contactFavorites'):[];
        $scope.recipients = {contacts: [], groups: []};

        $rootScope.messages = (window.localStorage.getArray('cdem_messages')) ? window.localStorage.getArray('cdem_messages'):{inbox:[],sent:[],trash:[]};

        console.log('got messages?: ',$rootScope.messages, $rootScope.haveGotMessages, $rootScope.newMessageCount, $rootScope.globals.lastDownloadedMessages);

        if(!$rootScope.haveGotMessages && AppService.checkConnection()) { // get messages if there are new ones
            $rootScope.haveGotMessages = true;
            ApiService.GetMessages($rootScope.globals.lastDownloadedMessages, function (response) {
                if (response.success) {

                    $scope.updateUnreadMessageCount();

                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        }

        $rootScope.updateUnreadMessageCount = function(){
            var unreadMessages =  $rootScope.messages.inbox.filter(function (element) {
                return !element.read;
            });
            $scope.unreadMessageCount = unreadMessages.length;
            console.log('update message count: '+unreadMessages.length);
            var unread = {'unread_message_count': unreadMessages.length};
            if(AppService.checkConnection()) ApiService.UpdateProfile( unread );

            if ($rootScope.push) {
                $rootScope.push.setApplicationIconBadgeNumber(function () {
                    console.log('success update badge ' + unreadMessages.length);
                }, function () {
                    console.log('error update badge ' + unreadMessages.length);
                }, unreadMessages.length);
            }
        }
        $scope.updateUnreadMessageCount();

        $scope.toggleMenu = function(){
            $ionicSideMenuDelegate.toggleRight();
        };

        $scope.enableComposeMode = function(){
            $scope.inComposeMode = true;
            $ionicSideMenuDelegate.toggleRight(false);
        };
        $scope.clearComposeMode = function(){
            $scope.inComposeMode = false;
            $rootScope.new_message = {};
            angular.forEach($scope.contacts, function(value, key) {
                value.selected = false;
            });
            angular.forEach($scope.favorites, function(value, key) {
                value.selected = false;
            });
            angular.forEach($scope.groups, function(value, key) {
                value.selected = false;
            });

            $scope.recipients = {contacts: [], groups: []};
            if($state.current.name != 'main.contacts') {
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true,
                    historyRoot: true
                });
                $state.go('main.contacts');
            };
        };
        $scope.composeNewMessage = function(contact){
            $scope.recipients.contacts.push(contact.email);
            contact.selected = true;
            $scope.enableComposeMode();
            $state.go('main.messageCompose');
        };
        $scope.composeGroupMessage = function(group){
            $scope.recipients.groups = [group.id];
            $scope.enableComposeMode();
            $state.go('main.messageCompose');
        };


        $scope.messageDate = function(date_string, format){
            if(!date_string) return;
            var arr = date_string.split(/[- :]/),
                date = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
            var today = new Date();
            if(format) return $filter('date')(date,format);
            if (today.getDate() == date.getDate()) { // Same day
                return $filter('date')(date,'hh:mm');
            } else {
                return $filter('date')(date,'MMM d');
            }
        };
        $scope.messageFrom = function(sender){
            var filter = $scope.contacts.filter(function (element) {
                return sender.toLowerCase() === element.email.toLowerCase();
            });
            return filter[0];
        };
        $scope.messageTo = function(toArray){
            var contacts = [];

            angular.forEach(toArray, function(value, key) {
                var contact = $scope.contacts.filter(function (element) {
                    return value.toLowerCase() === element.email.toLowerCase();
                });
                this.push(contact[0]);
            }, contacts);
            return contacts;
        };
        $scope.messageToGroups = function(toArray){
            var groups = [];

            angular.forEach(toArray, function(value, key) {
                var group = $scope.groups.filter(function (element) {
                    return value === element.id;
                });
                this.push(group[0]);
            }, groups);
            return groups;
        };
        $scope.toggleMessageRead = function(message){
            message.read = (!message.read);
            $scope.updateUnreadMessageCount();
            window.localStorage.setArray('cdem_messages', $scope.messages);
            $ionicListDelegate.closeOptionButtons()
        };
        $scope.trashMessage = function(message, event){
            //event.preventDefault();
            event.stopPropagation();
            $scope.messages.inbox.splice($scope.messages.inbox.indexOf(message), 1);
            if(typeof $scope.messages.trash == 'undefined') $scope.messages.trash = [];
            $scope.messages.trash.push(message);
            window.localStorage.setArray('cdem_messages', $scope.messages);

            if($state.current.name != 'main.messageInbox') {
                $state.go('main.messageInbox');
            }
            $ionicListDelegate.closeOptionButtons();

            AppService.helpText('Message deleted.', 1000);

        };
        $scope.untrashMessage = function(message, event){
            //event.preventDefault();
            event.stopPropagation();
            $scope.messages.trash.splice($scope.messages.trash.indexOf(message), 1);
            $scope.messages.inbox.push(message);
            window.localStorage.setArray('cdem_messages', $scope.messages);
            $ionicListDelegate.closeOptionButtons()
        };

        $scope.goBack = function(){
            $ionicHistory.goBack();
        }

        $scope.clearSearch = function() {
            $scope.search.searchQuery = '';
        };
    })

    /* Profile */
    .controller('ProfilePageController', function($scope, $rootScope, ApiService, $location, $ionicHistory, $state, $ionicLoading, $ionicSideMenuDelegate, AppService) {

        var cached_data = window.localStorage.getArray('cdem_data');
console.log("cdem_roles", cached_data.cdem_roles);

//        $scope.static_cdem_groups = [
//            {id: 1, name: 'Governance        ', children: ['Joint Committee Member','CEG Member']},
//            {id: 2, name: 'Controller        ', children: ['Group Controller','Local Controller']},
//            {id: 3, name: 'Recovery Manager  ', children: ['Group Recovery Manager','Local Recovery Manager']},
//            {id: 4, name: 'ECC Staff Member  ', children: ['ECC Controller PA','ECC Response Manager','ECC Health, Safety and Risk Advisor','ECC PIM','ECC Logistics','ECC Operations','ECC Planning','ECC Intelligence','ECC Welfare','ECC Lifelines Utility Coordinator','ECC Recovery']},
//            {id: 5, name: 'EOC Staff Member  ', children: ['EOC Controller PA','EOC Response Manager','EOC Health, Safety and Risk Advisor','EOC PIM','EOC Logistics','EOC Operations','EOC Planning','EOC Intelligence','EOC Welfare','EOC Recovery']},
//            {id: 6, name: 'Technical Expert  '},
//            {id: 7, name: 'Emergency Services', children: ['Police','Fire','Health']},
//            {id: 8, name: 'Lifelines         ', children: ['Electricity','3 Waters','Transportation','Communications']},
//            {id: 9, name: 'Welfare Response  ', children: ['Registration','Needs Assessment & Registration','Shelter and Accommodation','Animal','Inquiry','Household goods and services','Psychosocial Support','Financial Assistance','Care and protection services for children and young people']}
//        ];

        $scope.static_cdem_groups = cached_data.cdem_roles;


        if (!AppService.checkConnection()) {
            AppService.helpText('NOTE: this feature is unavailable while not connected to the internet.', 3000);
        }

        console.log('local contacts ', $scope.contacts);

        var cached_contacts = AppService.getCachedContacts();

        console.log('cached contacts', cached_contacts);

        var filter = cached_contacts.filter(function (element) {
            return $rootScope.globals.currentUser.username.toLowerCase() === element.email.toLowerCase();
        });
        $scope.user = filter[0];

        console.log('user', filter);


        $scope.non_private_data = ($scope.user.non_private_data && JSON.parse($scope.user.non_private_data)) ? JSON.parse($scope.user.non_private_data) : [];
        $scope.private_data = ($scope.user.private_data && JSON.parse($scope.user.private_data)) ? JSON.parse($scope.user.private_data) : [];
        if(!$scope.private_data['Home Phone']) $scope.private_data['Home Phone'] = '';
        if(!$scope.private_data['Home Address']) $scope.private_data['Home Address'] = '';

        $scope.d = {};

        if($scope.non_private_data.selected_cdem_group) {
            var filter = $scope.static_cdem_groups.filter(function (element) {
                console.log(element.name);
                return $scope.non_private_data.selected_cdem_group.toLowerCase().trim() === element.name.toLowerCase().trim();
            });

            $scope.d.selected_cdem_group = filter[0];
            delete $scope.non_private_data.selected_cdem_group;
        }
        if($scope.non_private_data.selected_cdem_group_child) {
            $scope.d.selected_cdem_group_child = $scope.non_private_data.selected_cdem_group_child;
            delete $scope.non_private_data.selected_cdem_group_child;
        }

        delete $scope.non_private_data.job_title;

        console.log('user profile', $scope.user);


        $scope.removeField =  function(array,key){
            delete array[key];
        };

        $scope.updateProfile = function(){
            $scope.loading = true;

            $ionicLoading.show({
                template: 'Saving profile...'
            });

            $scope.non_private_data.selected_cdem_group = $scope.d.selected_cdem_group.name;
            $scope.non_private_data.selected_cdem_group_child = $scope.d.selected_cdem_group_child;

            $scope.user.non_private_data = JSON.stringify($scope.non_private_data);
            $scope.user.private_data = JSON.stringify($scope.private_data);
            delete $scope.non_private_data.selected_cdem_group;
            delete $scope.non_private_data.selected_cdem_group_child;

            console.log($scope.user);
            console.log($rootScope.globals.device_push);
            if($rootScope.globals.device_push) { // if device info is present, merge with user
                angular.extend($scope.user, $rootScope.globals.device_push);
            }

            ApiService.UpdateProfile($scope.user, function(response){
                $ionicLoading.hide();
                if(response.success) {
                    $scope.loading = false;

                    var cached_data = window.localStorage.getArray('cdem_data');
                    for (var key in cached_data.contacts) {
                        if ($scope.user.email == cached_data.contacts[key].email) {
                            console.log(key, cached_data.contacts[key]);
                            cached_data.contacts[key] = $scope.user;
                        }
                    }
                    window.localStorage.setArray('cdem_data', cached_data);

                    $rootScope.globals.profileUpdated = true;
                    window.localStorage.setArray('globals', $rootScope.globals);
                    window.localStorage.setItem('has_checked_home_profile_details', true);

                    $state.go('main.contacts');
                } else {
                    $scope.loading = false;
                    // show error popup with skip
                }
            }).error(function(){
                $scope.loading = false;
                $ionicLoading.hide();
            })
        };

        $scope.logout = function(){
            $ionicSideMenuDelegate.toggleLeft(false);
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache().then(function() {
                console.log("clear cache");
                $state.go('auth');
            });
        }
    })

    /* Contacts */
    .controller('ContactsPageController', function($scope, $rootScope, $ionicScrollDelegate, $ionicHistory, $location, $log, $timeout, $filter, $state) {
        $scope.navTitle = "CDEM Contacts";

        $scope.getItemHeight = function(item, index) {
            if (typeof item.letter != 'undefined') {
                return 30;
            } else {
                return 40;
                //return (item.selected_cdem_group) ? 64:40;
            };
        };

        console.log($scope.contacts);

        //var users = $filter('orderBy')($scope.contacts,'first_name');
        var users = $filter('orderBy')($scope.contacts,'first_name');
        var log = [];

        $scope.alphabet = iterateAlphabet();

        //Sort user list by first letter of name
        var tmp=[];
        var letters=[];
        for(i=0;i<users.length;i++){
            var letter=users[i].first_name.toUpperCase().charAt(0);
            if($scope.alphabet.indexOf(letter) < 0) letter = "#";
            if( letters[ letter] == undefined){
                letters[ letter]=[]
                tmp.push( {letter: letter} );
            }
            //tmp[ letter].push( users[i] );
            tmp.push( users[i] );
        }
        $scope.sorted_contacts = tmp;

        //Create alphabet object
        function iterateAlphabet()
        {
            var str = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numbers = new Array();
            for(var i=0; i<str.length; i++)
            {
                var nextChar = str.charAt(i);
                numbers.push(nextChar);
            }
            return numbers;
        }$scope.alpha_groups = [];
        for (var i=0; i<10; i++) {
            $scope.alpha_groups[i] = {
                name: i,
                items: []
            };
            for (var j=0; j<3; j++) {
                $scope.alpha_groups[i].items.push(i + '-' + j);
            }
        }

        /*
         * if given group is the selected group, deselect it
         * else, select the given group
         */
        $scope.toggleGroup = function(group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
            $ionicScrollDelegate.$getByHandle('contacts').resize();
        };
        $scope.isGroupShown = function(group) {
            return $scope.shownGroup === group;
        };

        //Click letter event
        $scope.scrollToItem = function(letter, event) {

            var item = $filter('filter')($scope.sorted_contacts, function (d) {return d.letter === letter;});
            var itemIndex = $scope.sorted_contacts.indexOf(item[0]);

            var scrollHeight = 0;
            for (var i = 0; i < itemIndex; i++) {
                scrollHeight += $scope.getItemHeight($scope.sorted_contacts[i], i);
            }
            $ionicScrollDelegate.scrollTo(0, scrollHeight);
        };

        $scope.contactClick = function(contact, event){
            event.preventDefault();
            event.stopPropagation();
            if($scope.inComposeMode) {
                var array = $scope.recipients.contacts;

                if (contact.selected) {
                    contact.selected = false;
                    var index = array.indexOf(contact.email)
                    array.splice(index, 1);
                } else {
                    contact.selected = true;
                    array.push(contact.email);
                }
            } else {
                $state.go('main.contactDetail',{id: contact.email});
                //$location.path('#/main/contacts/'+contact.id);
            }
        };

        $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: false
        });

    })
    .controller('GroupsPageController', function($scope, $rootScope, $ionicScrollDelegate, $ionicHistory, $location, $log, $timeout, $filter, $state) {

        $scope.groups_getItemHeight = function(item, index) {
            return 50;
        };

        $scope.toggleGroupSelection = function(group, event){
            event.preventDefault();
            event.stopPropagation();
            var array = $scope.recipients.groups;

            if(group.selected) {
                group.selected = false;
                var index = array.indexOf(group.id)
                array.splice(index, 1);
            } else {
                group.selected = true;
                array.push(group.id);
            }
        };

        $scope.groupClick = function(group, event){
            event.preventDefault();
            event.stopPropagation();
            if($scope.inComposeMode) {
                var array = $scope.recipients.groups;

                if (group.selected) {
                    group.selected = false;
                    var index = array.indexOf(group.id)
                    array.splice(index, 1);
                } else {
                    group.selected = true;
                    array.push(group.id);
                }
            } else {
                console.log('group click', group);
                $state.go('main.groupDetail',{id: group.id});
            }
        };

        $scope.$watch('search.searchQuery', function(newValue, oldValue) {
            if (newValue === oldValue) { return; }
            $ionicScrollDelegate.resize();
        });

    })

    .controller('FavouritesPageController', function($scope, $rootScope, $ionicScrollDelegate, $ionicHistory, $location, $log, $timeout, $filter, $state) {

        $scope.getItemHeight = function(item, index) {
            if(!item) return 75;
            return (item.letter) ? 30:75;
        };

        //var users = $filter('orderBy')($scope.contacts,'first_name');
        var users = $filter('orderBy')($scope.favorites,'first_name');
        var log = [];

        $scope.alphabet = iterateAlphabet();

        //Sort user list by first letter of name
        var tmp=[];
        var letters=[];
        for(i=0;i<users.length;i++){
            var letter=users[i].first_name.toUpperCase().charAt(0);
            if($scope.alphabet.indexOf(letter) < 0) letter = "#";
            if( letters[ letter] == undefined){
                letters[ letter]=[]
                tmp.push( {letter: letter} );
            }
            //tmp[ letter].push( users[i] );
            tmp.push( users[i] );
        }
        $scope.sorted_contacts = tmp;

        //Create alphabet object
        function iterateAlphabet()
        {
            var str = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numbers = new Array();
            for(var i=0; i<str.length; i++)
            {
                var nextChar = str.charAt(i);
                numbers.push(nextChar);
            }
            return numbers;
        }$scope.alpha_groups = [];
        for (var i=0; i<10; i++) {
            $scope.alpha_groups[i] = {
                name: i,
                items: []
            };
            for (var j=0; j<3; j++) {
                $scope.alpha_groups[i].items.push(i + '-' + j);
            }
        }

        /*
         * if given group is the selected group, deselect it
         * else, select the given group
         */
        $scope.toggleGroup = function(group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
            $ionicScrollDelegate.$getByHandle('contacts').resize();
        };
        $scope.isGroupShown = function(group) {
            return $scope.shownGroup === group;
        };

        //Click letter event
        $scope.scrollToItem = function(letter, event) {

            var item = $filter('filter')($scope.sorted_contacts, function (d) {return d.letter === letter;})[0];
            var itemIndex = $scope.sorted_contacts.indexOf(item);

            var scrollHeight = 0;
            for (var i = 0; i < itemIndex; i++) {
                scrollHeight += $scope.getItemHeight($scope.sorted_contacts[i], i);
            }
            $ionicScrollDelegate.scrollTo(0, scrollHeight);
        };

        $scope.contactClick = function(contact, event){
            event.preventDefault();
            event.stopPropagation();
            if($scope.inComposeMode) {
                var array = $scope.recipients.contacts;

                if (contact.selected) {
                    contact.selected = false;
                    var index = array.indexOf(contact.email)
                    array.splice(index, 1);
                } else {
                    contact.selected = true;
                    array.push(contact.email);
                }
            } else {
                $state.go('main.contactDetail',{id: contact.email});
                //$location.path('#/main/contacts/'+contact.id);
            }
        };

    })

    .filter("emptyifblank", function(){ return function(object, query){
        if(!query)
            return []
        else
            return object;
    }})
    .filter('removeunderscores', function(){ return function(text) {
        return text.replace(/_/g, ' ');
    }})
    .filter('cdemgroup', function(){ return function(text) {
        return text.replace('group', 'role');
    }})
    .filter('searchfilter', function() {
        return function (input, query) {
            if (query) {
                var r = RegExp('(' + query + ')', 'g');
                return input.replace(r, '<span class="super-class">$1</span>');
            } else {
                return input;
            }
        }
    })

    .controller('ContactDetailPageController', function($scope, $rootScope, $stateParams, AppService, $ionicPopover) {
        $scope.navTitle = "CDEM Contact";

        var cached_contacts = AppService.getCachedContacts();

        var filter = cached_contacts.filter(function (element) {
            return $stateParams.id.toLowerCase() === element.email.toLowerCase();
        });
        $scope.contact = filter[0];

        console.log($scope.contact);

        $scope.extra_fields = JSON.parse($scope.contact.non_private_data);
        if($scope.contact.private_data) angular.merge($scope.extra_fields, JSON.parse($scope.contact.private_data));

        $scope.contact.selected_cdem_group = $scope.extra_fields.selected_cdem_group;
        $scope.contact.selected_cdem_group_child = $scope.extra_fields.selected_cdem_group_child;
        delete $scope.extra_fields.selected_cdem_group;
        delete $scope.extra_fields.selected_cdem_group_child;

        delete $scope.extra_fields.job_title;

        var isFavorite = $scope.favorites.filter(function (element) {
            return $scope.contact.email.toLowerCase() === element.email.toLowerCase();
        });
        $scope.contact.isFavorite = (isFavorite.length > 0);
        console.log('isfavorite', $scope.contact.isFavorite);

        $scope.toggleFavorite = function(contact, event){
            if( contact.isFavorite ) {
                $scope.favorites = $scope.favorites.filter(function (element) {
                    return contact.email.toLowerCase() != element.email.toLowerCase();
                });
                $scope.contact.isFavorite = false;
                AppService.helpText('Favourite removed.', 1000);
            } else {
                $scope.favorites.push(contact);
                $scope.contact.isFavorite = true;
                AppService.helpText('Favourite added.', 1000);
            }
            console.log($scope.favorites);
            window.localStorage.setArray('contactFavorites', $scope.favorites);
        };

        $scope.isCellNumber = function(number){
            var prefix = number.replace(' ','').substring(0,3);
            return (prefix == '021' || prefix == '022' || prefix == '028' || prefix == '027' || prefix == '029' || prefix == '020');
        };

        $scope.triggerEmail = function(EMAIL) {
            if (EMAIL) {
                window.location ="mailto:"+ EMAIL;
            }
        };
        $scope.triggerSMS = function(SMS_NUMBER) {
            if (SMS_NUMBER) {
                SMS_NUMBER = SMS_NUMBER.replace(' ','');
                if(device.platform == 'Android'){
                    window.location ="sms:"+ SMS_NUMBER +"?body=" + encodeURIComponent(" ");
                } else {
                    window.location ="sms:"+ SMS_NUMBER;
                }
            }
        };
        $scope.showContactAvailableNumbers = function(contact, $event) {
            var numbers = [];

            if(contact.phone) numbers.push(contact.phone);
            if(contact.mobile_phone) numbers.push(contact.mobile_phone);

            var regexp = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;

            angular.forEach(JSON.parse(contact.non_private_data),function(value, key) {
                if( regexp.test(value) && contact.phone != value ) numbers.push(value);
            });

            $scope.availableNumbers = numbers;

            $ionicPopover.fromTemplateUrl('showNumbers-popover.html', {
                scope: $scope
            }).then(function (popover) {
                $scope.popover = popover;
                // open message type popup
                $scope.popover.show($event);
            });
        };
        $scope.triggerCall = function(NUMBER) {
            if (NUMBER) {
                NUMBER = NUMBER.replace(' ','');

                window.location ="tel:"+ NUMBER;
            }
        };
        $scope.triggerAddress = function(ADDRESS) {
            if (ADDRESS) {
                window.open('http://maps.google.com/?q='+ encodeURI(ADDRESS), '_system', 'location=yes');
            }
        };

        $scope.onExtraFieldClick = function(key, value, extra_fields){
            console.log(key, value);
            key = key.toLowerCase();

            // ADDRESS
            if(key.indexOf('address') > -1){
                $scope.triggerAddress(value + ' , ' + extra_fields['City'] + ' , ' + extra_fields['Post Code']);
                return;
            }
            // EMAIL
            if(key.indexOf('@') > -1) {
                $scope.triggerEmail(value);
                return;
            }
            // WEBSITE
            if(key.indexOf('website') > -1 || value.indexOf('www.') > -1 || value.indexOf('http') > -1) {
                if(value.indexOf('http') < 0) value = 'http://' +value;
                window.open(value, '_system', 'location=yes');
                return;
            }
            // PHONE
            var regexp = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
            if( regexp.test(value) ) {
                $scope.triggerCall(value);
                return;
            }
        };

    })

    .controller('GroupDetailPageController', function($scope, $rootScope, $stateParams, AppService) {
        $scope.navTitle = "CDEM Group";
        $scope.groups = AppService.getCachedGroups();

        console.log('groups', $stateParams.id, $scope.groups);

//        var result;
//        for( var i = 0, len = $scope.groups.length; i < len; i++ ) {
//
//            if( $scope.groups[i].id == $stateParams.id ) {
//                result = $scope.groups[i];
//                break; // found group in 1st level
//            }
//            if( $scope.groups[i].children.length ) {
//                for( var v = 0, len = $scope.groups[i].children.length; v < len; v++ ) {
//                    if ( $scope.groups[i].children[v].id === $stateParams.id) {
//                        result = $scope.groups[i].children[v];
//                        break;
//                    } // found group in child level
//                }
//            }
//        }
//
//        console.log('group', result);
//
//        $scope.group = result;

        var filter = $scope.groups.filter(function (element) {
            return $stateParams.id === element.id;
        });
        $scope.group = filter[0];

        $scope.subgroups = $scope.group.children;

        $scope.groupcontacts = [];

        angular.forEach($scope.group.contacts, function(value, key) {
            var contact = $scope.contacts.filter(function (element) {
                return value.toLowerCase() === element.email.toLowerCase();
            });
            if(contact[0]) this.push(contact[0]);
        }, $scope.groupcontacts);

        console.log($scope.groupcontacts);

        $scope.getItemHeight = function(item, index) {
            if(!item) return 40;
            if (typeof item.letter != 'undefined') {
                return 30;
            } else {
                return 40;
                //return (item.selected_cdem_group) ? 64:40;
            };
        };

    })

    /* Messages */
    .controller('MessageComposePageController', function($scope, $rootScope, $stateParams, ApiService, AppService, $state, $ionicPopover, $ionicModal, $ionicLoading, $filter) {

        if(!AppService.checkConnection()){
            AppService.helpText('NOTE: you must have an internet connection to use this feature.', 3000);
        }

        if(!$rootScope.new_message) $rootScope.new_message = {};
        $rootScope.new_message.messageType = 'push';

        if (typeof cordova != "undefined" && typeof cordova.plugins.email != "undefined") {
            cordova.plugins.email.isAvailable(function (isAvailable) {
                $scope.emailAvailable = isAvailable;
            });
        }

        $scope.clearForm = function(message){
            console.log(message);
            $scope.clearComposeMode();
            $rootScope.new_message = {};
        }

        $scope.sendMessage = function(){
            var data = this.new_message;
            //this.new_message = null;

            var groupcontacts = [];
            angular.forEach($scope.recipients.groups, function(value, key) {
                var group = $scope.groups.filter(function (element) {
                    return value === element.id;
                });

                groupcontacts = groupcontacts.concat(group[0].contacts);
            });

            console.log($scope.groups, groupcontacts, $scope.recipients);

            if(data.messageType && data.messageType == 'email' && typeof cordova.plugins.email != "undefined"){

                AppService.helpText('Opening email client...');

                var contacts = $scope.recipients.contacts;

                var to = contacts.concat(groupcontacts);

                console.log(to);

                cordova.plugins.email.open({
                    to:         to, // email addresses for TO field
                    subject:    data.subject, // subject of the email
                    body:       data.body, // email body (for HTML, set isHtml to true)
                    isHtml:     false // indicats if the body is HTML or plain text
                }, function(){
                    this.clearComposeMode();
                    this.clearForm();
                }, $scope);

            } else {


                $ionicLoading.show({
                    template: 'Sending message...'
                });

                //data.debug = true;
                data.to_contacts = $scope.recipients.contacts.join();
                data.to_groups = $scope.recipients.groups.join();
                data.type = 'push';

                console.log(data);

                var d = new Date();
                var sent_message = {
                    contacts: $scope.recipients.contacts,
                    groups: $scope.recipients.groups,
                    title: data.subject,
                    body: data.body,
                    sent: $filter('date')(d,'yyyy-MM-dd HH:mm:ss')
                };

                return ApiService.sendNewMessage(data).success(function(r) {

                    if(r.success){
                        $scope.submitSuccess = true;
                        $scope.messages.sent.push(sent_message);
                        window.localStorage.setArray('cdem_messages', $scope.messages);

                        $scope.clearForm();
                        $state.go('main.messageSent');
                    } else {
                        $scope.submitSuccess = false;
                        $scope.error = 'Something went wrong, please try again';
                    };
                    $ionicLoading.hide();
                }).error(function(){
                    $scope.submitSuccess = false;
                    $ionicLoading.hide();
                    $scope.error = 'Something went wrong, please try again';
                });
            };

        };

        $ionicPopover.fromTemplateUrl('recepients-popover.html', {
            scope: $scope
        }).then(function(popover) {
            $scope.popover = popover;
        });

        $scope.openReciepientsPopover = function($event) {
            $scope.popover.show($event);
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });


        $scope.removeRecipient = function(recipient,event){
            $scope.recipients.contacts.splice($scope.recipients.contacts.indexOf(recipient.email), 1);
        }
        $scope.removeGroupRecipient = function(recipient,event){
            $scope.recipients.groups.splice($scope.recipients.groups.indexOf(recipient.id), 1);
        }

    })
    .controller('MessageInboxPageController', function($scope, $rootScope, ApiService) {

        $scope.load = function(){

            ApiService.GetMessages($rootScope.globals.lastDownloadedMessages, function (response) {
                $scope.$broadcast('scroll.refreshComplete');

                if (response.success) {

                    $scope.updateUnreadMessageCount();

                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }

            });

        };


        console.log($scope.messages);
        $scope.getItemHeight = function(item, index) {
            return 74;
        };

        $scope.clearSearch = function() {
            $scope.m.inboxSearch = '';
        };

    })
    .controller('MessageSentPageController', function($scope, $filter, $ionicPopover) {

        //$scope.messages.sent = $scope.messages.sent.reverse();
        console.log($scope.messages.sent);

        $scope.sentToContacts = function(message){
            if(!message.contacts[0] && !message.groups[0]) return '';

            if(message.groups[0]) {
                var group = $scope.groups.filter(function (element) {
                    return message.groups[0] === element.id;
                });
                var others = (message.groups.length > 1) ? ' + ' + message.groups.length + ' others' : '';
                return group[0].name + others;
            } else {

                var contact = $scope.contacts.filter(function (element) {
                    return message.contacts[0].toLowerCase() === element.email.toLowerCase();
                });
                var others = (message.contacts.length > 1) ? ' + ' + message.contacts.length + ' others' : '';
                return contact[0].fullname + others;
            }
        };

        $scope.openReciepientsPopover = function(message, $event) {
            if(message.contacts.length < 2) return;

            $scope.message_title = message.title;
            $scope.recipients = [];

            angular.forEach(message.contacts, function(value, key) {
                var contact = $scope.contacts.filter(function (element) {
                    return value.toLowerCase() === element.email.toLowerCase();
                });
                this.push(contact[0]);
            }, $scope.recipients);

            $ionicPopover.fromTemplateUrl('sent-recepients-popover.html', {
                scope: $scope
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.popover.show($event);
            });
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
            if($scope.popover) $scope.popover.remove();
        });

    })
    .controller('MessageTrashPageController', function($scope) {

    })
    .controller('MessageDetailPageController', function($scope, $rootScope, $stateParams, $ionicPopover, $state) {
        var filter = $scope.messages.inbox.filter(function (element) {
            return $stateParams.id === element.id;
        });
        $scope.message = filter[0];

        if(!$scope.message) {
            $state.go('main.messageInbox');
        }

        $scope.message.read = true;
        window.localStorage.setArray('cdem_messages', $scope.messages);
        $scope.updateUnreadMessageCount();

        $ionicPopover.fromTemplateUrl('recepients-popover.html', {
            scope: $scope
        }).then(function(popover) {
            $scope.popover = popover;
        });

        $scope.replyTo = function(message){
            $scope.recipients.contacts = [message.sender];
            $rootScope.new_message = {
                subject: 'Re: ' + message.title
            };
            $scope.enableComposeMode();
            $state.go('main.messageCompose');
        };
        $scope.replyAll = function(message){
            $scope.recipients.contacts = message.contacts;
            $rootScope.new_message = {
                subject: 'Re: ' + message.title
            };
            $scope.enableComposeMode();
            $state.go('main.messageCompose');
        };

        $scope.openReciepientsPopover = function($event) {
            $scope.popover.show($event);
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });
    })

;
