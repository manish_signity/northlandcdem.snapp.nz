<?php
require_once "authenticate.php";
require_once "../config/dbconnection.php"; 

$auth = authenticate($conn);

function addColumn(&$values, $col_name, $value) {
    $values[]=array('col_name'=>$col_name,'value'=>$value);
}

if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {

        $angularjs_post=json_decode(file_get_contents('php://input'), true); // to work with AngularJS POST
        $angularjs_post_debug=isset($angularjs_post_debug['debug'])?$angularjs_post_debug['debug']:false;
        $post_debug=isset($_POST['debug'])?$_POST['debug']:false;
        
//        $post_vars=$angularjs_post;
        $post_vars=$_POST;
        
        $first_name=isset($post_vars['first_name'])?$post_vars['first_name']:false;
        
        $company=isset($post_vars['company'])?$post_vars['company']:false;
        
        $organisation_phone=isset($post_vars['organisation_phone'])?$post_vars['organisation_phone']:false;
        $email_second=isset($post_vars['email_second'])?$post_vars['email_second']:false;
        $ddi=isset($post_vars['ddi'])?$post_vars['ddi']:false;
        $home_phone=isset($post_vars['home_phone'])?$post_vars['home_phone']:false;
        
        $department=isset($post_vars['department'])?$post_vars['department']:false;
        $job_title=isset($post_vars['job_title'])?$post_vars['job_title']:false;
        $mobile_phone=isset($post_vars['mobile_phone'])?$post_vars['mobile_phone']:false;
        $phone=isset($post_vars['phone'])?$post_vars['phone']:false;
        $private_data=isset($post_vars['private_data'])?$post_vars['private_data']:false;
        $non_private_data=isset($post_vars['non_private_data'])?$post_vars['non_private_data']:false;
        
        $unread_message_count=isset($post_vars['unread_message_count'])?$post_vars['unread_message_count']:false;
        
        $device_gcm_id=isset($post_vars['device_gcm_id'])?$post_vars['device_gcm_id']:false;
        $device_apn_id=isset($post_vars['device_apn_id'])?$post_vars['device_apn_id']:false;
        $device_platform=isset($post_vars['device_platform'])?$post_vars['device_platform']:'';
        $device_version=isset($post_vars['device_version'])?$post_vars['device_version']:'';
        $device_uid=isset($post_vars['device_uid'])?$post_vars['device_uid']:'';
        $device_name=isset($post_vars['device_name'])?$post_vars['device_name']:'';
        $device_model=isset($post_vars['device_model'])?$post_vars['device_model']:'';
        
        $values=array();
        
        if ($first_name) {
            addColumn($values, 'first_name', $first_name);
        }
       
        if ($company) {
            addColumn($values, 'company', $company);
        }
        
        if ($organisation_phone) {
        	addColumn($values, 'organisation_phone', $organisation_phone);
        }
        if ($email_second) {
        	addColumn($values, 'email_second', $email_second);
        }
        if ($ddi) {
        	addColumn($values, 'ddi', $ddi);
        }
        if ($home_phone) {
        	addColumn($values, 'home_phone', $home_phone);
        }
        
        
        if ($department) {
                 addColumn($values, 'department', $department);
        }else{
        	     addColumn($values, 'department', '');
        }
        
        if ($job_title) {
            addColumn($values, 'job_title', $job_title);
        }
        
        if ($mobile_phone) {
               addColumn($values, 'mobile_phone', $mobile_phone);
        }else{
        	   addColumn($values, 'mobile_phone', '');
        }
        
        
        if ($phone) {
              addColumn($values, 'phone', $phone);
        }else{
        	  addColumn($values, 'phone', '');
        }
        
        
        if ($private_data) {
            addColumn($values, 'private_data', $private_data);
        }
        if ($non_private_data) {
            addColumn($values, 'non_private_data', $non_private_data);
        }
        
        if ($unread_message_count) {
            addColumn($values, 'unread_message_count', $unread_message_count);
        }
        
        if (!empty($values)) {
            $update_sql="update contact set last_modified=now(), last_modified_by=null,";

            $col_names=array();
            foreach($values as $data) {
                $col_names[]=" ".$data['col_name']."=:".$data['col_name'];
            }
            $update_sql.=implode(",",$col_names);
            
            $update_sql.=" where email=:email";
            
            $query=$conn->prepare($update_sql);
            foreach($values as $data) {
                $query->bindValue($data['col_name'],$data['value']);
            }
            $query->bindValue("email",$email);
            $query->execute();
            $return['message']="Profile updated. ";
            
        } else {
            $return['message']="No profile data to update. ";
        }
        
        // device setup
        if ($device_gcm_id) {
            $query=$conn->prepare("insert into device (gcm_id, platform, version, last_modified) values (:gcm_id, :platform, :version, now()) "
                    . "on duplicate key update platform=values(platform), version=values(version), last_modified=values(last_modified)");
            $query->bindValue("gcm_id",$device_gcm_id);
            $query->bindValue("platform",$device_platform);
            $query->bindValue("version",$device_version);
            $query->execute();
            $device_id = $conn->lastInsertId();
            
            if (!$device_id) {
                $query=$conn->prepare("select id from device where gcm_id=:gcm_id");
                $query->bindValue("gcm_id",$device_gcm_id);
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_ASSOC);
                if ($results) {
                    $device_id=$results[0]['id'];
                }
            }
            
            $query=$conn->prepare("insert into contact_device values (:device_id, :email) on duplicate key update email=values(email)");
            $query->bindValue("device_id",$device_id);
            $query->bindValue("email",$email);
            $query->execute();
            
            $return['message']=$return['message'].". Android device updated";
        }
        if ($device_apn_id) {
            $sql = "select pid from apns_devices where devicetoken=:apnid";
            $query=$conn->prepare($sql);
            $query->bindValue("apnid",$device_apn_id);
            $query->execute();
            $result=$query->fetchAll(PDO::FETCH_ASSOC);
            $apnid=null;
            if(!$result || !$result[0]) {  
                $query=$conn->prepare(
                        "insert into apns_devices (clientid, appname, appversion, deviceuid, devicetoken, devicename, devicemodel, deviceversion, pushbadge, pushalert, pushsound, created) "
                        . "values('',:appname,:appversion,:deviceuid,:devicetoken,:devicename,:devicemodel,:deviceversion,:pushbadge,:pushalert,:pushsound,NOW()) "
                        . "on duplicate key update devicetoken=values(devicetoken), devicename=values(devicename), devicemodel=values(devicemodel), deviceversion=values(deviceversion) ");
                $query->bindValue("appname",'HBRC Emergency Contacts');
                $query->bindValue("appversion",'1.0');
                $query->bindValue("deviceuid",$device_uid);
                $query->bindValue("devicetoken",$device_apn_id);
                $query->bindValue("devicename",$device_name);
                $query->bindValue("devicemodel",$device_model);
                $query->bindValue("deviceversion",$device_version);
                $query->bindValue("pushbadge",'enabled');
                $query->bindValue("pushalert",'enabled');
                $query->bindValue("pushsound",'enabled');
                $count=0;
                try{$count=$query->execute();}catch(Exception $e){}
                $apnid = $conn->lastInsertId();
                if($count) {
                    $success="1";
                    $return['message']=$return['message']."APN Device registered. ";
                    
                }
                else{
                    $success="0";
                    $return['message']=$return['message'].="APN Device failed to register. ";
                }
            } else {
                // check if device set to 'uninstalled'
                $query=$conn->prepare("update apns_devices set status = 'active', modified = now() where devicetoken=:apnid and status = 'uninstalled'");
                $query->bindValue("apnid",$device_apn_id);
                $query->execute();
                
                if ($query->rowCount()) {
                    $success="1";
                    $return['message']=$return['message'].="APN Device re-registered. ";
                    $apnid=$result[0]['pid'];
                } else {
                    $success="1";
                    $return['message']=$return['message'].="APN Device already registered. ";
                    $apnid=$result[0]['pid'];
                }
            }
            
            if ($apnid!==null) {
                $query=$conn->prepare("insert into device (apn_id, platform, version, last_modified) values (:apn_id, :platform, :version, now()) "
                        . "on duplicate key update platform=values(platform), version=values(version), last_modified=values(last_modified)");
                $query->bindValue("apn_id",$apnid);
                $query->bindValue("platform",$device_platform);
                $query->bindValue("version",$device_version);
                $query->execute();
                $device_id = $conn->lastInsertId();

                if (!$device_id) {
                    $query=$conn->prepare("select id from device where apn_id=:apn_id");
                    $query->bindValue("apn_id",$apnid);
                    $query->execute();
                    $results=$query->fetchAll(PDO::FETCH_ASSOC);
                    if ($results) {
                        $device_id=$results[0]['id'];
                    }
                }

                $query=$conn->prepare("insert into contact_device values (:device_id, :email) on duplicate key update email=values(email)");
                $query->bindValue("device_id",$device_id);
                $query->bindValue("email",$email);
                $query->execute();

                $return['message']=$return['message'].". iOS device updated";
            }
            
        }
        
        if ($angularjs_post_debug || $post_debug) {
            $debug=array(
                'angularjs_post'=>$angularjs_post,
                'post'=>$_POST
            );
            $return['debug']=$debug;
        }
        
    }

    echo json_encode($return);
}

exit;