<?php
require_once "authenticate.php";
require_once "../config/dbconnection.php"; 

$auth = authenticate($conn);

if ($auth !== NULL) {
    $return=$auth['return'];
    $email=$auth['email'];
    if ($return['success']==="1") {
        $return['success']=1;
        $return['message']="Successful login";

        $last_modified=array();

        $query=$conn->prepare("select max(contact.last_modified) as lmc, max(groups.last_modified) as lmg from contact, groups ");
        $query->execute();
        $last_mod_results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($last_mod_results)) {
            $last_modified['contact']=$last_mod_results[0]['lmc'];
            $last_modified['group']=$last_mod_results[0]['lmg'];
        }
        
        $return['data']['last_modified']=$last_modified;

        $fromTime=isset($_GET['from'])?strtotime($_GET['from']):false;
        $from=$fromTime?date('Y-m-d H:i:s',$fromTime):false;
        $fromQuery=$from?" and m.sent >= :from":"";
        
        $query=$conn->prepare("select count(distinct m.id) as nmc from message m, message_contact mc where m.id=mc.message_id and (m.sender=:sender or mc.email=:email)".$fromQuery);
        $query->bindValue("sender",$email);
        $query->bindValue("email",$email);
        if ($from) {
            $query->bindValue("from",$from);
        }
        $query->execute();
        $new_messages_results=$query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($new_messages_results)) {
            $return['data']['new_message_count']=$new_messages_results[0]['nmc'];
        }
        
        // set last logged in
        $query=$conn->prepare("update contact set last_login = now() where email = :email");
        $query->bindValue("email",$email);
        $query->execute();
    }

    echo json_encode($return);
}

exit;