<?php
require_once "authenticate.php";
require_once "../config/dbconnection.php"; 
$auth = authenticate($conn);

$return_data['success']            = false;
$return_data['message']            = 'Invalid login';
$return_data['data']['contacts']   = array();
$return_data['data']['cdem_roles'] = array();

if($auth !== NULL){
    $return = $auth['return'];
    $email  = $auth['email'];
    if ($return['success']==="1") {
    	$chkQuery = $conn->prepare("select * from contact where email like :email ");
    	$chkQuery->bindValue("email",$email);
    	$chkQuery->execute();
    	$chkResults = $chkQuery->fetchAll(PDO::FETCH_ASSOC);
    	
    	if (isset($chkResults[0]['email'])  && !empty($chkResults[0]['email'])) {
    		   $chkResults[0]['password']    = '';
    		   $return['success']            = true;
    		   $return['message']            = '';
    		   $chkResults[0]['fullname']   = $chkResults[0]['first_name'];
    		   $return['data']['contacts'][$chkResults[0]['email']]   = $chkResults[0];
    		   $return['data']['cdem_roles'] = $CDEM_ROLES;
    		   echo json_encode($return);
    		   die();
    	}else{
    		   echo json_encode($return_data);
    		   die();
    	}
    }else{
               echo json_encode($return_data);
    		   die();
    }
}else{
	          echo json_encode($return_data);
    	      die();
}

exit;