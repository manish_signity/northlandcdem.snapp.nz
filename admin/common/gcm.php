<?php
//this class implements Google Cloud Messaging in the app

//include required files
//require_once("phpInclude/dbconnection.php");

class GCM{
	
	function __construct(){
		
	}
	
	public function sendPush($registration_ids,$message){
		//gcs url
		$url="https://android.googleapis.com/gcm/send";
		
		//data to send
		$fields=array(
			"registration_ids"=>$registration_ids,
			"data"=>$message
		);
		//headers
		$headers=array(
			"Authorization: key=".GOOGLE_API_KEY,
			"Content-Type: application/json"
		);
                
		//open curl connection
		$ch=curl_init();
                
		//set curl parameters
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                                
                if (CURL_CACERTS) {
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE); 
                    curl_setopt($ch, CURLOPT_CAINFO, CURL_CACERTS);
//                    curl_setopt($ch, CURLOPT_CAINFO, "C:/Users/paul/Documents/snApp/cacerts/cacert.pem");
                }
                
		//execute curl
		$result=curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'GCM error: ' . curl_error($ch);
                    curl_close($ch);
                    return array('failure'=>count($registration_ids));
                }

                curl_close($ch);
                $json_result=json_decode($result);
		return $json_result!==null?get_object_vars($json_result):null;
	}
}

// +-----------------------------------------+
// + Example 1:								 +
// +-----------------------------------------+
// $gcm=new GCM;
// echo $gcm->sendPush(1,2);
?>